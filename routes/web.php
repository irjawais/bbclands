<?php

use Illuminate\Http\Request;


Route::get('/', 'UserController@index')->name('home');
Route::get('/signin', 'UserController@signin')->name('signin');
Route::get('search','PropertyController@search')->name('search');
Route::get('/properties', 'PropertyController@index')->name('properties');
Route::get('/property/{id}/{name}', 'PropertyController@detail')->name('detail');
Route::post('/deal-submited', 'PropertyController@ajaxDeal')->name('ajaxDeal');
Route::get('/agreement', 'PropertyController@agreement')->name('agreement');
Route::post('/agreement', 'PropertyController@agreementPost')->name('agreement');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/submit-property', 'PropertyController@submitProperty')->name('submit.property');
    Route::post('/submit-property', 'PropertyController@submitPropertyPost')->name('submit.property');
    Route::get('/my-profile', 'ProfileController@myProfile')->name('myProfile');
    Route::post('/my-profile', 'ProfileController@submitProfile')->name('submitProfile');
    Route::get('/change-password','HomeController@showChangePasswordForm')->name('changePasswordForm');
    Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
    Route::get('/my-property','PropertyController@myproperty')->name('myProperty');
    Route::get('/delete-property/{id}','PropertyController@removeProperty')->name('removeProperty');
    Route::get('/edit-property/{id}','PropertyController@editProperty')->name('editProperty');
    Route::post('/edit-property', 'PropertyController@editPropertyPost')->name('submit.editPropertyPost');
    
});
Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' =>'admin','namespace'=>'Admin','as' => 'admin.'], function () {

    Route::get('/', function () {
        return redirect(route("admin.login"));
    });
    
    //Login Routes...
    Route::get('login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('login','Auth\LoginController@login')->name('login');
    Route::post('logout','Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('register');

    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/agent', 'AgentController@index')->name('agent');
        Route::get('/ajax_get_agent', 'AgentController@ajax_get_agent')->name('ajax_get_agent');
        Route::get('agent/detail/{id}', 'AgentController@detail')->name('detail');
        Route::get('agent/detail/{id}/approval/yes', 'AgentController@agent_approvl_yes')->name('agent_approvl_yes');
        Route::get('agent/detail/{id}/suspend/yes', 'AgentController@agent_suspend_yes');
        Route::get('agent/detail/{id}/suspend/no', 'AgentController@agent_suspend_no');
        Route::get('agent/detail/{id}/power', 'AgentController@power')->name('power');
        Route::get('agent/detail/{id}/power/manager', 'PowerController@power_manager')->name('power_manager');
        Route::get('agent/detail/{id}/power/assistant', 'PowerController@power_assist')->name('power_assist');
        Route::get('agent/detail/{id}/power/remove', 'PowerController@power_remove')->name('power_remove');
        
        Route::get('featured', 'PropertyController@featured')->name('featured');
        Route::get('ajax-featured', 'PropertyController@ajaxFeatured')->name('ajaxFeatured');
        Route::get('featured/{id}/{mark}', 'PropertyController@mark_as_featured')->name('mark_as');
        Route::get('agreement', 'PropertyController@agreement')->name('agreement');
        Route::get('ajax-agreement', 'PropertyController@ajaxAgreement')->name('ajaxAgreement');
        Route::get('agreement/download/{id}', 'PropertyController@agreementDownload')->name('agreement.download');

        
        
    });
});

Route::get('email_verify',function (){
    return view('agent.email.emailverification');
})->name('email_verify');
Route::post('email_verify','Agent\EmailController@email_verifyPost')->name('emailverificationPost');
Route::get('resend_email_code','Agent\EmailController@resend_email_code')->name('resend.email.code');
Route::get('acount_approval',function (){
    return view('agent.restrictions.approval');
})->name('acount_approval');
Route::get('acount_suspend',function (){
    return view('agent.restrictions.suspend');
})->name('acount_suspend');



Route::get('email_tamplate','Agent\EmailController@email_tamplate');
Route::group(['prefix' =>'agent','namespace'=>'Agent','as' => 'agent.','middleware' => ['agent:agent']], function () {

    Route::get('/', function () {
        return redirect(route("agent.login"));
    });
    
    
    
    //Login Routes...
    Route::get('login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('login','Auth\LoginController@login')->name('login');
    Route::post('logout','Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('register');
    Route::group(['middleware' => ['auth:agent']], function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/add_property', 'PropertyController@add_property')->name('add.property');
        Route::post('/add_property', 'PropertyController@add_propertyPost')->name('property.Post');
        Route::get('/myproperty', 'PropertyController@myproperty')->name('my.property');
        Route::get('/ajaxmypropertydata', 'PropertyController@ajaxmypropertydata')->name('ajaxmypropertydata');
        Route::get('/viewproperty/{id}', 'PropertyController@viewproperty')->name('viewproperty');
        Route::get('/property/new-request', 'PropertyController@newRequest')->name('property.request');
        Route::get('/property', 'PropertyController@allProperty')->name('property.all');
        Route::get('/property/all-property', 'PropertyController@ajaxallProperty')->name('ajaxallProperty');
        
        
        Route::get('/property/new-property-request', 'PropertyController@ajaxNewPropertyRequest')->name('ajaxNewPropertyRequest');
        Route::get('/property/new-deals', 'DealsController@newDeals')->name('new.deals');
        Route::get('/property/ajax-new-deals', 'DealsController@ajaxNewDeals')->name('ajaxNewDeals');
        Route::get('/deal-accept/{id}', 'DealsController@acceptDeal');
        Route::get('/deal-reject/{id}', 'DealsController@rejectDeal');
        Route::get('/deal-complete/{id}', 'DealsController@completeDeal');
        Route::get('/deal-delete/{id}', 'DealsController@deleteDeal');
        Route::get('/accept-property/{id}', 'PropertyController@acceptProperty')->name('acceptProperty');
        Route::get('/delete-property/{id}', 'PropertyController@deleteProperty')->name('deleteProperty');
        Route::get('/delete-my-property/{id}', 'PropertyController@deleteMyProperty')->name('deleteMyProperty');
        Route::get('/ajax-my-property', 'PropertyController@ajaxMyProperty')->name('ajaxMyProperty');
        Route::get('/my-all-deals', 'DealsController@myAllDeals')->name('my.all.property');
        Route::get('/ajax-my-all-deals', 'DealsController@ajaxMyAllDeals')->name('ajaxMyAllDeals');
        
    });
});