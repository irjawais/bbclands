<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovingColumnsGaragesFromPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function($table) {
            /* $table->dropColumn('garages');
            $table->dropColumn('tenure'); */
            $table->dropColumn('bed');
            $table->dropColumn('bath');
            
            $table->integer('feature_count')->nullable();
            $table->longText('feature_id')->nullable();
            $table->longText('uploaded_files')->nullable();
         });
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('property', function($table) {
            $table->dropColumn('feature_count');
            $table->dropColumn('feature_id');
            $table->dropColumn('uploaded_files');
         });
    }
}
