<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingColumnsPhoneNoEmailAddressToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function($table) {
            $table->string('owner_phoneno')->nullable();
            $table->string('owner_email')->nullable();
            $table->longText('owner_address')->nullable();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property', function($table) {
            $table->dropColumn('owner_phoneno');
            $table->dropColumn('owner_email');
            $table->dropColumn('owner_address');
         });
    }
}
