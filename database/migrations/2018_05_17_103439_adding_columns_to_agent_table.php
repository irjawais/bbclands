<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingColumnsToAgentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent', function (Blueprint $table) {
            $table->string('email_verify_code')->nullable();
            $table->integer('email_verified')->default(0);//1 or zero yes or no
            $table->integer('approve_by_admin')->default(0);
            $table->integer('suspend')->default(0);//by admin
            $table->string('power')->nullable();//manager
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent', function (Blueprint $table) {
            $table->dropColumn('email_verify_code');
            $table->dropColumn('email_verified');
            $table->dropColumn('approve_by_admin');
            $table->dropColumn('suspend');
            $table->dropColumn('power');
       });
    }
}
