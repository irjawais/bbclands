<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgreementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->string("name");
            $table->string("phone_office")->nullable();
            $table->string("f_or_h_name");//father or husband name
            $table->string("phone_residence")->nullable();
            $table->string("cnic_passport");
            $table->string("mobile")->nullable();
            $table->string("nationality");
            $table->string("whatsapp")->nullable();
            $table->string("email")->nullable();
            $table->string("twitter")->nullable();
            $table->string("facebook")->nullable();
            $table->string("linkedin")->nullable();
            $table->longText('present_address')->nullable();
            $table->longText('permanent_address')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('cheque')->nullable();
            $table->string('draft')->nullable();

            $table->string('date')->nullable();
            $table->string('branch')->nullable();
            $table->string('drawn_no')->nullable();
            $table->string('price')->nullable();
            $table->string('price_unit')->nullable();
            $table->timestamps();
            $table->increments('id');
            $table->longText('about_me')->nullable();
            $table->string('file')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
