<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBedColumnsPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->integer('bed')->nullable();
            $table->integer('bath')->nullable();
            $table->string('sqft')->nullable();
           /*  $table->string('garages')->nullable();
            $table->string('tenure')->nullable(); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->dropColumn('bed');
            $table->dropColumn('bath');
            $table->dropColumn('sqft');
            /* $table->dropColumn('garages');
            $table->dropColumn('tenure'); */
        });
    }
}
