<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("property_id")->nullable();
            $table->foreign('property_id')->references('id')->on('property')->onDelete('restrict');
            $table->string("name");
            $table->string("email")->nullable();
            $table->string("phoneno");
            $table->string("description")->nullable();
            $table->string("status");
            $table->boolean("delete")->default(0);
            $table->string("lng")->nullable();
            $table->string("lat")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
