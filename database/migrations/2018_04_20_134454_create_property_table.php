<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purpose', 100);
            $table->string('property_type', 100);
            $table->string('country', 200);
            $table->string('city', 200);
            //$table->string('location', 200);
            $table->string('property_title', 250);
            $table->string('description', 200);
            $table->bigInteger('price');

            $table->string('area', 200);
            $table->string('unit', 200);
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property');
    }
}
