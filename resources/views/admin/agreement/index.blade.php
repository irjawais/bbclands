@extends('admin.layouts.app')

@section('content')
<div class="right_col" role="main">
         
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Featured Products <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <!-- <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p> -->

                    <div class="table-responsive">
                      <table id="property_table" class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                                <th class="column-title">Name </th>
                                <th class="column-title">Phone No</th>
                                <th class="column-title">Date</th>
                                <th class="column-title"></th>
                           
                          </tr>
                        </thead>

                      </table>
                    </div>
@endsection
@section('script')

<script type="text/javascript">
 $(document).ready(function () {
    
    $('#property_table').DataTable({
   
   processing: true,
   serverSide: true,
   ajax:{ 
       "url":  '{{route('admin.ajaxAgreement')}}' ,
       data: {
       'csrf_token':$('meta[name=csrf_token]').attr("content")
       }
   },
   
   columns: [
       
       { data: 'name', name: 'name' },
       { data: 'mobile', name: 'mobile' },
       { data: 'created_at', name: 'created_at' },
       {
           "mData": null,
           "bSortable": false,
            "mRender": function (data) { 
                return `
                    <a  class="btn btn-success btn-xs"  title="Mark from featured" href=/admin/agreement/download/${data.id}>View Agreement</a>
                    `; 
              
           }
       }
   ]
});
});
</script>

@endsection
@section('css')
<style>
   #property_table tbody tr td{
        line-height: 50px;
    }
</style>
@endsection