<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Agreement</title>
        {{-- <link rel="stylesheet" href="{{ asset('pdf_bbclands/css/foundation.css') }}" />
        <link rel="stylesheet" href="{{ asset('pdf_bbclands/style.css') }}" />
        <script src="{{ asset('pdf_bbclands/js/vendor/modernizr.js') }}"></script> --}}

        <link type="text/css" media="all" rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/css/foundation.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

        
        <style>
            @media print {
            body {
                
                background-size: contain;
                background-repeat: no-repeat;
                background-position: left top;
                height: 1300px;
                width: 900px;"
            }
        }
        </style>
    </head>
    <body>
        <img src=" {{asset('pdf_bbclands/img/BBC_bg.jpg')}}" alt="" style="    max-width: 100%;
        height: auto;
        -ms-interpolation-mode: bicubic;
        display: inline-block;
        vertical-align: middle;
        position: absolute;
        z-index: -111;
        height: 1250px;
        width: 100%;">
        <!-- End Top Bar -->
        <!-- Main Page Content and Sidebar -->
        {{-- background-image: url({{ asset('pdf_bbclands/img/BBC_bg.jpg') }}); --}}
        <div class="row" style="    
    background-size: contain;
    background-repeat: no-repeat;
    background-position: left top;
    height: 1300px;
    width: 900px;">
            <!-- Contact Details -->
            <div class="columns large-12">
                <div class="section-container tabs" data-section style="margin-top: 320px;border:none">
                    <section class="section">
                        <div class="content" data-slug="panel1"> 
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline">NAME MR / MISS</label>
                                </div>
                                <div class="large-10 columns">
                                    <p class="agrdata">{{$agreement->name}}</p>
                                </div>
                            </div>                             
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> FATHER / HUSBAND NAME</label>
                                </div>
                                <div class="large-10 columns">
                                    <p class="agrdata">{{$agreement->f_or_h_name or " "}}</p>
                                </div>
                            </div>                             
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> CNIC / PASSPORT NUMBER</label>
                                </div>
                                <div class="large-10 columns">
                                    <p class="agrdata">{{$agreement->cnic_passport or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> NATIONALITY</label>
                                </div>
                                <div class="large-10 columns">
                                    <p class="agrdata">{{$agreement->nationality or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> PRESENT ADDRESS</label>
                                </div>
                                <div class="large-10 columns">
                                    <p class="agrdata">{{$agreement->present_address or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> PERMANENT ADDRESS</label>
                                </div>
                                <div class="columns large-10">
                                    <p class="agrdata">{{$agreement->permanent_address or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> TELEPHONE OFFICE</label>
                                </div>
                                <div class="columns large-4">
                                    <p class="agrdata">{{$agreement->phone_office or " "}}</p>
                                </div>
                                <div class="columns large-4">
                                    <p class="agrdata"></p>
                                </div>
                                <div class="large-2 columns">
                                    <label class="inline"> RESIDENCE</label>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> MOBILE</label>
                                </div>
                                <div class="columns large-4">
                                    <p class="agrdata">{{$agreement->phone_residence or " "}}</p>
                                </div>
                                <div class="large-2 columns">
                                    <label class="inline"> WHATSAPP</label>
                                </div>
                                <div class="columns large-4">
                                    <p class="agrdata">{{$agreement->whatsapp or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> E-MAIL</label>
                                </div>
                                <div class="columns large-10">
                                    <p class="agrdata">{{$agreement->email or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> MODE OF PAYMENT</label>
                                </div>
                                <div class="columns large-10">
                                    <p class="agrdata">CHEQUE : Cheque No. {{$agreement->cheque or " "}}</p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-2 columns">
                                    <label class="inline"> DATE</label>
                                </div>
                                <div class="columns large-4">
                                    <p class="agrdata">{{$agreement->date or " "}}</p>
                                </div>
                                <div class="large-2 columns">
                                    <label class="inline"> BRANCH</label>
                                </div>
                                <div class="columns large-4">
                                    <p class="agrdata"> {{$agreement->branch or " "}}</p>
                                </div>
                                <div class="row collapse">
                                    <div class="large-2 columns">
                                        <label class="inline"> DRAWN ON :</label>
                                    </div>
                                    <div class="columns large-10">
                                        <p class="agrdata">{{$agreement->drawn_no or " "}}</p>
                                    </div>
                                </div>
                                <div class="row collapse">
                                    <div class="large-2 columns">
                                        <label class="inline"> IN AMOUNT</label>
                                    </div>
                                    <div class="columns large-10">
                                        <p class="agrdata">{{$agreement->price or " "}}</p>
                                    </div>
                                </div>
                                <div class="row collapse">
                                    <div class="large-2 columns">
                                        <label class="inline"> DATED</label>
                                    </div>
                                    <div class="columns large-4">
                                        <p class="agrdata">{{$agreement->date or " "}}</p>
                                    </div>
                                    <div class="columns large-4">
                                        <p class="agrdata">.</p>
                                    </div>
                                    <div class="large-2 columns">
                                        <label class="inline">. </label>
                                    </div>
                                </div>
                            </div>                             
                        </div>
                    </section>
                </div>
            </div>
            <!-- End Contact Details -->
            <!-- Sidebar -->
            <!-- End Sidebar -->
        </div>
        <!-- End Main Content and Sidebar -->
        <!-- Footer -->
        <div class="reveal" id="modal7" data-reveal>
            <h4>Where We Are</h4>
            <p><img src="http://placehold.it/800x600" /></p>
            <button class="close-button" data-close aria-label="Close reveal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- End Footer -->
        <!-- Map Modal -->
        {{-- <script src="{{ asset('pdf_bbclands/js/vendor/jquery.min.js') }}"></script>
        <script src="{{ asset('pdf_bbclands/js/foundation.min.js') }}"></script> --}}
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/vendor/jquery.min.js"></script>
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/foundation.min.js"></script>
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
        <script>
          $(document).foundation();
          //let doc = new jsPDF('p','pt','a4');
         var doc = new jsPDF({
        //orientation: 'landscape',
        unit: 'in',
        format: [10, 10]
        })
     doc.addHTML(document.body,function() {
        
                doc.save('Agreement Form.pdf');
            });

          
          /* var imgData;
                html2canvas($(document.body), {
                    useCORS: false,
                    onrendered: function (canvas) {
                        imgData = canvas.toDataURL(
                            'http://bbclands.com/pdf_bbclands/img/BBC_bg.jpg');
                        var doc = new jsPDF('p', 'pt', 'a4');
                        doc.addImage(imgData, 'PNG', 10, 10);
                        doc.save('sample-file.pdf');
                        //setTimeout(function () { window.open(imgData) }, 1000);
                    }
                }); */
            
        </script>
    </body>
</html>