@extends('admin.layouts.app')

@section('content')
<div class="right_col" role="main">
         
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Featured Products <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <!-- <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p> -->

                    <div class="table-responsive">
                      <table id="property_table" class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                                <th class="column-title">Property </th>
                                <th class="column-title">Title</th>
                                <th class="column-title">Price</th>
                                <th class="column-title">City</th>
                                <th class="column-title">Date</th>
                                <th class="column-title"></th>
                           
                          </tr>
                        </thead>

                      </table>
                    </div>
@endsection
@section('script')

<script type="text/javascript">
 $(document).ready(function () {
    
    $('#property_table').DataTable({
   
   processing: true,
   serverSide: true,
   ajax:{ 
       "url":  '{{route('admin.ajaxFeatured')}}' ,
       data: {
       'csrf_token':$('meta[name=csrf_token]').attr("content")
       }
   },
   
   columns: [
       {
           
           "mData": null,
           "bSortable": false,
           //name:"settlement.id",
          "mRender": function (data) { 
                //var matches = data.uploaded_files.match(/\[\[(.*?)\]/)[1];
                var matches = "/attachment_property/"+data.uploaded_files.match(/\[\[(.*?)\]/)[1];
                match = matches.replace('&quot;', '');
                match = match.replace('&quot;', '');
            return `
            <a style="color:blue" class="btn btn-info btn-xs" title="View Property" href=/agent/viewproperty/${data.id}> 
                <img style="widht:50px;height:50px;" src = "${match}">
            </a>
               `; 
              /* return `
                   <a style="color:blue" class="btn btn-info btn-xs" title="View Property" href=/agent/viewproperty/${data.id}>${data.id}</a>
               `;  */
           }
       },
       { data: 'property_title', name: 'property_title' },
       { data: 'price', name: 'price' },
       { data: 'city', name: 'city' },  
       { data: 'created_at', name: 'created_at' },
       {
           "mData": null,
           "bSortable": false,
            "mRender": function (data) { 
                if(data.is_featured == 0){
                    return `
                    <a  class="btn btn-warning btn-xs"  title="Mark from featured" href=/admin/featured/${data.id}/1>Mark</a>
                    `; 
                }
                else if(data.is_featured == 1){
                    return `
                    <a class="btn btn-danger btn-xs"  title="Unmark from featured" href=/admin/featured/${data.id}/0>Unmark</a>
                    `; 
                    }
                
              /* return `
                   <a style="color:blue" class="btn btn-warning btn-xs"  title="Accept Requested" href=/agent/accept-property/${data.id}>Accept</a>
                   <a style="color:blue" class="btn btn-danger btn-xs"  title="Delete" href=/agent/delete-property/${data.id}>Delete</a>
               `;  */
           }
       }
   ]
});
});
</script>

@endsection
@section('css')
<style>
   #property_table tbody tr td{
        line-height: 50px;
    }
</style>
@endsection