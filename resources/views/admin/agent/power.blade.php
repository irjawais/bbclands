@extends('admin.layouts.app')

@section('content')
<div class="right_col" role="main">
         
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grand Powers <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="table-responsive">
                      <table id="agent_table" class="table">
                        <thead>
                          <tr class="headings">
                            
                            <th class="column-title">Powers Name </th>
                            <th class="column-title">Can Do </th>
                            <th class="column-title"> </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Manager</td>
                            <td>Delete Property, Set Perority , Can  view All types of deals</td>
                            <td><a href="{{route('admin.power_manager',$id)}}" class="btn btn-success">Set</a></td>
                          </tr>
                          <tr>
                            <td>Assistant</td>
                            <td>Can  view All types of deals </td>
                            <td><a href="{{route('admin.power_assist',$id)}}"  class="btn btn-success">Set</a></td>
                          </tr>
                        </tbody>
                      </table>
                      <a href="{{route('admin.power_remove',$id)}}"  class="btn btn-success">Remove All Power</a>
                    </div>

@endsection