@extends('admin.layouts.app')

@section('content')
<div class="right_col" role="main">
<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{{$agent->name}} <small> {{$agent->email}}</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <div>
                    <div class="row">
                        <div class="col-sm-3" >
                            <p>Power: </p>
                        </div>
                        <div class="col-sm-6" >
                            <p>
                                @if($agent->power=="manager")
                                        Manager
                                @endif
                                @if(!$agent->power)
                                        Nothing
                                @endif
                            </p>
                        </div>
                        <div class="col-sm-3" >
                        <a href="{{route('admin.power',$agent->id)}}"class="btn btn-info btn-md">Action</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" >
                            <p>Email Verified: </p>
                        </div>
                        <div class="col-sm-9" >
                            <p>
                                @if($agent->email_verified==1)
                                        Verified
                                @endif
                                @if($agent->email_verified==0)
                                        Email Not Verified
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" >
                            <p>Registered At: </p>
                        </div>
                        <div class="col-sm-9" >
                            <p>
                              {{$agent->created_at}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" >
                            <p>Approved: </p>
                        </div>
                        <div class="col-sm-6" >
                            <p>
                            @if($agent->approval==1)
                                        Approved
                            @endif
                            @if($agent->approval==0)
                                        Not Approved
                            @endif
                            </p>
                        </div>
                        <div class="col-sm-3" >
                        @if($agent->approval==1)
                        <a  class="btn btn-danger btn-md">Reject</a>
                            @endif
                            @if($agent->approval==0)
                                <a href="/admin/agent/detail/{{$agent->id}}/approval/yes" class="btn btn-primary btn-md">Accept</a>
                            @endif
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" >
                            <p>Susspended Status: </p>
                        </div>
                        <div class="col-sm-6" >
                            <p>
                            @if($agent->suspend==1)
                                        No
                            @endif
                            @if($agent->suspend==0)
                                        Yes
                            @endif
                            </p>
                        </div>
                        <div class="col-sm-3" >
                        @if($agent->suspend==1)
                            <a href="/admin/agent/detail/{{$agent->id}}/suspend/no" class="btn btn-primary btn-md">Unblock</a>
                            @endif
                            @if($agent->suspend==0)
                            <a href="/admin/agent/detail/{{$agent->id}}/suspend/yes" class="btn btn-danger btn-md">Block</a>
                            @endif
                        
                        </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
@endsection