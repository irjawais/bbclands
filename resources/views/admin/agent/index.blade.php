@extends('admin.layouts.app')

@section('content')
<div class="right_col" role="main">
         
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Agents <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <!-- <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p> -->

                    <div class="table-responsive">
                      <table id="agent_table" class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            
                            <th class="column-title"># </th>
                            <th class="column-title">Name </th>
                            <th class="column-title">Email </th>
                            <th class="column-title">Power </th>
                          </tr>
                        </thead>

                      </table>
                    </div>
@endsection
@section('script')

<script type="text/javascript">
 $(document).ready(function () {
    
    $('#agent_table').DataTable({
   
        processing: true,
        serverSide: true,
        ajax:{ 
            "url":  '{{url('admin/ajax_get_agent')}}' ,
            data: {
            'csrf_token':$('meta[name=csrf_token]').attr("content")
            }
        },
        
        columns: [
            {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue"  title="View Agent" href=/admin/agent/detail/${data.id}>${data.id}</a>
                    `; 
                }
            },
           { data: 'name', name: 'name' },  
          { data: 'email', name: 'email' },
          {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   if(data.power=="manager"){
                    return `
                        <span class="label label-info">Manager</span>
                    `; 
                   }
                   if(!data.power){
                    return `<span class="label label-info">None</span>
                     `;  }
                    return `
                        <span class="label label-info">${data.power}</span>
                    `; 
                   }
                   
                }
            
        ]
    });
});
</script>

@endsection