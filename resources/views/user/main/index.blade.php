@extends('user.layouts.apps')
@section('css')
 <!-- External Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- CSS files -->
   <link rel="stylesheet" href="{{ asset('bbclands/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('bbclands/css/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('bbclands/img/favicon.png') }}">
@endsection

@section('content')
    <section class="main-slider">
        <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:rgba(0,0,0,0.5);padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.4.6 fullwidth mode -->
            <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ asset('bbclands/img/main_slider_1-100x50.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('bbclands/img/dummy.png') }}" alt="" title="main_slider_1" width="1920" height="820" data-lazyload="{{ asset('bbclands/img/main_slider_1.jpg') }}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme" id="slide-1-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-60','-40']" data-fontsize="['18','18','16','14']"
                            data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 18px; line-height: 22px; font-weight: 400; color: #f3f3f3; letter-spacing: 0px;font-family:Roboto;">THE BEST REAL ESTATE DEALS </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme" id="slide-1-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['60','60','40','26']"
                            data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 900; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Discover Your Perfect Home </div>
                        <!-- LAYER NR. 3 -->
                        <a href="https://www.youtube.com/watch?v=R3xyBG2u1BU&t=26s" data-lity class="tp-caption   tp-resizeme" target="_self" id="slide-1-layer-7" data-x="['center','center','center','center']" data-hoffset="['6','6','6','6']" data-y="['top','top','top','top']" data-voffset="['480','380','300','230']"
                            data-fontsize="['60','60','36','36']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-actions='' data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;text-decoration: none;"><i class="fa fa-play-circle-o"></i> </a>
                    </li>
					<!-- SLIDE  -->
                    <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ asset('bbclands/img/main_slider_1-2-100x50.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('bbclands/img/dummy.png') }}" alt="" title="main_slider_1" width="1920" height="820" data-lazyload="{{ asset('bbclands/img/main_slider_1-2.jpg') }}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme" id="slide-1-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-60','-40']" data-fontsize="['18','18','16','14']"
                            data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 18px; line-height: 22px; font-weight: 400; color: #f3f3f3; letter-spacing: 0px;font-family:Roboto;">THE BEST REAL ESTATE DEALS </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme" id="slide-1-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['60','60','40','26']"
                            data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 900; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Discover Your Perfect Home </div>
                        <!-- LAYER NR. 3 -->
                        <a href="https://www.youtube.com/watch?v=R3xyBG2u1BU&t=26s" data-lity class="tp-caption   tp-resizeme" target="_self" id="slide-1-layer-7" data-x="['center','center','center','center']" data-hoffset="['6','6','6','6']" data-y="['top','top','top','top']" data-voffset="['480','380','300','230']"
                            data-fontsize="['60','60','36','36']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-actions='' data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;text-decoration: none;"><i class="fa fa-play-circle-o"></i> </a>
                    </li>
                    <!-- SLIDE  -->
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
        <!-- END REVOLUTION SLIDER -->
        <section class="main-search main-search--absolute">
            <div class="container">
                <div class="main-search__container">
                    <section class="listing-search">
                        <form action="{{route('search')}}" method="get" class="listing-search__form">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="listing-type" class="listing-search__label">Property Types</label>
                                    <select name="type" id="listing-type" class="property_type">
                                            <option value="all" selected>All Listing Type</option>
                                            <option value="home">Home</option>
                                            <option value="plot">Plot</option>
                                            <option value="commercial">Commercial</option>
                                        </select>
                                </div>
                                <!-- .col -->
                                <div class="col-sm-3">
                                    <label for="offer-type" class="listing-search__label">Purpose</label>
                                    <select name="purpose" id="offer-type" class="ht-field">
                                            <option value="all">All</option>
                                            <option value="rent">Rent</option>
                                            <option value="sale">Sale</option>
                                            <option value="buy">Buy</option>
                                        </select>
                                </div>
                                <!-- .col -->
                                <div class="col-sm-3">
                                    <label for="city" class="listing-search__label">Select Your City</label>
                                    <select name="city" id="city" class="ht-field">
                                        <option value="all">Select City</option>
                                        <option class="option" value="Abbottabad">Abbottabad</option>
                                        <option class="option" value="Adezai">Adezai</option>
                                        <option class="option" value="Ali Bandar">Ali Bandar</option>
                                        <option class="option" value="Amir Chah">Amir Chah</option>
                                        <option class="option" value="Attock">Attock</option>
                                        <option class="option" value="Ayubia">Ayubia</option>
                        <option class="option" value="Bahawalpur">Bahawalpur</option>
                        <option class="option" value="Baden">Baden</option>
                        <option class="option" value="Bagh">Bagh</option>
                        <option class="option" value="Bahawalnagar">Bahawalnagar</option>
                        <option class="option" value="Burewala">Burewala</option>
                        <option class="option" value="Banda Daud Shah">Banda Daud Shah</option>
                        <option class="option" value="Bannu district|Bannu">Bannu</option>
                        <option class="option" value="Batagram">Batagram</option>
                        <option class="option" value="Bazdar">Bazdar</option>
                        <option class="option" value="Bela">Bela</option>
                        <option class="option" value="Bellpat">Bellpat</option>
                        <option class="option" value="Bhag">Bhag</option>
                        <option class="option" value="Bhakkar">Bhakkar</option>
                        <option class="option" value="Bhalwal">Bhalwal</option>
                        <option class="option" value="Bhimber">Bhimber</option>
                        <option class="option" value="Birote">Birote</option>
                        <option class="option" value="Buner">Buner</option>
                        <option class="option" value="Burj">Burj</option>
                        <option class="option" value="Chiniot">Chiniot</option>
                        <option class="option" value="Chachro">Chachro</option>
                        <option class="option" value="Chagai">Chagai</option>
                        <option class="option" value="Chah Sandan">Chah Sandan</option>
                        <option class="option" value="Chailianwala">Chailianwala</option>
                        <option class="option" value="Chakdara">Chakdara</option>
                        <option class="option" value="Chakku">Chakku</option>
                        <option class="option" value="Chakwal">Chakwal</option>
                        <option class="option" value="Chaman">Chaman</option>
                        <option class="option" value="Charsadda">Charsadda</option>
                        <option class="option" value="Chhatr">Chhatr</option>
                        <option class="option" value="Chichawatni">Chichawatni</option>
                        <option class="option" value="Chitral">Chitral</option>
                        <option class="option" value="Dadu">Dadu</option>
                        <option class="option" value="Dera Ghazi Khan">Dera Ghazi Khan</option>
                        <option class="option" value="Dera Ismail Khan">Dera Ismail Khan</option>
                         <option class="option" value="Dalbandin">Dalbandin</option>
                        <option class="option" value="Dargai">Dargai</option>
                        <option class="option" value="Darya Khan">Darya Khan</option>
                        <option class="option" value="Daska">Daska</option>
                        <option class="option" value="Dera Bugti">Dera Bugti</option>
                        <option class="option" value="Dhana Sar">Dhana Sar</option>
                        <option class="option" value="Digri">Digri</option>
                        <option class="option" value="Dina City|Dina">Dina</option>
                        <option class="option" value="Dinga">Dinga</option>
                        <option class="option" value="Diplo, Pakistan|Diplo">Diplo</option>
                        <option class="option" value="Diwana">Diwana</option>
                        <option class="option" value="Dokri">Dokri</option>
                        <option class="option" value="Drosh">Drosh</option>
                        <option class="option" value="Duki">Duki</option>
                        <option class="option" value="Dushi">Dushi</option>
                        <option class="option" value="Duzab">Duzab</option>
                        <option class="option" value="Faisalabad">Faisalabad</option>
                        <option class="option" value="Fortabbas">Fort Abbas</option>
                        <option class="option" value="Fateh Jang">Fateh Jang</option>
                        <option class="option" value="Ghotki">Ghotki</option>
                        <option class="option" value="Gwadar">Gwadar</option>
                        <option class="option" value="Gujranwala">Gujranwala</option>
                        <option class="option" value="Gujrat">Gujrat</option>
                        <option class="option" value="Gadra">Gadra</option>
                        <option class="option" value="Gajar">Gajar</option>
                        <option class="option" value="Gandava">Gandava</option>
                        <option class="option" value="Garhi Khairo">Garhi Khairo</option>
                        <option class="option" value="Garruck">Garruck</option>
                        <option class="option" value="Ghakhar Mandi">Ghakhar Mandi</option>
                        <option class="option" value="Ghanian">Ghanian</option>
                        <option class="option" value="Ghauspur">Ghauspur</option>
                        <option class="option" value="Ghazluna">Ghazluna</option>
                        <option class="option" value="Girdan">Girdan</option>
                        <option class="option" value="Gulistan">Gulistan</option>
                        <option class="option" value="Gwash">Gwash</option>
                        <option class="option" value="Hyderabad">Hyderabad</option>
                        <option class="option" value="Hala">Hala</option>
                        <option class="option" value="Haripur">Haripur</option>
                        <option class="option" value="Hab Chauki">Hab Chauki</option>
                        <option class="option" value="Hafizabad">Hafizabad</option>
                        <option class="option" value="Hameedabad">Hameedabad</option>
                        <option class="option" value="Hangu">Hangu</option>
                        <option class="option" value="Harnai">Harnai</option>
                        <option class="option" value="Hasilpur">Hasilpur</option>
                        <option class="option" value="Haveli Lakha">Haveli Lakha</option>
                        <option class="option" value="Hinglaj">Hinglaj</option>
                        <option class="option" value="Hoshab">Hoshab</option>
                        <option class="option" value="Islamabad">Islamabad</option>
                        <option class="option" value="Islamkot">Islamkot</option>
                        <option class="option" value="Ispikan">Ispikan</option>
                        <option class="option" value="Jacobabad">Jacobabad</option>
                        <option class="option" value="Jamshoro">Jamshoro</option>
                        <option class="option" value="Jhang">Jhang</option>
                        <option class="option" value="Jhelum">Jhelum</option>
                        <option class="option" value="Jamesabad">Jamesabad</option>
                        <option class="option" value="Jampur">Jampur</option>
                        <option class="option" value="Janghar">Janghar</option>
                        <option class="option" value="Jati, Jati(Mughalbhin)">Jati</option>
                        <option class="option" value="Jauharabad">Jauharabad</option>
                        <option class="option" value="Jhal">Jhal</option>
                        <option class="option" value="Jhal Jhao">Jhal Jhao</option>
                        <option class="option" value="Jhatpat">Jhatpat</option>
                        <option class="option" value="Jhudo">Jhudo</option>
                        <option class="option" value="Jiwani">Jiwani</option>
                        <option class="option" value="Jungshahi">Jungshahi</option>
                        <option class="option" value="Karachi">Karachi</option>
                        <option class="option" value="Kotri">Kotri</option>
                        <option class="option" value="Kalam">Kalam</option>
                        <option class="option" value="Kalandi">Kalandi</option>
                        <option class="option" value="Kalat">Kalat</option>
                        <option class="option" value="Kamalia">Kamalia</option>
                        <option class="option" value="Kamararod">Kamararod</option>
                        <option class="option" value="Kamber">Kamber</option>
                        <option class="option" value="Kamokey">Kamokey</option>
                        <option class="option" value="Kanak">Kanak</option>
                        <option class="option" value="Kandi">Kandi</option>
                        <option class="option" value="Kandiaro">Kandiaro</option>
                        <option class="option" value="Kanpur">Kanpur</option>
                        <option class="option" value="Kapip">Kapip</option>
                        <option class="option" value="Kappar">Kappar</option>
                        <option class="option" value="Karak City">Karak City</option>
                        <option class="option" value="Karodi">Karodi</option>
                        <option class="option" value="Kashmor">Kashmor</option>
                        <option class="option" value="Kasur">Kasur</option>
                        <option class="option" value="Katuri">Katuri</option>
                        <option class="option" value="Keti Bandar">Keti Bandar</option>
                        <option class="option" value="Khairpur">Khairpur</option>
                        <option class="option" value="Khanaspur">Khanaspur</option>
                        <option class="option" value="Khanewal">Khanewal</option>
                        <option class="option" value="Kharan">Kharan</option>
                        <option class="option" value="kharian">kharian</option>
                        <option class="option" value="Khokhropur">Khokhropur</option>
                        <option class="option" value="Khora">Khora</option>
                        <option class="option" value="Khushab">Khushab</option>
                        <option class="option" value="Khuzdar">Khuzdar</option>
                        <option class="option" value="Kikki">Kikki</option>
                        <option class="option" value="Klupro">Klupro</option>
                        <option class="option" value="Kohan">Kohan</option>
                        <option class="option" value="Kohat">Kohat</option>
                        <option class="option" value="Kohistan">Kohistan</option>
                        <option class="option" value="Kohlu">Kohlu</option>
                        <option class="option" value="Korak">Korak</option>
                        <option class="option" value="Korangi">Korangi</option>
                        <option class="option" value="Kot Sarae">Kot Sarae</option>
                        <option class="option" value="Kotli">Kotli</option>
                        <option class="option" value="Lahore">Lahore</option>
                        <option class="option" value="Larkana">Larkana</option>
                        <option class="option" value="Lahri">Lahri</option>
                        <option class="option" value="Lakki Marwat">Lakki Marwat</option>
                        <option class="option" value="Lasbela">Lasbela</option>
                        <option class="option" value="Latamber">Latamber</option>
                        <option class="option" value="Layyah">Layyah</option>
                        <option class="option" value="Leiah">Leiah</option>
                        <option class="option" value="Liari">Liari</option>
                        <option class="option" value="Lodhran">Lodhran</option>
                        <option class="option" value="Loralai">Loralai</option>
                        <option class="option" value="Lower Dir">Lower Dir</option>
                        <option class="option" value="Shadan Lund">Shadan Lund</option>
                        <option class="option" value="Multan">Multan</option>
                        <option class="option" value="Mandi Bahauddin">Mandi Bahauddin</option>
                        <option class="option" value="Mansehra">Mansehra</option>
                        <option class="option" value="Mian Chanu">Mian Chanu</option>
                        <option class="option" value="Mirpur">Mirpur</option>
                        <option class="option" value="Moro, Pakistan|Moro">Moro</option>
                        <option class="option" value="Mardan">Mardan</option>
                        <option class="option" value="Mach">Mach</option>
                        <option class="option" value="Madyan">Madyan</option>
                        <option class="option" value="Malakand">Malakand</option>
                        <option class="option" value="Mand">Mand</option>
                        <option class="option" value="Manguchar">Manguchar</option>
                        <option class="option" value="Mashki Chah">Mashki Chah</option>
                        <option class="option" value="Maslti">Maslti</option>
                        <option class="option" value="Mastuj">Mastuj</option>
                        <option class="option" value="Mastung">Mastung</option>
                        <option class="option" value="Mathi">Mathi</option>
                        <option class="option" value="Matiari">Matiari</option>
                        <option class="option" value="Mehar">Mehar</option>
                        <option class="option" value="Mekhtar">Mekhtar</option>
                        <option class="option" value="Merui">Merui</option>
                        <option class="option" value="Mianwali">Mianwali</option>
                        <option class="option" value="Mianez">Mianez</option>
                        <option class="option" value="Mirpur Batoro">Mirpur Batoro</option>
                        <option class="option" value="Mirpur Khas">Mirpur Khas</option>
                        <option class="option" value="Mirpur Sakro">Mirpur Sakro</option>
                        <option class="option" value="Mithi">Mithi</option>
                        <option class="option" value="Mongora">Mongora</option>
                        <option class="option" value="Murgha Kibzai">Murgha Kibzai</option>
                        <option class="option" value="Muridke">Muridke</option>
                        <option class="option" value="Musa Khel Bazar">Musa Khel Bazar</option>
                        <option class="option" value="Muzaffar Garh">Muzaffar Garh</option>
                        <option class="option" value="Muzaffarabad">Muzaffarabad</option>
                        <option class="option" value="Nawabshah">Nawabshah</option>
                        <option class="option" value="Nazimabad">Nazimabad</option>
                        <option class="option" value="Nowshera">Nowshera</option>
                        <option class="option" value="Nagar Parkar">Nagar Parkar</option>
                        <option class="option" value="Nagha Kalat">Nagha Kalat</option>
                        <option class="option" value="Nal">Nal</option>
                        <option class="option" value="Naokot">Naokot</option>
                        <option class="option" value="Nasirabad">Nasirabad</option>
                        <option class="option" value="Nauroz Kalat">Nauroz Kalat</option>
                        <option class="option" value="Naushara">Naushara</option>
                        <option class="option" value="Nur Gamma">Nur Gamma</option>
                        <option class="option" value="Nushki">Nushki</option>
                        <option class="option" value="Nuttal">Nuttal</option>
                        <option class="option" value="Okara">Okara</option>
                        <option class="option" value="Ormara">Ormara</option>
                        <option class="option" value="Peshawar">Peshawar</option>
                        <option class="option" value="Panjgur">Panjgur</option>
                        <option class="option" value="Pasni City">Pasni City</option>
                        <option class="option" value="Paharpur">Paharpur</option>
                        <option class="option" value="Palantuk">Palantuk</option>
                        <option class="option" value="Pendoo">Pendoo</option>
                        <option class="option" value="Piharak">Piharak</option>
                        <option class="option" value="Pirmahal">Pirmahal</option>
                        <option class="option" value="Pishin">Pishin</option>
                        <option class="option" value="Plandri">Plandri</option>
                        <option class="option" value="Pokran">Pokran</option>
                        <option class="option" value="Pounch">Pounch</option>
                        <option class="option" value="Quetta">Quetta</option>
                        <option class="option" value="Qambar">Qambar</option>
                        <option class="option" value="Qamruddin Karez">Qamruddin Karez</option>
                        <option class="option" value="Qazi Ahmad">Qazi Ahmad</option>
                        <option class="option" value="Qila Abdullah">Qila Abdullah</option>
                        <option class="option" value="Qila Ladgasht">Qila Ladgasht</option>
                        <option class="option" value="Qila Safed">Qila Safed</option>
                        <option class="option" value="Qila Saifullah">Qila Saifullah</option>
                        <option class="option" value="Rawalpindi">Rawalpindi</option>
                        <option class="option" value="Rabwah">Rabwah</option>
                        <option class="option" value="Rahim Yar Khan">Rahim Yar Khan</option>
                        <option class="option" value="Rajan Pur">Rajan Pur</option>
                        <option class="option" value="Rakhni">Rakhni</option>
                        <option class="option" value="Ranipur">Ranipur</option>
                        <option class="option" value="Ratodero">Ratodero</option>
                        <option class="option" value="Rawalakot">Rawalakot</option>
                        <option class="option" value="Renala Khurd">Renala Khurd</option>
                        <option class="option" value="Robat Thana">Robat Thana</option>
                        <option class="option" value="Rodkhan">Rodkhan</option>
                        <option class="option" value="Rohri">Rohri</option>
                        <option class="option" value="Sialkot">Sialkot</option>
                        <option class="option" value="Sadiqabad">Sadiqabad</option>
                        <option class="option" value="Safdar Abad- (Dhaban Singh)">Safdar Abad</option>
                        <option class="option" value="Sahiwal">Sahiwal</option>
                        <option class="option" value="Saidu Sharif">Saidu Sharif</option>
                        <option class="option" value="Saindak">Saindak</option>
                        <option class="option" value="Sakrand">Sakrand</option>
                        <option class="option" value="Sanjawi">Sanjawi</option>
                        <option class="option" value="Sargodha">Sargodha</option>
                        <option class="option" value="Saruna">Saruna</option>
                        <option class="option" value="Shabaz Kalat">Shabaz Kalat</option>
                        <option class="option" value="Shadadkhot">Shadadkhot</option>
                        <option class="option" value="Shahbandar">Shahbandar</option>
                        <option class="option" value="Shahpur">Shahpur</option>
                        <option class="option" value="Shahpur Chakar">Shahpur Chakar</option>
                        <option class="option" value="Shakargarh">Shakargarh</option>
                        <option class="option" value="Shangla">Shangla</option>
                        <option class="option" value="Sharam Jogizai">Sharam Jogizai</option>
                        <option class="option" value="Sheikhupura">Sheikhupura</option>
                        <option class="option" value="Shikarpur">Shikarpur</option>
                        <option class="option" value="Shingar">Shingar</option>
                        <option class="option" value="Shorap">Shorap</option>
                        <option class="option" value="Sibi">Sibi</option>
                        <option class="option" value="Sohawa">Sohawa</option>
                        <option class="option" value="Sonmiani">Sonmiani</option>
                        <option class="option" value="Sooianwala">Sooianwala</option>
                        <option class="option" value="Spezand">Spezand</option>
                        <option class="option" value="Spintangi">Spintangi</option>
                        <option class="option" value="Sui">Sui</option>
                        <option class="option" value="Sujawal">Sujawal</option>
                        <option class="option" value="Sukkur">Sukkur</option>
                        <option class="option" value="Suntsar">Suntsar</option>
                        <option class="option" value="Surab">Surab</option>
                        <option class="option" value="Swabi">Swabi</option>
                        <option class="option" value="Swat">Swat</option>
                        <option class="option" value="Tando Adam">Tando Adam</option>
                        <option class="option" value="Tando Bago">Tando Bago</option>
                        <option class="option" value="Tangi">Tangi</option>
                        <option class="option" value="Tank City">Tank City</option>
                        <option class="option" value="Tar Ahamd Rind">Tar Ahamd Rind</option>
                        <option class="option" value="Thalo">Thalo</option>
                        <option class="option" value="Thatta">Thatta</option>
                        <option class="option" value="Toba Tek Singh">Toba Tek Singh</option>
                        <option class="option" value="Tordher">Tordher</option>
                        <option class="option" value="Tujal">Tujal</option>
                        <option class="option" value="Tump">Tump</option>
                        <option class="option" value="Turbat">Turbat</option>
                        <option class="option" value="Umarao">Umarao</option>
                        <option class="option" value="Umarkot">Umarkot</option>
                        <option class="option" value="Upper Dir">Upper Dir</option>
                        <option class="option" value="Uthal">Uthal</option>
                        <option class="option" value="Vehari">Vehari</option>
                        <option class="option" value="Veirwaro">Veirwaro</option>
                        <option class="option" value="Vitakri">Vitakri</option>
                        <option class="option" value="Wadh">Wadh</option>
                        <option class="option" value="Wah Cantt">Wah Cantt</option>
                        <option class="option" value="Warah">Warah</option>
                        <option class="option" value="Washap">Washap</option>
                        <option class="option" value="Wasjuk">Wasjuk</option>
                        <option class="option" value="Wazirabad">Wazirabad</option>
                        <option class="option" value="Yakmach">Yakmach</option>
                        <option class="option" value="Zhob">Zhob</option>
                        <option class="option" value="Other">Other</option>
                                        </select>
                                </div>
                                <!-- .col -->
                                <div class="col-sm-3">
                                    <label for="listing-btn" class="listing-search__label">Advanced Search</label>
                                    <button type="submit"  class="header__cta">Search</button>
                                </div>
                                {{-- <div class="col-sm-3">
                                    <label for="listing-btn" class="listing-search__label listing-search__label--hidden">Advanced Search</label>
                                    <a href="#" id="listing-btn" class="listing-search__btn">Advanced Search</a>
                                </div> --}}
                                <!-- .col -->
                            </div>
                            <!-- row -->
                            {{-- <div class="listing-search__advance">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="keywords" class="listing-search__label">Keyword</label>
                                        <input type="text" id="keywords" class="listing-search__field" placeholder="Type your keywords...">
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="min-price" class="listing-search__label">Min Price</label>
                                        <select name="min-price" id="min-price" class="ht-field">
                                                <option value="Unlimited" selected>Unlimited</option>
                                                <option value="$1,000">$1,000</option>
                                                <option value="$10,000">$10,000</option>
                                                <option value="$20,000">$20,000</option>
                                                <option value="$50,000">$50,000</option>
                                                <option value="$100,000">$100,000</option>
                                            </select>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="bedrooms" class="listing-search__label">Bedrooms</label>
                                        <select name="bedrooms" id="bedrooms" class="ht-field">
                                                <option value="Any" selected>Any</option>
                                                <option value="1">01 bedroom(s)</option>
                                                <option value="2">02 bedroom(s)</option>
                                                <option value="3">03 bedroom(s)</option>
                                                <option value="4">04 bedroom(s)</option>
                                                <option value="5">05 bedroom(s)</option>
                                                <option value="6">06 bedroom(s)</option>
                                                <option value="7">07 bedroom(s)</option>
                                            </select>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="bathrooms" class="listing-search__label">Bathrooms</label>
                                        <select name="bathrooms" id="bathrooms" class="ht-field">
                                                <option value="Any" selected>Any</option>
                                                <option value="1">01 bathroom(s)</option>
                                                <option value="2">02 bathroom(s)</option>
                                                <option value="3">03 bathroom(s)</option>
                                                <option value="4">04 bathroom(s)</option>
                                                <option value="5">05 bathroom(s)</option>
                                                <option value="6">06 bathroom(s)</option>
                                                <option value="7">07 bathroom(s)</option>
                                            </select>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="property-size" class="listing-search__label">Property Size</label>
                                        <div class="listing-search__amount">
                                            <label for="property-amount">Sq.Ft</label>
                                            <span id="property-amount"></span>
                                        </div>
                                        <!-- listing-search__amount -->
                                        <div id="property-size" class="listing-search__slider listing-search__property-size"></div>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="lot-size" class="listing-search__label">Lot Size</label>
                                        <div class="listing-search__amount">
                                            <label for="lot-amount">Sq.Ft</label>
                                            <span id="lot-amount"></span>
                                        </div>
                                        <!-- .listing-search__amount -->
                                        <div id="lot-size" class="listing-search__slider listing-search__lot-size"></div>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="garages" class="listing-search__label">Garages</label>
                                        <select name="garages" id="garages" class="ht-field">
                                                <option value="Any" selected>Any</option>
                                                <option value="1">01 garage(s)</option>
                                                <option value="2">02 garage(s)</option>
                                                <option value="3">03 garage(s)</option>
                                                <option value="4">04 garage(s)</option>
                                            </select>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-sm-3">
                                        <label for="tenure" class="listing-search__label">Tenure</label>
                                        <select name="tenure" id="tenure" class="ht-field">
                                                <option value="Any" selected>Any</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                    </div>
                                    <!-- .col -->
                                </div>
                                <!-- .row -->
                                <div class="listing-search__more">
                                    <a class="listing-search__more-btn" href="#">Additional Features</a>
                                    <div class="listing-search__more-inner">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-3">
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="parking-garage" id="parking-garage" class="listing-search__more-field">
                                                    <label for="parking-garage" class="listing-search__more-label">Parking/Garage (26)</label>
                                                </div>
                                                <!-- .listing-search__more-wrapper -->
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="balcony-terrace" id="balcony-terrace" class="listing-search__more-field">
                                                    <label for="balcony-terrace" class="listing-search__more-label">Balcony/Terrace (24)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="garden" id="garden" class="listing-search__more-field">
                                                    <label for="garden" class="listing-search__more-label">Garden (26)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="porter-security" id="porter-security" class="listing-search__more-field">
                                                    <label for="porter-security" class="listing-search__more-label">Porter/Security (24)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="fireplace" id="fireplace" class="listing-search__more-field">
                                                    <label for="fireplace" class="listing-search__more-label">Fireplace (23)</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-3">
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="rural-secluded" id="rural-secluded" class="listing-search__more-field">
                                                    <label for="rural-secluded" class="listing-search__more-label">Rural/Secluded (21)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="air-conditioning" id="air-conditioning" class="listing-search__more-field">
                                                    <label for="air-conditioning" class="listing-search__more-label">Air Conditioning (22)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="lawn" id="lawn" class="listing-search__more-field">
                                                    <label for="lawn" class="listing-search__more-label">Lawn (4)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="swimming-pool" id="swimming-pool" class="listing-search__more-field">
                                                    <label for="swimming-pool" class="listing-search__more-label">Swimming Pool (4)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="barbecue" id="barbecue" class="listing-search__more-field">
                                                    <label for="barbecue" class="listing-search__more-label">Barbecue (23)</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-3">
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="microwave" id="microwave" class="listing-search__more-field">
                                                    <label for="microwave" class="listing-search__more-label">Microwave (24)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="tv-cable" id="tv-cable" class="listing-search__more-field">
                                                    <label for="tv-cable" class="listing-search__more-label">TV Cable (5)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="washer" id="washer" class="listing-search__more-field">
                                                    <label for="washer" class="listing-search__more-label">Washer (24)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="outdoor-shower" id="outdoor-shower" class="listing-search__more-field">
                                                    <label for="outdoor-shower" class="listing-search__more-label">Outdoor Shower (22)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="gym" id="gym" class="listing-search__more-field">
                                                    <label for="gym" class="listing-search__more-label">Gym (4)</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-3">
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="window-coverings" id="window-coverings" class="listing-search__more-field">
                                                    <label for="window-coverings" class="listing-search__more-label">Window Coverings (21)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="dryer" id="dryer" class="listing-search__more-field">
                                                    <label for="dryer" class="listing-search__more-label">Dryer (21)</label>
                                                </div>
                                                <div class="listing-search__more-wrapper">
                                                    <input type="checkbox" name="laundry" id="laundry" class="listing-search__more-field">
                                                    <label for="laundry" class="listing-search__more-label">Laundry (24)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .row -->
                                    </div>
                                    <!-- .listing-search__more-inner -->
                                </div>
                                <!-- .listing-search__more -->
                            </div> --}}
                            <!-- .listing-search__advance -->
                        </form>
                        <!-- .listing-search__form -->
                    </section>
                    <!-- .listing-search -->
                    {{-- <section class="listing-sort">
                        <div class="listing-sort__inner">
                            <ul class="listing-sort__list">
                                <li class="listing-sort__item">
                                    <a href="#" class="listing-sort__link"><i class="fa fa-th-list" aria-hidden="true" class="listing-sort__icon"></i></a>
                                </li>
                                <li class="listing-sort__item">
                                    <a href="#" class="listing-sort__link"><i class="fa fa-th" aria-hidden="true" class="listing-sort__icon"></i></a>
                                </li>
                                <li class="listing-sort__item">
                                    <a href="#" class="listing-sort__link listing-sort__link--active"><i class="fa fa-th-large" aria-hidden="true" class="listing-sort__icon"></i></a>
                                </li>
                            </ul>
                            <span class="listing-sort__result">1-9 of 25 results</span>
                            <p class="listing-sort__sort"> <label for="sort-type" class="listing-sort__label">
                                        <i class="fa fa-sort-amount-asc" aria-hidden="true"></i> Sort by
                                    </label> <select name="sort-type" id="sort-type" class="ht-field listing-sort__field">
                                        <option value="default">Default</option>
                                        <option value="low-price">Price (Low to High)</option>
                                        <option value="high-price">Price (High to Low)</option>
                                        <option value="featured">Featured</option>
                                    </select> </p>
                        </div>
                        <!-- .listing-sort__inner -->
                    </section> --}}
                    <!-- .listing-sort -->
                </div>
                <!-- .main-search__container -->
            </div>
            <!-- .container -->
        </section>
        <!-- .main-search -->
    </section>
    <!-- .main-slider -->
    <section class="item-grid">
        <div class="container">
            <div class="row">
                @foreach($featured_properties as $key=> $property)
                @if( $key <2)

                <div class="col-md-6 item-grid__container">
                    <div class="listing">
                        <div class="item-grid__image-container">
                            <a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}">
                                <div class="item-grid__image-overlay"></div>
                                <!-- .item-grid__image-overlay -->
                            <?php
                            if((array)json_decode($property->uploaded_files,true)){
                                    $value  = (array)json_decode($property->uploaded_files,true)[0];
                                    $src =  '/attachment_property/'.$value[0];
                                }else{
                                    $src =  '/attachment_property/noimage.jpg';
                                }
                            ?>
                                <img src="{{asset($src)}}" style="height:20pc;" alt="BBCLands" class="listing__img">
                                <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                            </a>
                        </div>
                        <!-- .item-grid__image-container -->
                        <div class="item-grid__content-container">
                            <div class="listing__content">
                                <div class="listing__header">
                                    <div class="listing__header-primary">
                                        <h3 class="listing__title"><a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}">{{$property->property_title}}</a></h3>
                                        <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> {{$property->lo_address}}</p>
                                    </div>
                                    <!-- .listing__header-primary -->
                                    <p class="listing__price">
                                        @if($property->country=="pakistan")
                                            <span>Rs.</span>
                                        @endif
                                        {{$property->price}}
                                    </p>
                                </div>
                                <!-- .listing__header -->
                                <div class="listing__details">
                                    <ul class="listing__stats">
                                        {{-- <li>
                                            <span class="listing__figure">5<sup>&plus;</sup></span> Beds
                                        </li> --}}
                                        {{-- <li>
                                            <span class="listing__figure">Type</span> {{$property->property_type}}
                                        </li> --}}
                                        <li>
                                            <span class="listing__figure">{{$property->area}}</span> {{$property->unit}}
                                        </li>
                                    </ul>
                                    <!-- .listing__stats -->
                                    <a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                </div>
                                <!-- .listing__details -->
                            </div>
                            <!-- .listing-content -->
                        </div>
                        <!-- .item-grid__content-container -->
                    </div>
                    <!-- .listing -->
                </div>
                @endif
                @endforeach
                <!-- .col -->
                {{-- <div class="col-md-6 item-grid__container">
                    <div class="listing">
                        <div class="item-grid__image-container">
                            <a href="#">
                                <div class="item-grid__image-overlay"></div>
                                <!-- .item-grid__image-overlay -->
                                <img src="{{ asset('bbclands/img/dream_house_take_away.jpg') }}" alt="BBCLands" class="listing__img">
                                <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                            </a>
                        </div>
                        <!-- item-grid__image-container -->
                        <div class="item-grid__content-container">
                            <div class="listing__content">
                                <div class="listing__header">
                                    <div class="listing__header-primary">
                                        <h3 class="listing__title"><a href="#">House Take Away</a></h3>
                                        <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> 157 West 57th St, 77 - Central Park South, NYC</p>
                                    </div>
                                    <!-- .listing__header-primary -->
                                    <p class="listing__price">$2,285,500</p>
                                </div>
                                <!-- .listing__header -->
                                <div class="listing__details">
                                    <ul class="listing__stats">
                                        <li>
                                            <span class="listing__figure">5<sup>&plus;</sup></span> Beds
                                        </li>
                                        <li>
                                            <span class="listing__figure">3</span> Baths
                                        </li>
                                        <li>
                                            <span class="listing__figure">1,250</span> sq.ft
                                        </li>
                                    </ul>
                                    <!-- .listing__stats -->
                                    <a href="#" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                </div>
                                <!-- .listing__details -->
                            </div>
                            <!-- .listing-content -->
                        </div>
                        <!-- .item-grid__content-container -->
                    </div>
                    <!-- .listing -->
                </div> --}}

            </div>
            <!-- .row -->
            {{-- <div class="item-grid--centered">
                <a href="{{route('properties')}}" class="item-grid__load-more">Load More</a>
            </div> --}}
        </div>
        <!-- .container -->
    </section>
    <!-- .item-grid-2 -->
    <section class="features">
        <div class="features__overlay">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="feature">
                            <img src="{{ asset('bbclands/img/icon_map.png') }}" alt="Map" class="feature__icon">
                            <h3 class="feature__title">Freshest Market Info</h3>
                            <p class="feature__desc">
                                Our extensive database of listings and market info provide the most accurate.</p>
                        </div>
                        <!-- .feature -->
                    </div>
                    <!-- .col -->
                    <div class="col-sm-4">
                        <div class="feature">
                            <img src="{{ asset('bbclands/img/imgicon_search.png') }}" alt="Search" class="feature__icon">
                            <h3 class="feature__title">Top Local Agents</h3>
                            <p class="feature__desc">
                                Our extensive database of listings and market info provide the most accurate.</p>
                        </div>
                        <!-- .feature -->
                    </div>
                    <!-- .col -->
                    <div class="col-sm-4">
                        <div class="feature">
                            <img src="{{ asset('bbclands/img/imgicon_negotiation.png') }}" alt="Negotiation" class="feature__icon">
                            <h3 class="feature__title">Peace of Mind</h3>
                            <p class="feature__desc">
                                Our extensive database of listings and market info provide the most accurate.</p>
                        </div>
                        <!-- .feature -->
                    </div>
                    <!-- .col -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </div>
        <!-- .features__overlay -->
    </section>
    <!-- .features -->
    <section class="featured-listing">
        <div class="container">
            <h2 class="section__title">Featured Listing</h2>
            <div class="row">
                @foreach($featured_properties as $key=> $property)
                @if( $key >=2 && $key <=4)

                <div class="col-md-4 item-grid__container">
                    <div class="listing">
                        <div class="item-grid__image-container">
                            <a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}">
                                <div class="item-grid__image-overlay"></div>
                                <!-- .item-grid__image-overlay -->
                            <?php   $value  = (array)json_decode($property->uploaded_files,true)[0];
                              $src =  '/attachment_property/'.$value[0];
                            ?>
                                <img src="{{asset($src)}}" style="height:15pc;" alt="BBCLands" class="listing__img">
                                <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                            </a>
                        </div>
                        <!-- .item-grid__image-container -->
                        <div class="item-grid__content-container">
                            <div class="listing__content">
                                <div class="listing__header">
                                    <div class="listing__header-primary">
                                        <h3 class="listing__title"><a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}">{{$property->property_title}}</a></h3>
                                        <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> {{$property->lo_address}}</p>
                                    </div>
                                    <!-- .listing__header-primary -->
                                    <p class="listing__price">
                                        @if($property->country=="pakistan")
                                            <span>Rs.</span>
                                        @endif
                                        {{$property->price}}
                                    </p>
                                </div>
                                <!-- .listing__header -->
                                <div class="listing__details">
                                    <ul class="listing__stats">
                                        {{-- <li>
                                            <span class="listing__figure">5<sup>&plus;</sup></span> Beds
                                        </li> --}}
                                        {{-- <li>
                                            <span class="listing__figure">Type</span> {{$property->property_type}}
                                        </li> --}}
                                        <li>
                                            <span class="listing__figure">{{$property->area}}</span> {{$property->unit}}
                                        </li>
                                    </ul>
                                    <!-- .listing__stats -->
                                    <a href="#" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                </div>
                                <!-- .listing__details -->
                            </div>
                            <!-- .listing-content -->
                        </div>
                        <!-- .item-grid__content-container -->
                    </div>
                    <!-- .listing -->
                </div>
                @endif
                @endforeach
                {{-- <div class="col-md-4 featured-listing__container">
                    <div class="listing">
                        <div class="item-grid__image-container">
                            <a href="#">
                                <div class="item-grid__image-overlay"></div>
                                <!-- .item-grid__image-overlay -->
                                <img src="{{ asset('bbclands/img/weston_hightpointe_place.jpg') }}" alt="" class="listing__img">
                                <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                            </a>
                        </div>
                        <!-- .item-grid__image-container -->
                        <div class="item-grid__content-container">
                            <div class="listing__content">
                                <h3 class="listing__title"><a href="#">Weston Hightpointe Place</a></h3>
                                <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> 157 West 57th St, 77 - Central Park South, NYC</p>
                                <p class="listing__price"><a href="#">Request Price</a></p>
                                <div class="listing__details">
                                    <ul class="listing__stats">
                                        <li>
                                            <span class="listing__figure">5<sup>&plus;</sup></span> Beds
                                        </li>
                                        <li>
                                            <span class="listing__figure">3</span> Baths
                                        </li>
                                        <li>
                                            <span class="listing__figure">1,250</span> sq.ft
                                        </li>
                                    </ul>
                                    <!-- .listing__stats -->
                                    <a href="#" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                </div>
                                <!-- .listing__details -->
                            </div>
                            <!-- .listing-content -->
                        </div>
                        <!-- .item-grid__content-container -->
                    </div>
                    <!-- .listing -->
                </div> --}}
                <!-- .col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>
    <!-- .featured-listing -->
    <section class="testimonial">
        <div class="container">
            <div class="testimonial__container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="testimonial__video">
                            <img src="{{ asset('bbclands/img/testimonial_1.jpg') }}" alt="Testimonial" class="testimonial__video-img">
                            <a href="https://www.youtube.com/watch?v=R3xyBG2u1BU&t=26s" data-lity> <i class="fa fa-play-circle-o play-icon" aria-hidden="true"></i> </a>
                            <div class="testimonial__overlay"></div>
                            <!-- .testimonial__overlay -->
                        </div>
                        <!-- .testimonial__video -->
                    </div>
                    <!-- .col -->
                    <div class="col-md-6">
                        <div class="testimonial__content">
                            <h2 class="section__title">2000&plus; Happy Clients</h2>
                            <p class="testimonial__desc">BBC Lands has recently sold our house and home. We have found that it was one of the best investments we could have made, as nothing was a problem to her, and it certainly took the worry of marketing and selling off our mind,
                                as BBCLands had everything under control.</p>
                            <div class="testimonial__customer">
                                <img src="{{ asset('bbclands/img/testimonial_avatar.png') }}" alt="Faraz Khan">
                                <div class="testimonial__customer-profile">
                                    <h4 class="testimonial__customer-name">Faraz Awan</h4>
                                      <p class="testimonial__customer-company">Multan, Pakistan</p>
                                    <img src="{{ asset('bbclands/img/rating.png') }}" alt="">
                                </div>
                                <!-- .testimonial__customer-profile -->
                            </div>
                            <!-- .testimonial__customer -->
                        </div>
                        <!-- .testimonial__content -->
                    </div>
                    <!-- .col -->
                </div>
                <!-- .row -->
            </div>
            <!-- .testimonial__container -->
        </div>
        <!-- .container -->
    </section>
    <!-- .testimonial -->
    <section class="partners">
        <div class="container">
            <ul class="partners__list">
                <li>
                    <img src="{{ asset('bbclands/img/partner1.jpg') }}" alt="Partner">
                </li>
                <li>
                    <img src="{{ asset('bbclands/img/partner2.jpg') }}" alt="Partner">
                </li>
                <li>
                    <img src="{{ asset('bbclands/img/partner3.jpg') }}" alt="Partner">
                </li>
                <li>
                    <img src="{{ asset('bbclands/img/partner4.jpg') }}" alt="Partner">
                </li>
                <li>
                    <img src="{{ asset('bbclands/img/partner5.jpg') }}" alt="Partner">
                </li>
            </ul>
            <!-- .partners__list -->
        </div>
        <!-- .container -->
    </section>
    <!-- .partners -->
    <section class="news">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="news__single">
                        <div class="news__time">
                            <span class="news__date">.</span>
                            <span class="news__my">July, 2018</span>
                        </div>
                        <!-- .news__time -->
                        <div class="news__main">
                            <h3 class="news__title"><a href="#">Releases 2Q 2017 Tokyo, Japan</a></h3>
                            <p class="news__excerpt">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with...</p>
                            {{--<a href="#" class="news__readmore">&plus; Read More</a>--}}
                        </div>
                        <!-- .news__main-->
                    </div>
                    <!-- .news__single -->
                </div>
                <!-- .col -->
                <div class="col-md-6">
                    <div class="news__single">
                        <div class="news__time">
                            <span class="news__date">.</span>
                            <span class="news__my">July, 2018</span>
                        </div>
                        <!-- .news__time -->
                        <div class="news__main">
                            <h3 class="news__title"><a href="#">Releases 2Q 2017 Tokyo, Japan</a></h3>
                            <p class="news__excerpt">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with...</p>
                            {{--<a href="#" class="news__readmore">&plus; Read More</a>--}}
                        </div>
                        <!-- .news__main-->
                    </div>
                    <!-- .news__single -->
                </div>
                <!-- .col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>
    <!-- .news -->
    <section class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-md-6 newsletter__content">
                    <img src="{{ asset('bbclands/img/icon_mail.png') }}" alt="Newsletter" class="newsletter__icon">
                    <div class="newsletter__text-content">
                        <h2 class="newsletter__title">Newsletter</h2>
                        <p class="newsletter__desc">Sign up for our newsletter to get up-to-date from us</p>
                    </div>
                </div>
                <!-- .col -->
                <div class="col-md-6">
                    <form action="index.html" class="newsletter__form">
                        <input type="email" class="newsletter__field" placeholder="Enter Your E-mail">
                        <button type="submit" class="newsletter__submit">Subscribe</button>
                    </form>
                </div>
                <!-- .col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>
    <!-- .newsletter -->
    <!-- Modal -->
<div class="modal fade" id="pdfPopup" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="text-align: center;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

        <a href="docs/BROCHURE.pdf"><img src="img/popupbbclands.jpg"></a>
      </div>

    </div>

  </div>
</div>
                

    @endsection
   @section('footer')
    <!-- .footer__links -->
    <div class="footer__main">
            <div class="container">
                <div class="footer__logo">
                    <h1 class="screen-reader-text">BBCLands</h1>
                    <img src='{{asset("bbclands/img/logo_dark.png")}}' alt="BBCLands">
                </div>
                <!-- .footer__logo -->
                <p class="footer__desc">BBC Lands is made for buying and selling property faster, easier and customized for you.</p>
                <ul class="footer__social">
                    <li>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                    </li>
                </ul>
                <!-- .footer__social -->
            </div>
            <!-- .container -->
        </div>
        <!-- .footer__main -->
        <div class="footer__copyright">
            <div class="container">
                <div class="footer__copyright-inner">
                    <p class="footer__copyright-desc">
                        &copy; 2018 <span class="footer--highlighted">BBCLands</span> All Right Reserved.</p>
                    <ul class="footer__copyright-list">
                        <li>
                            <a href="#">Neighborhood Guide</a>
                        </li>
                        <li>
                            <a href="#">Market Trends</a>
                        </li>
                        <li>
                            <a href="#">Schools</a>
                        </li>
                        <li>
                            <a href="#">Estate Tips</a>
                        </li>
                    </ul>
                </div>
                <!-- .footer__copyright-inner -->
            </div>
            <!-- .container -->
        </div>
        <!-- .footer__copyright -->
    </footer>
     <!-- .footer -->
     <a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>
    <div id="submit-property-wysiwyg-icons">
        <svg xmlns="http://www.w3.org/2000/svg">
                <symbol id="trumbowyg-blockquote" viewbox="0 0 72 72">
                    <path d="M21.3 31.9h-.6c.8-1.2 1.9-2.2 3.4-3.2 2.1-1.4 5-2.7 9.2-3.3l-1.4-8.9c-4.7.7-8.5 2.1-11.7 4-2.4 1.4-4.3 3.1-5.8 4.9-2.3 2.7-3.7 5.7-4.5 8.5-.8 2.8-1 5.4-1 7.5 0 2.3.3 4 .4 4.8 0 .1.1.3.1.4 1.2 5.4 6.1 9.5 11.9 9.5 6.7 0 12.2-5.4 12.2-12.2s-5.5-12-12.2-12zM49.5 31.9h-.6c.8-1.2 1.9-2.2 3.4-3.2 2.1-1.4 5-2.7 9.2-3.3l-1.4-8.9c-4.7.7-8.5 2.1-11.7 4-2.4 1.4-4.3 3.1-5.8 4.9-2.3 2.7-3.7 5.7-4.5 8.5-.8 2.8-1 5.4-1 7.5 0 2.3.3 4 .4 4.8 0 .1.1.3.1.4 1.2 5.4 6.1 9.5 11.9 9.5 6.7 0 12.2-5.4 12.2-12.2s-5.5-12-12.2-12z" />
                </symbol>
                <symbol id="trumbowyg-bold" viewbox="0 0 72 72">
                    <path d="M51.1 37.8c-1.1-1.4-2.5-2.5-4.2-3.3 1.2-.8 2.1-1.8 2.8-3 1-1.6 1.5-3.5 1.5-5.3 0-2-.6-4-1.7-5.8-1.1-1.8-2.8-3.2-4.8-4.1-2-.9-4.6-1.3-7.8-1.3h-16v42h16.3c2.6 0 4.8-.2 6.7-.7 1.9-.5 3.4-1.2 4.7-2.1 1.3-1 2.4-2.4 3.2-4.1.9-1.7 1.3-3.6 1.3-5.7.2-2.5-.5-4.7-2-6.6zM40.8 50.2c-.6.1-1.8.2-3.4.2h-9V38.5h8.3c2.5 0 4.4.2 5.6.6 1.2.4 2 1 2.7 2 .6.9 1 2 1 3.3 0 1.1-.2 2.1-.7 2.9-.5.9-1 1.5-1.7 1.9-.8.4-1.7.8-2.8 1zm2.6-20.4c-.5.7-1.3 1.3-2.5 1.6-.8.3-2.5.4-4.8.4h-7.7V21.6h7.1c1.4 0 2.6 0 3.6.1s1.7.2 2.2.4c1 .3 1.7.8 2.2 1.7.5.9.8 1.8.8 3-.1 1.3-.4 2.2-.9 3z" />
                </symbol>
                <symbol id="trumbowyg-close" viewbox="0 0 72 72">
                    <path d="M57 20.5l-5.4-5.4-15.5 15.5-15.6-15.5-5.4 5.4L30.7 36 15.1 51.5l5.4 5.4 15.6-15.5 15.5 15.5 5.4-5.4L41.5 36z" />
                </symbol>
                <symbol id="trumbowyg-create-link" viewbox="0 0 72 72">
                    <path d="M31.1 48.9l-6.7 6.7c-.8.8-1.6.9-2.1.9s-1.4-.1-2.1-.9L15 50.4c-1.1-1.1-1.1-3.1 0-4.2l6.1-6.1.2-.2 6.5-6.5c-1.2-.6-2.5-.9-3.8-.9-2.3 0-4.6.9-6.3 2.6L11 41.8c-3.5 3.5-3.5 9.2 0 12.7l5.2 5.2c1.7 1.7 4 2.6 6.3 2.6s4.6-.9 6.3-2.6l6.7-6.7c2.5-2.6 3.1-6.7 1.5-10l-5.9 5.9zM38.7 22.5l6.7-6.7c.8-.8 1.6-.9 2.1-.9s1.4.1 2.1.9l5.2 5.2c1.1 1.1 1.1 3.1 0 4.2l-6.1 6.1-.2.2L42 38c1.2.6 2.5.9 3.8.9 2.3 0 4.6-.9 6.3-2.6l6.7-6.7c3.5-3.5 3.5-9.2 0-12.7l-5.2-5.2c-1.7-1.7-4-2.6-6.3-2.6s-4.6.9-6.3 2.6l-6.7 6.7c-2.7 2.7-3.3 6.9-1.7 10.2l6.1-6.1c0 .1 0 .1 0 0z" />
                    <path d="M44.2 30.5c.2-.2.4-.6.4-.9 0-.3-.1-.6-.4-.9l-2.3-2.3c-.3-.2-.6-.4-.9-.4-.3 0-.6.1-.9.4L25.9 40.6c-.2.2-.4.6-.4.9 0 .3.1.6.4.9l2.3 2.3c.2.2.6.4.9.4.3 0 .6-.1.9-.4l14.2-14.2zM49.9 55.4h-8.5v-5h8.5v-8.9h5.2v8.9h8.5v5h-8.5v8.9h-5.2v-8.9z" />
                </symbol>
                <symbol id="trumbowyg-del" viewbox="0 0 72 72">
                    <path d="M45.8 45c0 1-.3 1.9-.9 2.8-.6.9-1.6 1.6-3 2.1s-3.1.8-5 .8c-2.1 0-4-.4-5.7-1.1-1.7-.7-2.9-1.7-3.6-2.7-.8-1.1-1.3-2.6-1.5-4.5l-.1-.8-6.7.6v.9c.1 2.8.9 5.4 2.3 7.6 1.5 2.3 3.5 4 6.1 5.1 2.6 1.1 5.7 1.6 9.4 1.6 2.9 0 5.6-.5 8-1.6 2.4-1.1 4.3-2.7 5.6-4.7 1.3-2 2-4.2 2-6.5 0-1.6-.3-3.1-.9-4.5l-.2-.6H44c0 .1 1.8 2.3 1.8 5.5zM29 28.9c-.8-.8-1.2-1.7-1.2-2.9 0-.7.1-1.3.4-1.9.3-.6.7-1.1 1.4-1.6.6-.5 1.4-.9 2.5-1.1 1.1-.3 2.4-.4 3.9-.4 2.9 0 5 .6 6.3 1.7 1.3 1.1 2.1 2.7 2.4 5.1l.1.9 6.8-.5v-.9c-.1-2.5-.8-4.7-2.1-6.7s-3.2-3.5-5.6-4.5c-2.4-1-5.1-1.5-8.1-1.5-2.8 0-5.3.5-7.6 1.4-2.3 1-4.2 2.4-5.4 4.3-1.2 1.9-1.9 3.9-1.9 6.1 0 1.7.4 3.4 1.2 4.9l.3.5h11.8c-2.3-.9-3.9-1.7-5.2-2.9zm13.3-6.2zM22.7 20.3zM13 34.1h46.1v3.4H13z" />
                </symbol>
                <symbol id="trumbowyg-em" viewbox="0 0 72 72">
                    <path d="M26 57l10.1-42h7.2L33.2 57H26z" />
                </symbol>
                <symbol id="trumbowyg-fullscreen" viewbox="0 0 72 72">
                    <path d="M25.2 7.1H7.1v17.7l6.7-6.5 10.5 10.5 4.5-4.5-10.4-10.5zM47.2 7.1l6.5 6.7-10.5 10.5 4.5 4.5 10.5-10.4 6.7 6.8V7.1zM47.7 43.2l-4.5 4.5 10.4 10.5-6.8 6.7h18.1V47.2l-6.7 6.5zM24.3 43.2L13.8 53.6l-6.7-6.8v18.1h17.7l-6.5-6.7 10.5-10.5z" />
                    <path fill="currentColor" d="M10.7 28.8h18.1V11.2l-6.6 6.4L11.6 7.1l-4.5 4.5 10.5 10.5zM60.8 28.8l-6.4-6.6 10.5-10.6-4.5-4.5-10.5 10.5-6.7-6.9v18.1zM60.4 64.9l4.5-4.5-10.5-10.5 6.9-6.7H43.2v17.6l6.6-6.4zM11.6 64.9l10.5-10.5 6.7 6.9V43.2H11.1l6.5 6.6L7.1 60.4z" />
                </symbol>
                <symbol id="trumbowyg-h1" viewbox="0 0 72 72">
                    <path d="M6.4 14.9h7.4v16.7h19.1V14.9h7.4V57h-7.4V38H13.8v19H6.4V14.9zM47.8 22.5c1.4 0 2.8-.1 4.1-.4 1.3-.2 2.5-.6 3.6-1.2 1.1-.5 2-1.3 2.8-2.1.8-.9 1.3-1.9 1.5-3.2h5.5v41.2h-7.4v-29H47.8v-5.3z" />
                </symbol>
                <symbol id="trumbowyg-h2" viewbox="0 0 72 72">
                    <path d="M1.5 14.9h7.4v16.7H28V14.9h7.4V57H28V38H8.8v19H1.5V14.9zM70.2 56.9H42c0-3.4.9-6.4 2.5-9s3.8-4.8 6.6-6.7c1.3-1 2.7-1.9 4.2-2.9 1.5-.9 2.8-1.9 4-3 1.2-1.1 2.2-2.2 3-3.4.8-1.2 1.2-2.7 1.2-4.3 0-.7-.1-1.5-.3-2.4s-.5-1.6-1-2.4c-.5-.7-1.2-1.3-2.1-1.8-.9-.5-2.1-.7-3.5-.7-1.3 0-2.4.3-3.3.8s-1.6 1.3-2.1 2.2-.9 2-1.2 3.3c-.3 1.3-.4 2.6-.4 4.1h-6.7c0-2.3.3-4.4.9-6.3.6-1.9 1.5-3.6 2.7-5 1.2-1.4 2.7-2.5 4.4-3.3 1.7-.8 3.8-1.2 6.1-1.2 2.5 0 4.6.4 6.3 1.2 1.7.8 3.1 1.9 4.1 3.1 1 1.3 1.8 2.6 2.2 4.1.4 1.5.6 2.9.6 4.2 0 1.6-.3 3.1-.8 4.5-.5 1.3-1.2 2.6-2.1 3.7-.9 1.1-1.8 2.2-2.9 3.1-1.1.9-2.2 1.8-3.4 2.7-1.2.8-2.4 1.6-3.5 2.4-1.2.7-2.3 1.5-3.3 2.2-1 .7-1.9 1.5-2.6 2.3-.7.8-1.3 1.7-1.5 2.6h20.1v5.9z" />
                </symbol>
                <symbol id="trumbowyg-h3" viewbox="0 0 72 72">
                    <path d="M1.4 14.5h7.4v16.7h19.1V14.5h7.4v42.1h-7.4v-19H8.8v19H1.4V14.5zM53.1 32.4c1.1 0 2.2 0 3.3-.2 1.1-.2 2.1-.5 2.9-1 .9-.5 1.6-1.2 2.1-2 .5-.9.8-1.9.8-3.2 0-1.8-.6-3.2-1.8-4.2-1.2-1.1-2.7-1.6-4.6-1.6-1.2 0-2.2.2-3.1.7-.9.5-1.6 1.1-2.2 1.9-.6.8-1 1.7-1.3 2.7-.3 1-.4 2-.4 3.1h-6.7c.1-2 .5-3.9 1.1-5.6.7-1.7 1.6-3.2 2.7-4.4s2.6-2.2 4.2-2.9c1.6-.7 3.5-1.1 5.6-1.1 1.6 0 3.2.2 4.7.7 1.6.5 2.9 1.2 4.2 2.1 1.2.9 2.2 2.1 3 3.4.7 1.4 1.1 3 1.1 4.8 0 2.1-.5 3.9-1.4 5.4-.9 1.6-2.4 2.7-4.4 3.4v.1c2.4.5 4.2 1.6 5.5 3.5 1.3 1.9 2 4.1 2 6.8 0 2-.4 3.7-1.2 5.3-.8 1.6-1.8 2.9-3.2 3.9-1.3 1.1-2.9 1.9-4.7 2.5-1.8.6-3.6.9-5.6.9-2.4 0-4.5-.3-6.3-1s-3.3-1.7-4.5-2.9c-1.2-1.3-2.1-2.8-2.7-4.5-.6-1.8-1-3.7-1-5.9h6.7c-.1 2.5.5 4.6 1.9 6.3 1.3 1.7 3.3 2.5 5.9 2.5 2.2 0 4.1-.6 5.6-1.9 1.5-1.3 2.3-3.1 2.3-5.4 0-1.6-.3-2.9-.9-3.8-.6-.9-1.5-1.7-2.5-2.2-1-.5-2.2-.8-3.4-.9-1.3-.1-2.6-.2-3.9-.1v-5.2z" />
                </symbol>
                <symbol id="trumbowyg-h4" viewbox="0 0 72 72">
                    <path d="M1.5 14.9h7.4v16.7H28V14.9h7.4V57H28V38H8.9v19H1.5V14.9zM70.5 47.2h-5.3V57h-6.4v-9.8H41.2v-6.7l17.7-24.8h6.4v26.2h5.3v5.3zm-24.2-5.3h12.5V23.7h-.1L46.3 41.9z" />
                </symbol>
                <symbol id="trumbowyg-horizontal-rule" viewbox="0 0 72 72">
                    <path d="M9.1 32h54v8h-54z" />
                </symbol>
                <symbol id="trumbowyg-insert-image" viewbox="0 0 72 72">
                    <path d="M64 17v38H8V17h56m8-8H0v54h72V9z" />
                    <path d="M17.5 22C15 22 13 24 13 26.5s2 4.5 4.5 4.5 4.5-2 4.5-4.5-2-4.5-4.5-4.5zM16 50h27L29.5 32zM36 36.2l8.9-8.5L60.2 50H45.9S35.6 35.9 36 36.2z" />
                </symbol>
                <symbol id="trumbowyg-italic" viewbox="0 0 72 72">
                    <path d="M26 57l10.1-42h7.2L33.2 57H26z" />
                </symbol>
                <symbol id="trumbowyg-justify-center" viewbox="0 0 72 72">
                    <path d="M9 14h54v8H9zM9 50h54v8H9zM18 32h36v8H18z" />
                </symbol>
                <symbol id="trumbowyg-justify-full" viewbox="0 0 72 72">
                    <path d="M9 14h54v8H9zM9 50h54v8H9zM9 32h54v8H9z" />
                </symbol>
                <symbol id="trumbowyg-justify-left" viewbox="0 0 72 72">
                    <path d="M9 14h54v8H9zM9 50h54v8H9zM9 32h36v8H9z" />
                </symbol>
                <symbol id="trumbowyg-justify-right" viewbox="0 0 72 72">
                    <path d="M9 14h54v8H9zM9 50h54v8H9zM27 32h36v8H27z" />
                </symbol>
                <symbol id="trumbowyg-link" viewbox="0 0 72 72">
                    <path d="M30.9 49.1l-6.7 6.7c-.8.8-1.6.9-2.1.9s-1.4-.1-2.1-.9l-5.2-5.2c-1.1-1.1-1.1-3.1 0-4.2l6.1-6.1.2-.2 6.5-6.5c-1.2-.6-2.5-.9-3.8-.9-2.3 0-4.6.9-6.3 2.6L10.8 42c-3.5 3.5-3.5 9.2 0 12.7l5.2 5.2c1.7 1.7 4 2.6 6.3 2.6s4.6-.9 6.3-2.6l6.7-6.7C38 50.5 38.6 46.3 37 43l-6.1 6.1zM38.5 22.7l6.7-6.7c.8-.8 1.6-.9 2.1-.9s1.4.1 2.1.9l5.2 5.2c1.1 1.1 1.1 3.1 0 4.2l-6.1 6.1-.2.2-6.5 6.5c1.2.6 2.5.9 3.8.9 2.3 0 4.6-.9 6.3-2.6l6.7-6.7c3.5-3.5 3.5-9.2 0-12.7l-5.2-5.2c-1.7-1.7-4-2.6-6.3-2.6s-4.6.9-6.3 2.6l-6.7 6.7c-2.7 2.7-3.3 6.9-1.7 10.2l6.1-6.1z" />
                    <path d="M44.1 30.7c.2-.2.4-.6.4-.9 0-.3-.1-.6-.4-.9l-2.3-2.3c-.2-.2-.6-.4-.9-.4-.3 0-.6.1-.9.4L25.8 40.8c-.2.2-.4.6-.4.9 0 .3.1.6.4.9l2.3 2.3c.2.2.6.4.9.4.3 0 .6-.1.9-.4l14.2-14.2z" />
                </symbol>
                <symbol id="trumbowyg-ordered-list" viewbox="0 0 72 72">
                    <path d="M27 14h36v8H27zM27 50h36v8H27zM27 32h36v8H27zM11.8 15.8V22h1.8v-7.8h-1.5l-2.1 1 .3 1.3zM12.1 38.5l.7-.6c1.1-1 2.1-2.1 2.1-3.4 0-1.4-1-2.4-2.7-2.4-1.1 0-2 .4-2.6.8l.5 1.3c.4-.3 1-.6 1.7-.6.9 0 1.3.5 1.3 1.1 0 .9-.9 1.8-2.6 3.3l-1 .9V40H15v-1.5h-2.9zM13.3 53.9c1-.4 1.4-1 1.4-1.8 0-1.1-.9-1.9-2.6-1.9-1 0-1.9.3-2.4.6l.4 1.3c.3-.2 1-.5 1.6-.5.8 0 1.2.3 1.2.8 0 .7-.8.9-1.4.9h-.7v1.3h.7c.8 0 1.6.3 1.6 1.1 0 .6-.5 1-1.4 1-.7 0-1.5-.3-1.8-.5l-.4 1.4c.5.3 1.3.6 2.3.6 2 0 3.2-1 3.2-2.4 0-1.1-.8-1.8-1.7-1.9z" />
                </symbol>
                <symbol id="trumbowyg-p" viewbox="0 0 72 72">
                    <path d="M47.8 15.1H30.1c-4.7 0-8.5 3.7-8.5 8.4s3.7 8.4 8.4 8.4v25h7V19.8h3v37.1h4.1V19.8h3.7v-4.7z" />
                </symbol>
                <symbol id="trumbowyg-redo" viewbox="0 0 72 72">
                    <path d="M10.8 51.2c0-5.1 2.1-9.7 5.4-13.1 3.3-3.3 8-5.4 13.1-5.4H46v-12L61.3 36 45.9 51.3V39.1H29.3c-3.3 0-6.4 1.3-8.5 3.5-2.2 2.2-3.5 5.2-3.5 8.5h-6.5z" />
                </symbol>
                <symbol id="trumbowyg-removeformat" viewbox="0 0 72 72">
                    <path d="M58.2 54.6L52 48.5l3.6-3.6 6.1 6.1 6.4-6.4 3.8 3.8-6.4 6.4 6.1 6.1-3.6 3.6-6.1-6.1-6.4 6.4-3.7-3.8 6.4-6.4zM21.7 52.1H50V57H21.7zM18.8 15.2h34.1v6.4H39.5v24.2h-7.4V21.5H18.8v-6.3z" />
                </symbol>
                <symbol id="trumbowyg-strikethrough" viewbox="0 0 72 72">
                    <path d="M45.8 45c0 1-.3 1.9-.9 2.8-.6.9-1.6 1.6-3 2.1s-3.1.8-5 .8c-2.1 0-4-.4-5.7-1.1-1.7-.7-2.9-1.7-3.6-2.7-.8-1.1-1.3-2.6-1.5-4.5l-.1-.8-6.7.6v.9c.1 2.8.9 5.4 2.3 7.6 1.5 2.3 3.5 4 6.1 5.1 2.6 1.1 5.7 1.6 9.4 1.6 2.9 0 5.6-.5 8-1.6 2.4-1.1 4.3-2.7 5.6-4.7 1.3-2 2-4.2 2-6.5 0-1.6-.3-3.1-.9-4.5l-.2-.6H44c0 .1 1.8 2.3 1.8 5.5zM29 28.9c-.8-.8-1.2-1.7-1.2-2.9 0-.7.1-1.3.4-1.9.3-.6.7-1.1 1.4-1.6.6-.5 1.4-.9 2.5-1.1 1.1-.3 2.4-.4 3.9-.4 2.9 0 5 .6 6.3 1.7 1.3 1.1 2.1 2.7 2.4 5.1l.1.9 6.8-.5v-.9c-.1-2.5-.8-4.7-2.1-6.7s-3.2-3.5-5.6-4.5c-2.4-1-5.1-1.5-8.1-1.5-2.8 0-5.3.5-7.6 1.4-2.3 1-4.2 2.4-5.4 4.3-1.2 1.9-1.9 3.9-1.9 6.1 0 1.7.4 3.4 1.2 4.9l.3.5h11.8c-2.3-.9-3.9-1.7-5.2-2.9zm13.3-6.2zM22.7 20.3zM13 34.1h46.1v3.4H13z" />
                </symbol>
                <symbol id="trumbowyg-strong" viewbox="0 0 72 72">
                    <path d="M51.1 37.8c-1.1-1.4-2.5-2.5-4.2-3.3 1.2-.8 2.1-1.8 2.8-3 1-1.6 1.5-3.5 1.5-5.3 0-2-.6-4-1.7-5.8-1.1-1.8-2.8-3.2-4.8-4.1-2-.9-4.6-1.3-7.8-1.3h-16v42h16.3c2.6 0 4.8-.2 6.7-.7 1.9-.5 3.4-1.2 4.7-2.1 1.3-1 2.4-2.4 3.2-4.1.9-1.7 1.3-3.6 1.3-5.7.2-2.5-.5-4.7-2-6.6zM40.8 50.2c-.6.1-1.8.2-3.4.2h-9V38.5h8.3c2.5 0 4.4.2 5.6.6 1.2.4 2 1 2.7 2 .6.9 1 2 1 3.3 0 1.1-.2 2.1-.7 2.9-.5.9-1 1.5-1.7 1.9-.8.4-1.7.8-2.8 1zm2.6-20.4c-.5.7-1.3 1.3-2.5 1.6-.8.3-2.5.4-4.8.4h-7.7V21.6h7.1c1.4 0 2.6 0 3.6.1s1.7.2 2.2.4c1 .3 1.7.8 2.2 1.7.5.9.8 1.8.8 3-.1 1.3-.4 2.2-.9 3z" />
                </symbol>
                <symbol id="trumbowyg-subscript" viewbox="0 0 72 72">
                    <path d="M32 15h7.8L56 57.1h-7.9L44.3 46H27.4l-4 11.1h-7.6L32 15zm-2.5 25.4h12.9L36 22.3h-.2l-6.3 18.1zM58.7 59.9c.6-1.4 2-2.8 4.1-4.4 1.9-1.3 3.1-2.3 3.7-2.9.8-.9 1.3-1.9 1.3-3 0-.9-.2-1.6-.7-2.2-.5-.6-1.2-.9-2.1-.9-1.2 0-2.1.5-2.5 1.4-.3.5-.4 1.4-.5 2.5h-4c.1-1.8.4-3.2 1-4.3 1.1-2.1 3-3.1 5.8-3.1 2.2 0 3.9.6 5.2 1.8 1.3 1.2 1.9 2.8 1.9 4.8 0 1.5-.5 2.9-1.4 4.1-.6.8-1.6 1.7-3 2.6L66 57.7c-1 .7-1.7 1.2-2.1 1.6-.4.3-.7.7-1 1.1H72V64H57.8c0-1.5.3-2.8.9-4.1z" />
                </symbol>
                <symbol id="trumbowyg-superscript" viewbox="0 0 72 72">
                    <path d="M32 15h7.8L56 57.1h-7.9l-4-11.1H27.4l-4 11.1h-7.6L32 15zm-2.5 25.4h12.9L36 22.3h-.2l-6.3 18.1zM49.6 28.8c.5-1.1 1.6-2.3 3.4-3.6 1.5-1.1 2.5-1.9 3-2.4.7-.7 1-1.6 1-2.4 0-.7-.2-1.3-.6-1.8-.4-.5-1-.7-1.7-.7-1 0-1.7.4-2.1 1.1-.2.4-.3 1.1-.4 2.1H49c.1-1.5.3-2.6.8-3.5.9-1.7 2.5-2.6 4.8-2.6 1.8 0 3.2.5 4.3 1.5 1.1 1 1.6 2.3 1.6 4 0 1.3-.4 2.4-1.1 3.4-.5.7-1.3 1.4-2.4 2.2l-1.3 1c-.8.6-1.4 1-1.7 1.3-.3.3-.6.6-.8.9h7.4v3H48.8c0-1.3.3-2.4.8-3.5z" />
                </symbol>
                <symbol id="trumbowyg-table" viewbox="0 0 72 72">
                    <path d="M25.686 51.38v-6.347q0-.462-.297-.76-.298-.297-.761-.297H14.04q-.463 0-.761.297-.298.298-.298.76v6.346q0 .463.298.76.298.298.76.298h10.589q.463 0 .76-.298.298-.297.298-.76zm0-12.692v-6.346q0-.463-.297-.76-.298-.298-.761-.298H14.04q-.463 0-.761.298-.298.297-.298.76v6.346q0 .462.298.76.298.297.76.297h10.589q.463 0 .76-.297.298-.298.298-.76zm16.94 12.691v-6.346q0-.462-.297-.76-.298-.297-.761-.297H30.98q-.463 0-.76.297-.299.298-.299.76v6.346q0 .463.298.76.298.298.761.298h10.588q.463 0 .76-.298.299-.297.299-.76zm-16.94-25.383v-6.345q0-.463-.297-.76-.298-.298-.761-.298H14.04q-.463 0-.761.297-.298.298-.298.76v6.346q0 .463.298.76.298.298.76.298h10.589q.463 0 .76-.298.298-.297.298-.76zm16.94 12.692v-6.346q0-.463-.297-.76-.298-.298-.761-.298H30.98q-.463 0-.76.298-.299.297-.299.76v6.346q0 .462.298.76.298.297.761.297h10.588q.463 0 .76-.297.299-.298.299-.76zm16.94 12.691v-6.346q0-.462-.297-.76-.298-.297-.76-.297H47.92q-.463 0-.76.297-.298.298-.298.76v6.346q0 .463.297.76.298.298.761.298h10.588q.463 0 .761-.298.298-.297.298-.76zm-16.94-25.383v-6.345q0-.463-.297-.76-.298-.298-.761-.298H30.98q-.463 0-.76.297-.299.298-.299.76v6.346q0 .463.298.76.298.298.761.298h10.588q.463 0 .76-.298.299-.297.299-.76zm16.94 12.692v-6.346q0-.463-.297-.76-.298-.298-.76-.298H47.92q-.463 0-.76.298-.298.297-.298.76v6.346q0 .462.297.76.298.297.761.297h10.588q.463 0 .761-.297.298-.298.298-.76zm0-12.692v-6.345q0-.463-.297-.76-.298-.298-.76-.298H47.92q-.463 0-.76.297-.298.298-.298.76v6.346q0 .463.297.76.298.298.761.298h10.588q.463 0 .761-.298.298-.297.298-.76zm4.236-10.576v35.96q0 2.18-1.555 3.734-1.555 1.553-3.739 1.553H14.04q-2.184 0-3.739-1.553-1.555-1.553-1.555-3.735V15.42q0-2.181 1.555-3.735 1.555-1.553 3.739-1.553h44.468q2.184 0 3.739 1.553 1.555 1.554 1.555 3.735z" />
                </symbol>
                <symbol id="trumbowyg-underline" viewbox="0 0 72 72">
                    <path d="M36 35zM15.2 55.9h41.6V59H15.2zM21.1 13.9h6.4v21.2c0 1.2.1 2.5.2 3.7.1 1.3.5 2.4 1 3.4.6 1 1.4 1.8 2.6 2.5 1.1.6 2.7 1 4.8 1 2.1 0 3.7-.3 4.8-1 1.1-.6 2-1.5 2.6-2.5.6-1 .9-2.1 1-3.4.1-1.3.2-2.5.2-3.7V13.9H51v23.3c0 2.3-.4 4.4-1.1 6.1-.7 1.7-1.7 3.2-3 4.4-1.3 1.2-2.9 2-4.7 2.6-1.8.6-3.9.9-6.1.9-2.2 0-4.3-.3-6.1-.9-1.8-.6-3.4-1.5-4.7-2.6-1.3-1.2-2.3-2.6-3-4.4-.7-1.7-1.1-3.8-1.1-6.1V13.9z" />
                </symbol>
                <symbol id="trumbowyg-undo" viewbox="0 0 72 72">
                    <path d="M61.2 51.2c0-5.1-2.1-9.7-5.4-13.1-3.3-3.3-8-5.4-13.1-5.4H26.1v-12L10.8 36l15.3 15.3V39.1h16.7c3.3 0 6.4 1.3 8.5 3.5 2.2 2.2 3.5 5.2 3.5 8.5h6.4z" />
                </symbol>
                <symbol id="trumbowyg-unlink" viewbox="0 0 72 72">
                    <path d="M30.9 49.1l-6.7 6.7c-.8.8-1.6.9-2.1.9s-1.4-.1-2.1-.9l-5.2-5.2c-1.1-1.1-1.1-3.1 0-4.2l6.1-6.1.2-.2 6.5-6.5c-1.2-.6-2.5-.9-3.8-.9-2.3 0-4.6.9-6.3 2.6L10.8 42c-3.5 3.5-3.5 9.2 0 12.7l5.2 5.2c1.7 1.7 4 2.6 6.3 2.6s4.6-.9 6.3-2.6l6.7-6.7C38 50.5 38.6 46.3 37 43l-6.1 6.1zM38.5 22.7l6.7-6.7c.8-.8 1.6-.9 2.1-.9s1.4.1 2.1.9l5.2 5.2c1.1 1.1 1.1 3.1 0 4.2l-6.1 6.1-.2.2-6.5 6.5c1.2.6 2.5.9 3.8.9 2.3 0 4.6-.9 6.3-2.6l6.7-6.7c3.5-3.5 3.5-9.2 0-12.7l-5.2-5.2c-1.7-1.7-4-2.6-6.3-2.6s-4.6.9-6.3 2.6l-6.7 6.7c-2.7 2.7-3.3 6.9-1.7 10.2l6.1-6.1z" />
                    <path d="M44.1 30.7c.2-.2.4-.6.4-.9 0-.3-.1-.6-.4-.9l-2.3-2.3c-.2-.2-.6-.4-.9-.4-.3 0-.6.1-.9.4L25.8 40.8c-.2.2-.4.6-.4.9 0 .3.1.6.4.9l2.3 2.3c.2.2.6.4.9.4.3 0 .6-.1.9-.4l14.2-14.2zM41.3 55.8v-5h22.2v5H41.3z" />
                </symbol>
                <symbol id="trumbowyg-unordered-list" viewbox="0 0 72 72">
                    <path d="M27 14h36v8H27zM27 50h36v8H27zM9 50h9v8H9zM9 32h9v8H9zM9 14h9v8H9zM27 32h36v8H27z" />
                </symbol>
                <symbol id="trumbowyg-view-html" viewbox="0 0 72 72">
                    <path fill="none" stroke="currentColor" stroke-width="8" stroke-miterlimit="10" d="M26.9 17.9L9 36.2 26.9 54M45 54l17.9-18.3L45 17.9" />
                </symbol>
                <symbol id="trumbowyg-base64" viewbox="0 0 72 72">
                    <path d="M64 17v38H8V17h56m8-8H0v54h72V9z" />
                    <path d="M29.9 28.9c-.5-.5-1.1-.8-1.8-.8s-1.4.2-1.9.7c-.5.4-.9 1-1.2 1.6-.3.6-.5 1.3-.6 2.1-.1.7-.2 1.4-.2 1.9l.1.1c.6-.8 1.2-1.4 2-1.8.8-.4 1.7-.5 2.7-.5.9 0 1.8.2 2.6.6.8.4 1.6.9 2.2 1.5.6.6 1 1.3 1.2 2.2.3.8.4 1.6.4 2.5 0 1.1-.2 2.1-.5 3-.3.9-.8 1.7-1.5 2.4-.6.7-1.4 1.2-2.3 1.6-.9.4-1.9.6-3 .6-1.6 0-2.8-.3-3.9-.9-1-.6-1.8-1.4-2.5-2.4-.6-1-1-2.1-1.3-3.4-.2-1.3-.4-2.6-.4-3.9 0-1.3.1-2.6.4-3.8.3-1.3.8-2.4 1.4-3.5.7-1 1.5-1.9 2.5-2.5 1-.6 2.3-1 3.8-1 .9 0 1.7.1 2.5.4.8.3 1.4.6 2 1.1.6.5 1.1 1.1 1.4 1.8.4.7.6 1.5.7 2.5h-4c0-1-.3-1.6-.8-2.1zm-3.5 6.8c-.4.2-.8.5-1 .8-.3.4-.5.8-.6 1.2-.1.5-.2 1-.2 1.5s.1.9.2 1.4c.1.5.4.9.6 1.2.3.4.6.7 1 .9.4.2.9.3 1.4.3.5 0 1-.1 1.3-.3.4-.2.7-.5 1-.9.3-.4.5-.8.6-1.2.1-.5.2-.9.2-1.4 0-.5-.1-1-.2-1.4-.1-.5-.3-.9-.6-1.2-.3-.4-.6-.7-1-.9-.4-.2-.9-.3-1.4-.3-.4 0-.9.1-1.3.3zM36.3 41.3v-3.8l9-12.1H49v12.4h2.7v3.5H49v4.8h-4v-4.8h-8.7zM45 30.7l-5.3 7.2h5.4l-.1-7.2z" />
                </symbol>
                <symbol id="trumbowyg-back-color" viewbox="0 0 72 72">
                    <path d="M36.5 22.3l-6.3 18.1H43l-6.3-18.1z" />
                    <path d="M9 8.9v54.2h54.1V8.9H9zm39.9 48.2L45 46H28.2l-3.9 11.1h-7.6L32.8 15h7.8l16.2 42.1h-7.9z" />
                </symbol>
                <symbol id="trumbowyg-fore-color" viewbox="0 0 72 72">
                    <path d="M32 15h7.8L56 57.1h-7.9l-4-11.1H27.4l-4 11.1h-7.6L32 15zm-2.5 25.4h12.9L36 22.3h-.2l-6.3 18.1z" />
                </symbol>
                <symbol id="trumbowyg-emoji" viewbox="0 0 72 72">
                    <path d="M36.05 9C21.09 9 8.949 21.141 8.949 36.101c0 14.96 12.141 27.101 27.101 27.101 14.96 0 27.101-12.141 27.101-27.101S51.01 9 36.05 9zm9.757 15.095c2.651 0 4.418 1.767 4.418 4.418s-1.767 4.418-4.418 4.418-4.418-1.767-4.418-4.418 1.767-4.418 4.418-4.418zm-19.479 0c2.651 0 4.418 1.767 4.418 4.418s-1.767 4.418-4.418 4.418-4.418-1.767-4.418-4.418 1.767-4.418 4.418-4.418zm9.722 30.436c-14.093 0-16.261-13.009-16.261-13.009h32.522S50.143 54.531 36.05 54.531z" />
                </symbol>
                <symbol id="trumbowyg-insert-audio" viewbox="0 0 8 8">
                    <path d="M3.344 0L2 2H0v4h2l1.344 2H4V0h-.656zM5 1v1c.152 0 .313.026.469.063H5.5c.86.215 1.5.995 1.5 1.938a1.99 1.99 0 0 1-2 2.001v1a2.988 2.988 0 0 0 3-3 2.988 2.988 0 0 0-3-3zm0 2v2l.25-.031C5.683 4.851 6 4.462 6 4c0-.446-.325-.819-.75-.938v-.031h-.031L5 3z" />
                </symbol>
                <symbol id="trumbowyg-noembed" viewbox="0 0 72 72">
                    <path d="M31.5 33.6V25l11 11-11 11v-8.8z" />
                    <path d="M64 17v38H8V17h56m8-8H0v54h72V9z" />
                </symbol>
                <symbol id="trumbowyg-preformatted" viewbox="0 0 72 72">
                    <path d="M10.3 33.5c.4 0 .9-.1 1.5-.2s1.2-.3 1.8-.7c.6-.3 1.1-.8 1.5-1.3.4-.5.6-1.3.6-2.1V17.1c0-1.4.3-2.6.8-3.6s1.2-1.9 2-2.5c.8-.7 1.6-1.2 2.5-1.5.9-.3 1.6-.5 2.2-.5h5.3v5.3h-3.2c-.7 0-1.3.1-1.8.4-.4.3-.8.6-1 1-.2.4-.4.9-.4 1.3-.1.5-.1.9-.1 1.4v11.4c0 1.2-.2 2.1-.7 2.9-.5.8-1 1.4-1.7 1.8-.6.4-1.3.8-2 1-.7.2-1.3.3-1.7.4v.1c.5 0 1 .1 1.7.3.7.2 1.3.5 2 .9.6.5 1.2 1.1 1.7 1.9.5.8.7 2 .7 3.4v11.1c0 .4 0 .9.1 1.4.1.5.2.9.4 1.3s.6.7 1 1c.4.3 1 .4 1.8.4h3.2V63h-5.3c-.6 0-1.4-.2-2.2-.5-.9-.3-1.7-.8-2.5-1.5s-1.4-1.5-2-2.5c-.5-1-.8-2.2-.8-3.6V43.5c0-.9-.2-1.7-.6-2.3-.4-.6-.9-1.1-1.5-1.5-.6-.4-1.2-.6-1.8-.7-.6-.1-1.1-.2-1.5-.2v-5.3zM61.8 38.7c-.4 0-1 .1-1.6.2-.6.1-1.2.4-1.8.7-.6.3-1.1.7-1.5 1.3-.4.5-.6 1.3-.6 2.1v12.1c0 1.4-.3 2.6-.8 3.6s-1.2 1.9-2 2.5c-.8.7-1.6 1.2-2.5 1.5-.9.3-1.6.5-2.2.5h-5.3v-5.3h3.2c.7 0 1.3-.1 1.8-.4.4-.3.8-.6 1-1 .2-.4.4-.9.4-1.3.1-.5.1-.9.1-1.4V42.3c0-1.2.2-2.1.7-2.9.5-.8 1-1.4 1.7-1.8.6-.4 1.3-.8 2-1 .7-.2 1.3-.3 1.7-.4v-.1c-.5 0-1-.1-1.7-.3-.7-.2-1.3-.5-2-.9-.6-.4-1.2-1.1-1.7-1.9-.5-.8-.7-2-.7-3.4V18.5c0-.4 0-.9-.1-1.4-.1-.5-.2-.9-.4-1.3s-.6-.7-1-1c-.4-.3-1-.4-1.8-.4h-3.2V9.1h5.3c.6 0 1.4.2 2.2.5.9.3 1.7.8 2.5 1.5s1.4 1.5 2 2.5c.5 1 .8 2.2.8 3.6v11.6c0 .9.2 1.7.6 2.3.4.6.9 1.1 1.5 1.5.6.4 1.2.6 1.8.7.6.1 1.2.2 1.6.2v5.2z" />
                </symbol>
                <symbol id="trumbowyg-ruby" viewbox="0 0 72 72">
                    <path d="M16.499 24.477h8.018L41.08 67.5H33l-4.04-11.361H11.804L7.764 67.5H0l16.499-43.023zm-2.65 25.907h13.127l-6.438-18.497h-.177l-6.512 18.497zM65.053 16.685c-6.316 1.178-12.025 1.98-17.126 2.408a362.385 362.385 0 0 0-.965 5.833c-.25 1.57-.679 3.907-1.286 7.013 3.033-1.963 5.852-3.266 8.458-3.907 2.639-.642 4.905-.891 6.797-.75 1.891.108 3.746.661 5.566 1.661 1.82.964 3.264 2.408 4.334 4.334 1.104 1.893 1.427 4.088.965 6.584-.466 2.461-1.554 4.494-3.265 6.101-1.679 1.605-3.658 2.783-5.941 3.532-2.283.785-4.853 1.251-7.707 1.391-2.819.144-5.906.161-9.259.056 0-1.642-.287-3.212-.857-4.71l.108-.59c2.711.5 5.246.768 7.601.802 2.39 0 4.529-.195 6.421-.589 1.927-.393 3.605-1.069 5.031-2.031 1.427-.965 2.319-2.319 2.676-4.067.394-1.75.269-3.229-.373-4.443-.644-1.249-1.446-2.213-2.408-2.891-.929-.68-2.161-1.034-3.693-1.071-1.536-.072-3.265.089-5.192.482-1.927.391-3.82 1.14-5.672 2.248a24.308 24.308 0 0 0-4.978 3.907l-4.872-1.981c1.463-5.031 2.355-8.597 2.677-10.703.321-2.105.642-4.067.963-5.887-3.961.25-7.154.411-9.58.481-.215-1.927-.52-3.534-.91-4.817l.32-.32c3.604.32 7.225.446 10.865.375.214-1.355.481-3.103.804-5.245.354-2.175.407-3.621.16-4.336.034-.784.374-1.017 1.017-.695l5.085.749c.428.251.444.573.055.964l-.857.91c-.537 2.89-.981 5.352-1.338 7.385 4.279-.427 9.312-1.393 15.092-2.89l1.284 4.707" />
                </symbol>
                <symbol id="trumbowyg-upload" viewbox="0 0 72 72">
                    <path d="M64 27v28H8V27H0v36h72V27h-8z" />
                    <path d="M32.1 6.7h8v33.6h-8z" />
                    <path d="M48 35.9L36 49.6 24 36h24z" />
                </symbol>
            </svg>
    </div>
    <!-- JS Files -->


   @endsection
   @section('script')
   <!-- JS Files -->
   <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
    <script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>

    <script src="{{ asset('bbclands/js/plugins.js') }}"></script>
    <script src="{{ asset('bbclands/js/custom.js') }}"></script>


    <script type="text/javascript">
    $(window).on('load',function(){
       // $('#pdfPopup').modal('show');
    });
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        /* $(function(){
        $('#city').empty();
                $("#city").append(`
                <option>select City</option>
                <option class="option" value="Abbottabad">Abbottabad</option>
                <option class="option" value="Adezai">Adezai</option>
                <option class="option" value="Amir Chah">Amir Chah</option>
                <option class="option" value="Attock">Attock</option>
                <option class="option" value="Ayubia">Ayubia</option>
<option class="option" value="Bahawalpur">Bahawalpur</option>
<option class="option" value="Baden">Baden</option>
<option class="option" value="Bagh">Bagh</option>
<option class="option" value="Bahawalnagar">Bahawalnagar</option>
<option class="option" value="Burewala">Burewala</option>
<option class="option" value="Banda Daud Shah">Banda Daud Shah</option>
<option class="option" value="Bannu district|Bannu">Bannu</option>
<option class="option" value="Batagram">Batagram</option>
<option class="option" value="Bazdar">Bazdar</option>
<option class="option" value="Bela">Bela</option>
<option class="option" value="Bellpat">Bellpat</option>
<option class="option" value="Bhag">Bhag</option>
<option class="option" value="Bhakkar">Bhakkar</option>
<option class="option" value="Bhalwal">Bhalwal</option>
<option class="option" value="Bhimber">Bhimber</option>
<option class="option" value="Birote">Birote</option>
<option class="option" value="Buner">Buner</option>
<option class="option" value="Burj">Burj</option>
<option class="option" value="Chiniot">Chiniot</option>
<option class="option" value="Chachro">Chachro</option>
<option class="option" value="Chagai">Chagai</option>
<option class="option" value="Chah Sandan">Chah Sandan</option>
<option class="option" value="Chailianwala">Chailianwala</option>
<option class="option" value="Chakdara">Chakdara</option>
<option class="option" value="Chakku">Chakku</option>
<option class="option" value="Chakwal">Chakwal</option>
<option class="option" value="Chaman">Chaman</option>
<option class="option" value="Charsadda">Charsadda</option>
<option class="option" value="Chhatr">Chhatr</option>
<option class="option" value="Chichawatni">Chichawatni</option>
<option class="option" value="Chitral">Chitral</option>
<option class="option" value="Dadu">Dadu</option>
<option class="option" value="Dera Ghazi Khan">Dera Ghazi Khan</option>
<option class="option" value="Dera Ismail Khan">Dera Ismail Khan</option>
 <option class="option" value="Dalbandin">Dalbandin</option>
<option class="option" value="Dargai">Dargai</option>
<option class="option" value="Darya Khan">Darya Khan</option>
<option class="option" value="Daska">Daska</option>
<option class="option" value="Dera Bugti">Dera Bugti</option>
<option class="option" value="Dhana Sar">Dhana Sar</option>
<option class="option" value="Digri">Digri</option>
<option class="option" value="Dina City|Dina">Dina</option>
<option class="option" value="Dinga">Dinga</option>
<option class="option" value="Diplo, Pakistan|Diplo">Diplo</option>
<option class="option" value="Diwana">Diwana</option>
<option class="option" value="Dokri">Dokri</option>
<option class="option" value="Drosh">Drosh</option>
<option class="option" value="Duki">Duki</option>
<option class="option" value="Dushi">Dushi</option>
<option class="option" value="Duzab">Duzab</option>
<option class="option" value="Faisalabad">Faisalabad</option>
<option class="option" value="Fateh Jang">Fateh Jang</option>
<option class="option" value="Ghotki">Ghotki</option>
<option class="option" value="Gwadar">Gwadar</option>
<option class="option" value="Gujranwala">Gujranwala</option>
<option class="option" value="Gujrat">Gujrat</option>
<option class="option" value="Gadra">Gadra</option>
<option class="option" value="Gajar">Gajar</option>
<option class="option" value="Gandava">Gandava</option>
<option class="option" value="Garhi Khairo">Garhi Khairo</option>
<option class="option" value="Garruck">Garruck</option>
<option class="option" value="Ghakhar Mandi">Ghakhar Mandi</option>
<option class="option" value="Ghanian">Ghanian</option>
<option class="option" value="Ghauspur">Ghauspur</option>
<option class="option" value="Ghazluna">Ghazluna</option>
<option class="option" value="Girdan">Girdan</option>
<option class="option" value="Gulistan">Gulistan</option>
<option class="option" value="Gwash">Gwash</option>
<option class="option" value="Hyderabad">Hyderabad</option>
<option class="option" value="Hala">Hala</option>
<option class="option" value="Haripur">Haripur</option>
<option class="option" value="Hab Chauki">Hab Chauki</option>
<option class="option" value="Hafizabad">Hafizabad</option>
<option class="option" value="Hameedabad">Hameedabad</option>
<option class="option" value="Hangu">Hangu</option>
<option class="option" value="Harnai">Harnai</option>
<option class="option" value="Hasilpur">Hasilpur</option>
<option class="option" value="Haveli Lakha">Haveli Lakha</option>
<option class="option" value="Hinglaj">Hinglaj</option>
<option class="option" value="Hoshab">Hoshab</option>
<option class="option" value="Islamabad">Islamabad</option>
<option class="option" value="Islamkot">Islamkot</option>
<option class="option" value="Ispikan">Ispikan</option>
<option class="option" value="Jacobabad">Jacobabad</option>
<option class="option" value="Jamshoro">Jamshoro</option>
<option class="option" value="Jhang">Jhang</option>
<option class="option" value="Jhelum">Jhelum</option>
<option class="option" value="Jamesabad">Jamesabad</option>
<option class="option" value="Jampur">Jampur</option>
<option class="option" value="Janghar">Janghar</option>
<option class="option" value="Jati, Jati(Mughalbhin)">Jati</option>
<option class="option" value="Jauharabad">Jauharabad</option>
<option class="option" value="Jhal">Jhal</option>
<option class="option" value="Jhal Jhao">Jhal Jhao</option>
<option class="option" value="Jhatpat">Jhatpat</option>
<option class="option" value="Jhudo">Jhudo</option>
<option class="option" value="Jiwani">Jiwani</option>
<option class="option" value="Jungshahi">Jungshahi</option>
<option class="option" value="Karachi">Karachi</option>
<option class="option" value="Kotri">Kotri</option>
<option class="option" value="Kalam">Kalam</option>
<option class="option" value="Kalandi">Kalandi</option>
<option class="option" value="Kalat">Kalat</option>
<option class="option" value="Kamalia">Kamalia</option>
<option class="option" value="Kamararod">Kamararod</option>
<option class="option" value="Kamber">Kamber</option>
<option class="option" value="Kamokey">Kamokey</option>
<option class="option" value="Kanak">Kanak</option>
<option class="option" value="Kandi">Kandi</option>
<option class="option" value="Kandiaro">Kandiaro</option>
<option class="option" value="Kanpur">Kanpur</option>
<option class="option" value="Kapip">Kapip</option>
<option class="option" value="Kappar">Kappar</option>
<option class="option" value="Karak City">Karak City</option>
<option class="option" value="Karodi">Karodi</option>
<option class="option" value="Kashmor">Kashmor</option>
<option class="option" value="Kasur">Kasur</option>
<option class="option" value="Katuri">Katuri</option>
<option class="option" value="Keti Bandar">Keti Bandar</option>
<option class="option" value="Khairpur">Khairpur</option>
<option class="option" value="Khanaspur">Khanaspur</option>
<option class="option" value="Khanewal">Khanewal</option>
<option class="option" value="Kharan">Kharan</option>
<option class="option" value="kharian">kharian</option>
<option class="option" value="Khokhropur">Khokhropur</option>
<option class="option" value="Khora">Khora</option>
<option class="option" value="Khushab">Khushab</option>
<option class="option" value="Khuzdar">Khuzdar</option>
<option class="option" value="Kikki">Kikki</option>
<option class="option" value="Klupro">Klupro</option>
<option class="option" value="Kohan">Kohan</option>
<option class="option" value="Kohat">Kohat</option>
<option class="option" value="Kohistan">Kohistan</option>
<option class="option" value="Kohlu">Kohlu</option>
<option class="option" value="Korak">Korak</option>
<option class="option" value="Korangi">Korangi</option>
<option class="option" value="Kot Sarae">Kot Sarae</option>
<option class="option" value="Kotli">Kotli</option>
<option class="option" value="Lahore">Lahore</option>
<option class="option" value="Larkana">Larkana</option>
<option class="option" value="Lahri">Lahri</option>
<option class="option" value="Lakki Marwat">Lakki Marwat</option>
<option class="option" value="Lasbela">Lasbela</option>
<option class="option" value="Latamber">Latamber</option>
<option class="option" value="Layyah">Layyah</option>
<option class="option" value="Leiah">Leiah</option>
<option class="option" value="Liari">Liari</option>
<option class="option" value="Lodhran">Lodhran</option>
<option class="option" value="Loralai">Loralai</option>
<option class="option" value="Lower Dir">Lower Dir</option>
<option class="option" value="Shadan Lund">Shadan Lund</option>
<option class="option" value="Multan">Multan</option>
<option class="option" value="Mandi Bahauddin">Mandi Bahauddin</option>
<option class="option" value="Mansehra">Mansehra</option>
<option class="option" value="Mian Chanu">Mian Chanu</option>
<option class="option" value="Mirpur">Mirpur</option>
<option class="option" value="Moro, Pakistan|Moro">Moro</option>
<option class="option" value="Mardan">Mardan</option>
<option class="option" value="Mach">Mach</option>
<option class="option" value="Madyan">Madyan</option>
<option class="option" value="Malakand">Malakand</option>
<option class="option" value="Mand">Mand</option>
<option class="option" value="Manguchar">Manguchar</option>
<option class="option" value="Mashki Chah">Mashki Chah</option>
<option class="option" value="Maslti">Maslti</option>
<option class="option" value="Mastuj">Mastuj</option>
<option class="option" value="Mastung">Mastung</option>
<option class="option" value="Mathi">Mathi</option>
<option class="option" value="Matiari">Matiari</option>
<option class="option" value="Mehar">Mehar</option>
<option class="option" value="Mekhtar">Mekhtar</option>
<option class="option" value="Merui">Merui</option>
<option class="option" value="Mianwali">Mianwali</option>
<option class="option" value="Mianez">Mianez</option>
<option class="option" value="Mirpur Batoro">Mirpur Batoro</option>
<option class="option" value="Mirpur Khas">Mirpur Khas</option>
<option class="option" value="Mirpur Sakro">Mirpur Sakro</option>
<option class="option" value="Mithi">Mithi</option>
<option class="option" value="Mongora">Mongora</option>
<option class="option" value="Murgha Kibzai">Murgha Kibzai</option>
<option class="option" value="Muridke">Muridke</option>
<option class="option" value="Musa Khel Bazar">Musa Khel Bazar</option>
<option class="option" value="Muzaffar Garh">Muzaffar Garh</option>
<option class="option" value="Muzaffarabad">Muzaffarabad</option>
<option class="option" value="Nawabshah">Nawabshah</option>
<option class="option" value="Nazimabad">Nazimabad</option>
<option class="option" value="Nowshera">Nowshera</option>
<option class="option" value="Nagar Parkar">Nagar Parkar</option>
<option class="option" value="Nagha Kalat">Nagha Kalat</option>
<option class="option" value="Nal">Nal</option>
<option class="option" value="Naokot">Naokot</option>
<option class="option" value="Nasirabad">Nasirabad</option>
<option class="option" value="Nauroz Kalat">Nauroz Kalat</option>
<option class="option" value="Naushara">Naushara</option>
<option class="option" value="Nur Gamma">Nur Gamma</option>
<option class="option" value="Nushki">Nushki</option>
<option class="option" value="Nuttal">Nuttal</option>
<option class="option" value="Okara">Okara</option>
<option class="option" value="Ormara">Ormara</option>
<option class="option" value="Peshawar">Peshawar</option>
<option class="option" value="Panjgur">Panjgur</option>
<option class="option" value="Pasni City">Pasni City</option>
<option class="option" value="Paharpur">Paharpur</option>
<option class="option" value="Palantuk">Palantuk</option>
<option class="option" value="Pendoo">Pendoo</option>
<option class="option" value="Piharak">Piharak</option>
<option class="option" value="Pirmahal">Pirmahal</option>
<option class="option" value="Pishin">Pishin</option>
<option class="option" value="Plandri">Plandri</option>
<option class="option" value="Pokran">Pokran</option>
<option class="option" value="Pounch">Pounch</option>
<option class="option" value="Quetta">Quetta</option>
<option class="option" value="Qambar">Qambar</option>
<option class="option" value="Qamruddin Karez">Qamruddin Karez</option>
<option class="option" value="Qazi Ahmad">Qazi Ahmad</option>
<option class="option" value="Qila Abdullah">Qila Abdullah</option>
<option class="option" value="Qila Ladgasht">Qila Ladgasht</option>
<option class="option" value="Qila Safed">Qila Safed</option>
<option class="option" value="Qila Saifullah">Qila Saifullah</option>
<option class="option" value="Rawalpindi">Rawalpindi</option>
<option class="option" value="Rabwah">Rabwah</option>
<option class="option" value="Rahim Yar Khan">Rahim Yar Khan</option>
<option class="option" value="Rajan Pur">Rajan Pur</option>
<option class="option" value="Rakhni">Rakhni</option>
<option class="option" value="Ranipur">Ranipur</option>
<option class="option" value="Ratodero">Ratodero</option>
<option class="option" value="Rawalakot">Rawalakot</option>
<option class="option" value="Renala Khurd">Renala Khurd</option>
<option class="option" value="Robat Thana">Robat Thana</option>
<option class="option" value="Rodkhan">Rodkhan</option>
<option class="option" value="Rohri">Rohri</option>
<option class="option" value="Sialkot">Sialkot</option>
<option class="option" value="Sadiqabad">Sadiqabad</option>
<option class="option" value="Safdar Abad- (Dhaban Singh)">Safdar Abad</option>
<option class="option" value="Sahiwal">Sahiwal</option>
<option class="option" value="Saidu Sharif">Saidu Sharif</option>
<option class="option" value="Saindak">Saindak</option>
<option class="option" value="Sakrand">Sakrand</option>
<option class="option" value="Sanjawi">Sanjawi</option>
<option class="option" value="Sargodha">Sargodha</option>
<option class="option" value="Saruna">Saruna</option>
<option class="option" value="Shabaz Kalat">Shabaz Kalat</option>
<option class="option" value="Shadadkhot">Shadadkhot</option>
<option class="option" value="Shahbandar">Shahbandar</option>
<option class="option" value="Shahpur">Shahpur</option>
<option class="option" value="Shahpur Chakar">Shahpur Chakar</option>
<option class="option" value="Shakargarh">Shakargarh</option>
<option class="option" value="Shangla">Shangla</option>
<option class="option" value="Sharam Jogizai">Sharam Jogizai</option>
<option class="option" value="Sheikhupura">Sheikhupura</option>
<option class="option" value="Shikarpur">Shikarpur</option>
<option class="option" value="Shingar">Shingar</option>
<option class="option" value="Shorap">Shorap</option>
<option class="option" value="Sibi">Sibi</option>
<option class="option" value="Sohawa">Sohawa</option>
<option class="option" value="Sonmiani">Sonmiani</option>
<option class="option" value="Sooianwala">Sooianwala</option>
<option class="option" value="Spezand">Spezand</option>
<option class="option" value="Spintangi">Spintangi</option>
<option class="option" value="Sui">Sui</option>
<option class="option" value="Sujawal">Sujawal</option>
<option class="option" value="Sukkur">Sukkur</option>
<option class="option" value="Suntsar">Suntsar</option>
<option class="option" value="Surab">Surab</option>
<option class="option" value="Swabi">Swabi</option>
<option class="option" value="Swat">Swat</option>
<option class="option" value="Tando Adam">Tando Adam</option>
<option class="option" value="Tando Bago">Tando Bago</option>
<option class="option" value="Tangi">Tangi</option>
<option class="option" value="Tank City">Tank City</option>
<option class="option" value="Tar Ahamd Rind">Tar Ahamd Rind</option>
<option class="option" value="Thalo">Thalo</option>
<option class="option" value="Thatta">Thatta</option>
<option class="option" value="Toba Tek Singh">Toba Tek Singh</option>
<option class="option" value="Tordher">Tordher</option>
<option class="option" value="Tujal">Tujal</option>
<option class="option" value="Tump">Tump</option>
<option class="option" value="Turbat">Turbat</option>
<option class="option" value="Umarao">Umarao</option>
<option class="option" value="Umarkot">Umarkot</option>
<option class="option" value="Upper Dir">Upper Dir</option>
<option class="option" value="Uthal">Uthal</option>
<option class="option" value="Vehari">Vehari</option>
<option class="option" value="Veirwaro">Veirwaro</option>
<option class="option" value="Vitakri">Vitakri</option>
<option class="option" value="Wadh">Wadh</option>
<option class="option" value="Wah Cantt">Wah Cantt</option>
<option class="option" value="Warah">Warah</option>
<option class="option" value="Washap">Washap</option>
<option class="option" value="Wasjuk">Wasjuk</option>
<option class="option" value="Wazirabad">Wazirabad</option>
<option class="option" value="Yakmach">Yakmach</option>
<option class="option" value="Zhob">Zhob</option>
<option class="option" value="Other">Other</option>
`);
               }
        ) */
    </script>
    @endsection
