@extends('user.layouts.apps')
@section('css')
 <!-- External Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- CSS files -->
   <link rel="stylesheet" href="{{ asset('bbclands/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('bbclands/css/style.css') }}">
@endsection
@section('content')
<section class="my-profile">
        <div class="container">
            <ul class="ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border">
                <li class="ht-breadcrumbs__item">
                    <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Home</span></a>
                </li>
                <li class="ht-breadcrumbs__item">
                    <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Pages</span></a>
                </li>
                <li class="ht-breadcrumbs__item">
                    <span class="ht-breadcrumbs__page">My Profile</span>
                </li>
            </ul>
            <!-- .ht-breadcrumb -->
            <div class="my-profile__container">
                <br><br><br>
                <div class="row">
                    <div class="col-md-3">
                        <h2 class="bookmarked-listing__headline">Howdy, <strong>{{ Auth::user()->name }}</strong></h2>
                        <div class="settings-block">
                            <span class="settings-block__title">Manage Account</span>
                            <ul class="settings">
                                <li class="setting setting--current">
                                    <a href="my_profile.html" class="setting__link"><i class="ion-ios-person-outline setting__icon"></i>My Profile</a>
                                </li>
                                {{-- <li class="setting">
                                    <a href="bookmarked_listing.html" class="setting__link"><i class="ion-ios-heart-outline setting__icon"></i>Bookmarked Listings</a>
                                </li> --}}
                            </ul>
                            <!-- settings -->
                        </div>
                        <!-- .settings-block -->
                        <div class="settings-block">
                            <span class="settings-block__title">Manage Listing</span>
                            <ul class="settings">
                                <li class="setting">
                                    <a href="my_property.html" class="setting__link"><i class="ion-ios-home-outline setting__icon"></i>My Property</a>
                                </li>
                                <li class="setting">
                                    <a href="submit_property.html" class="setting__link"><i class="ion-ios-upload-outline setting__icon"></i>Submit New Property</a>
                                </li>
                                {{-- <li class="setting">
                                    <a href="package.html" class="setting__link"><i class="ion-ios-cart-outline setting__icon"></i>My Packages</a>
                                </li> --}}
                            </ul>
                            <!-- settings -->
                        </div>
                        <!-- .settings-block -->
                        <div class="settings-block">
                            <ul class="settings">
                                <li class="setting">
                                    <a href="change_password.html" class="setting__link"><i class="ion-ios-unlocked-outline setting__icon"></i>Change Password</a>
                                </li>
                                <li class="setting">
                                    <a href="index.html" class="setting__link"><i class="ion-ios-undo-outline setting__icon"></i>Log Out</a>
                                </li>
                            </ul>
                            <!-- settings -->
                        </div>
                        <!-- .settings-block -->
                    </div>
                    <!-- .col -->
                    <form action="{{route('submitProfile')}}" method="post">
                            {{ csrf_field() }}
                        <div class="col-md-4">
                            <label for="profile-first-name" class="my-profile__label">Full Name</label>
                            <input type="text" name="name" value="{{ Auth::user()->name }}" class="my-profile__field" id="profile-first-name">
                            @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            {{-- <label for="profile-last-name" class="my-profile__label">Last Name</label>
                            <input type="text" value="Haintheme" class="my-profile__field" id="profile-last-name"> --}}
                            {{-- <label for="profile-title" class="my-profile__label">Agent Title</label>
                            <input type="text" value="Haintheme" class="my-profile__field" id="profile-title"> --}}
                            <label for="profile-number" class="my-profile__label">Phone Number</label>
                            <input type="text" name="phoneno" value="{{ Auth::user()->phoneno }}" class="my-profile__field" id="profile-number">
                            @if ($errors->has('phoneno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phoneno') }}</strong>
                                    </span>
                                @endif
                            <label for="profile-email" class="my-profile__label">Email</label>
                            <input type="email" disabled value="{{ Auth::user()->email }}" class="my-profile__field" id="profile-email">
                            {{-- <label for="profile-twitter" class="my-profile__label">Twitter Address</label>
                            <input type="text" value="https://www.twitter.com/@haintheme" class="my-profile__field" id="profile-twitter">
                            <label for="profile-facebook" class="my-profile__label">Facebook Address</label>
                            <input type="text" value="https://www.facebook.com/haintheme" class="my-profile__field" id="profile-facebook">
                            <label for="profile-linkedin" class="my-profile__label">Linkedin Address</label>
                            <input type="text" value="https://www.linkedin.com/haintheme" class="my-profile__field" id="profile-linkedin"> --}}
                        </div>
                        <!-- .col -->
                        <div class="col-md-5">
                            <label for="profile-introduce" class="my-profile__label">About Me</label>
                            <textarea name="aboutme" id="profile-introduce" rows="20" class="my-profile__field">
                                    {{ Auth::user()->aboutme }}
                            </textarea>
                            @if ($errors->has('aboutme'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('aboutme') }}</strong>
                                    </span>
                            @endif
                            {{-- <label for="profile-avatar" class="my-profile__label">Avatar</label>
                            <div class="my-profile__wrapper">
                                <input type="file">
                                <span><i class="ion-image"></i> Upload Avatar</span>
                            </div> --}}
                            <button type="submit" class="my-profile__submit">Save Changes</button>
                        </div>
                        <!-- .col -->
                    </form>
                </div>
                <!-- .row -->
            </div>
            <!-- .my-profile__container -->
        </div>
        <!-- .container -->
    </section>
@endsection
@section('script')
   <!-- JS Files -->
   <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
    <script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
    
    <script src="{{ asset('bbclands/js/plugins.js') }}"></script>
    <script src="{{ asset('bbclands/js/custom.js') }}"></script>
    <script>
     
    </script>
    @endsection