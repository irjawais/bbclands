@extends('user.layouts.apps')
@section('css')
 <!-- External Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- CSS files -->
   <link rel="stylesheet" href="{{ asset('bbclands/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('bbclands/css/style.css') }}">
@endsection

@section('content')
    <section class="submit-property">
        <div class="container">
            <ul class="ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border">
                <li class="ht-breadcrumbs__item">
                    <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Home</span></a>
                </li>
                <li class="ht-breadcrumbs__item">
                    <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Pages</span></a>
                </li>
                <li class="ht-breadcrumbs__item">
                    <span class="ht-breadcrumbs__page">Submit New Property</span>
                </li>
            </ul>
            <!-- .ht-breadcrumb -->
            <br><br><br>
            <div class="submit-property__container">
                <div class="row">

                        <div class="col-md-3">
                                <h2 class="bookmarked-listing__headline">Howdy, <strong>{{Auth::user()->name}}!</strong></h2>
                                <div class="settings-block">
                                    <span class="settings-block__title">Manage Account</span>
                                    <ul class="settings">
                                        <li class="setting">
                                            <a href="{{route('myProfile')}}" class="setting__link"><i class="ion-ios-person-outline setting__icon"></i>My Profile</a>
                                        </li>
                                       
                                    </ul>
                                    <!-- settings -->
                                </div>
                                <!-- .settings-block -->
                                <div class="settings-block">
                                    <span class="settings-block__title">Manage Listing</span>
                                    <ul class="settings">
                                        <li class="setting">
                                            <a href="{{route('myProperty')}}"  class="setting__link"><i class="ion-ios-home-outline setting__icon"></i>My Property</a>
                                        </li>
                                        <li class="setting setting--current">
                                            <a href="{{route('submit.property')}}" class="setting__link"><i class="ion-ios-upload-outline setting__icon"></i>Submit New Property</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- settings -->
                                </div>
                                <!-- .settings-block -->
                                <div class="settings-block">
                                    <ul class="settings">
                                        {{-- <li class="setting">
                                            <a href="change_password.html" class="setting__link"><i class="ion-ios-unlocked-outline setting__icon"></i>Change Password</a>
                                        </li> --}}
                                        <li class="setting">
                                            <a href="/logout" class="setting__link"><i class="ion-ios-undo-outline setting__icon"></i>Log Out</a>
                                        </li>
                                    </ul>
                                    <!-- settings -->
                                </div>
                                <!-- .settings-block -->
                            </div>

                    <!-- .col -->
                            @if(isset($property))
                            <form action="{{route('submit.editPropertyPost')}}" name="propetyform" method="POST" enctype="multipart/form-data">
                                <input type="hidden" value="{{$property->id or ''}}"  name="property_id" >
                                @else
                            <form action="{{route('submit.property')}}" name="propetyform" method="POST" enctype="multipart/form-data">
                            @endif
                                {{ csrf_field() }}
                        <div class="col-md-9">
                            <div class="submit-property__block">
                                <h3 class="submit-property__headline">Basic Information</h3>
                                <div class="submit-property__group">
                                    <label for="property-title" class="submit-property__label">Property Title *</label>
                                    <input type="text" value="{{$property->property_title or ''}}" class="submit-property__field" name="property_title" id="property-title" placeholder="Any Thing For  Sale" required>
                                </div>
                                <!-- .submit-property__group -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="submit-property__group">
                                            <label for="property-type" class="submit-property__label">Property Type *</label>
                                            <select class="ht-field" id="property-type" name="property_type" required>
                                                    
                                                        <option @if(isset($property->property_type) && $property->property_type=="home") selected @endif value="home">Home</option>
                                                        <option @if(isset($property->property_type) && $property->property_type=="plot") selected @endif value="plot">Plot</option>
                                                        <option @if(isset($property->property_type) && $property->property_type=="commercial") selected @endif value="commercial">Commercial</option>
                                                   
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-6">
                                        <div class="submit-property__group">
                                            <label for="property-offer" class="submit-property__label">Purpose *</label>
                                            <select class="ht-field" id="property-offer" name="purpose" required>
                                                    <option @if(isset($property->purpose) && $property->purpose=="rent") selected @endif value="rent">Rent</option>
                                                    <option @if(isset($property->purpose) && $property->purpose=="sale") selected @endif value="sale">Sale</option>
                                                    <option @if(isset($property->purpose) && $property->purpose=="buy") selected @endif value="buy">Buy</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    {{-- <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-rental-period" class="submit-property__label">Rental Period *</label>
                                            <select class="ht-field" id="property-rental-period" required>
                                                    <option value="monthly">Monthly</option>
                                                    <option value="yearly">Yearly</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col --> --}}
                                </div>
                                <!-- .row -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-price" class="submit-property__label">Property Price *</label>
                                            <input value="{{$property->price or ''}}" type="text" name="price" class="submit-property__field" id="property-price" required>
                                            <span class="submit-property__unit">PKR</span>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-area" class="submit-property__label">Property Area *</label>
                                            <input type="text" value="{{$property->area or ''}}" name="area" class="submit-property__field" id="property-area" required>
                                            <span class="submit-property__unit">Area</span>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <div class="submit-property__group">
                                                <label  for="property-area" class="submit-property__label">Property Area *</label>
                                                {{-- <input type="text" name="unit" class="submit-property__field" id="property-area" required> --}}
                                                <select class="ht-field" name="unit" name="unit" id="unit" style="width:136px;" autocomplete="off">
                                                    <option @if(isset($property->unit) && $property->unit=="quare-feet") selected @endif value="square-feet">Square Feet</option>
                                                    <option @if(isset($property->unit) && $property->unit=="square-yards") selected @endif value="square-yards">Square Yards</option>
                                                    <option @if(isset($property->unit) && $property->unit=="square-meters") selected @endif value="square-meters">Square Meters</option>
                                                    <option @if(isset($property->unit) && $property->unit=="marla") selected @endif value="marla">Marla</option>
                                                    <option @if(isset($property->unit) && $property->unit=="kanal") selected @endif value="kanal">Kanal</option>
                                                </select>
                                                <span class="submit-property__unit">Unit</span>
                                            </div>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    
                                </div>
                                <!-- .row -->
                                <div class="submit-property__group">
                                    <label for="submit-property-wysiwyg" class="submit-property__label">Description *</label>
                                    {{-- <div id="submit-property-wysiwyg"></div> --}}
                                    <textarea rows="5" class="submit-property__field" name="description" placeholder="Description">{{$property->description or ''}}</textarea>
                                </div>
                                <!-- .submit-property__group -->
                            </div>
                            <!-- .submit-property__block -->
                            <div class="submit-property__block">
                                <h3 class="submit-property__headline">Pictures </h3>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="submit-property__group">
                                            <label for="property-featured-image" class="submit-property__label">Featured Image *</label>
                                            <div class="submit-property__upload">
                                                <input name="featured_image" type="file">
                                                <div class="submit-property__upload-inner">
                                                    <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                    <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                                </div>
                                            </div>
                                            <!-- .submit-proeprty__upload -->
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-7">
                                        <div class="submit-property__group">
                                            <label for="property-media" class="submit-property__label">All Images or Video *</label>
                                            <div class="submit-property__upload">
                                                <input name="others_files[]" type="file" multiple>
                                                <div class="submit-property__upload-inner">
                                                    <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                    <span class="submit-property__upload-desc">Drop all images here or click to upload</span>
                                                </div>
                                            </div>
                                            <!-- .submit-proeprty__upload -->
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                </div>
                                <!-- .row -->
                            </div>
                            <div class="submit-property__block">
                                    <h3 class="submit-property__headline">Property Owner Contact</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                                <div class="submit-property__group">
                                                        <label for="property-price" class="submit-property__label">Phone No *</label>
                                                        <input type="text" value="{{$property->owner_phoneno or ''}}" name="owner_phoneno" class="submit-property__field" id="property-price" required>
                                                        <span class="submit-property__unit">Phone No</span>
                                                </div>
                                        </div>
                                        <!-- .col -->
                                        <div class="col-md-4">
                                                <div class="submit-property__group">
                                                        <label for="property-price" class="submit-property__label">Email *</label>
                                                        <input type="text" name="owner_email" value="{{$property->owner_email or ''}}" class="submit-property__field" id="property-price" required>
                                                        <span class="submit-property__unit">Email</span>
                                                    </div>
                                            <!-- .submit-property__group -->
                                        </div>
                                        <div class="col-md-4">
                                                <div class="submit-property__group">
                                                        <label for="property-price" class="submit-property__label">Address *</label>
                                                        <input type="text" name="owner_address"  value="{{$property->owner_address or ''}}" class="submit-property__field" id="property-price" required>
                                                        <span class="submit-property__unit">Address</span>
                                                    </div>
                                            <!-- .submit-property__group -->
                                        </div>
                                        <!-- .col -->
                                    </div>
                                    <!-- .row -->
                                </div>
                            <!-- .submit-property__block -->
                            <div class="submit-property__block">
                                <h3 class="submit-property__headline">Location</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="submit-property__group">
                                            <label for="property-type" class="submit-property__label">Country *</label>
                                            <select class="ht-field" id="countryname" name="country" required>
                                                <option  selected="disable">Select Country</option>    
                                                <option @if(isset($property->country) && $property->country=="pakistan") selected @endif value="pakistan">Pakistan</option>
                                                    <option @if(isset($property->country) && $property->country=="canada") selected @endif id="canada">Canada</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-6">
                                        <div class="submit-property__group">
                                            <label for="property-offer" class="submit-property__label">City *</label>
                                            
                                                <select  id="cityname" name="city" required>
                                                    <option selected="disable">First Choose Country</option>  
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    {{-- <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-rental-period" class="submit-property__label">Rental Period *</label>
                                            <select class="ht-field" id="property-rental-period" required>
                                                    <option value="monthly">Monthly</option>
                                                    <option value="yearly">Yearly</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col --> --}}
                                </div>
                                {{-- <div class="submit-property__group">
                                    <div id="submit-property-map"></div>
                                </div> --}}
                                <!-- .submit-property__group -->
                                <div class="submit-property__group">
                                        <input id="pac-input" type="text" value="{{$property->lo_address or ''}}" class="submit-property__field" class="form-control" name="lo_address" placeholder="Type Map Location">
                                        <br>
                                    <div id="map" style="width:100%;height:300px;"></div>
                                    <div id="infowindow-content">
                                        <img src="" width="16" height="16" id="place-icon">
                                        <span id="place-name" class="title"></span><br>
                                        <span id="place-address"></span>
                                    </div>
                                    {{-- <label for="property-map-address" class="submit-property__label">Google Map Address *</label>
                                    <input type="text" class="submit-property__field" id="roperty-map-address" required> --}}
                                </div>
                                <!-- .submit-property__group -->
                                {{-- <div class="submit-property__group">
                                    <label for="property-address" class="submit-property__label">Friendly Address</label>
                                    <input type="text" class="submit-property__field" id="property-address">
                                </div> --}}
                                <!-- .submit-property__group -->
                                {{-- <div class="row">
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-longtitude" class="submit-property__label">Longtitude</label>
                                            <input type="text" class="submit-property__field" id="property-longtitude">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-latitude" class="submit-property__label">Latitude</label>
                                            <input type="text" class="submit-property__field" id="property__latitude">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-region" class="submit-property__label">Region</label>
                                            <select id="property-region" class="ht-field">
                                                    <option value="AZ">Arizona</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="WY">Wyoming</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WV">West Virginia</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                </div> --}}
                                <!-- .row -->
                            </div>
                            <!-- .submit-property__block -->
                            {{-- <div class="submit-property__block">
                                <h3 class="submit-property__headline">Detailed Information</h3>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-year" class="submit-property__label">Building Year *</label>
                                            <select id="property-year" class="ht-field">
                                                    <option value="1990s">1990 - 1999</option>
                                                    <option value="2000s">2000 - 2009</option>
                                                    <option value="2010s">2010 - 2019</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-bedrooms" class="submit-property__label">Bedrooms *</label>
                                            <select id="property-bedrooms" class="ht-field">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-bathrooms" class="submit-property__label">Bathrooms *</label>
                                            <select id="property-bathrooms" class="ht-field">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-lot-size" class="submit-property__label">Lot size *</label>
                                            <input type="text" class="submit-property__field" id="property-lot-size" required>
                                            <span class="submit-property__unit">sq.ft</span>
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-parking" class="submit-property__label">Parking</label>
                                            <input type="text" class="submit-property__field" id="property-parking">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-cooling" class="submit-property__label">Cooling</label>
                                            <input type="text" class="submit-property__field" id="property-cooling">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-sewer" class="submit-property__label">Sewer</label>
                                            <input type="text" class="submit-property__field" id="property-sewer">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-water" class="submit-property__label">Water</label>
                                            <input type="text" class="submit-property__field" id="">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                    <div class="col-md-4">
                                        <div class="submit-property__group">
                                            <label for="property-exercise-room" class="submit-property__label">Exercise Room</label>
                                            <input type="text" class="submit-property__field" id="property-exercise-room">
                                        </div>
                                        <!-- .submit-property__group -->
                                    </div>
                                    <!-- .col -->
                                </div>
                                <!-- .row -->
                            </div> --}}
                            <!-- .submit-property__block -->
                           {{--  <div class="submit-property__block">
                                <h3 class="submit-property__headline">Other Features</h3>
                                <div class="submit-property__features">
                                    <div class="submit-property__group">
                                        
                                        <div class="submit-property__wrapper">
                                                <input type="checkbox" id="air-conditioning" name="feature[]" class="submit-property__checkbox">
                                                <label for="air-conditioning" class="submit-property__label submit-property__feature"></label>
                                        </div>    
                                      
                                    </div>
                                    <!-- .submit-property__group -->
                                </div>
                                <!-- .submit-property__features -->
                            </div> --}}
                            <!-- .submit-property__block -->
                            {{-- <div class="submit-property__block">
                                <h3 class="submit-property__headline">Plan</h3>
                                <div class="submit-property__group">
                                    <div class="submit-property__upload">
                                        <input type="file" multiple>
                                        <div class="submit-property__upload-inner">
                                            <span class="ion-ios-plus-outline submit-property__icon"></span>
                                            <span class="submit-property__upload-desc">Drop all images here or click to upload</span>
                                        </div>
                                    </div>
                                    <!-- .submit-proeprty__upload -->
                                </div>
                                <!-- .submit-property__group -->
                            </div> --}}
                            <!-- .submit-property__block -->
                            <input id="pac-input" type="hidden" name="lng" value="" class="form-control" >
                            <input id="pac-input" type="hidden" name="lat" value="" class="form-control" >
                            <button type="submit" class="submit-property__submit">Submit</button>
                        </div>
                        <!-- .col -->
                    </form>
                </div>
                <!-- .row -->
            </div>
            <!-- .submit-property__container -->
        </div>
        <!-- .container -->
    </section>
    <!-- .submit-property -->
    <section class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-md-6 newsletter__content">
                    <img src="img/icon_mail.png" alt="Newsletter" class="newsletter__icon">
                    <div class="newsletter__text-content">
                        <h2 class="newsletter__title">Newsletter</h2>
                        <p class="newsletter__desc">Sign up for our newsletter to get up-to-date from us</p>
                    </div>
                </div>
                <!-- .col -->
                <div class="col-md-6">
                    <form action="index.html" class="newsletter__form">
                        <input type="email" class="newsletter__field" placeholder="Enter Your E-mail">
                        <button type="submit" class="newsletter__submit">Subscribe</button>
                    </form>
                </div>
                <!-- .col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>
    <!-- .newsletter -->
    @endsection
   
    @section('script')
    <!-- JS Files -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
      <script src="{{ asset('bbclands/js/plugins.js') }}"></script>
     <script src="{{ asset('bbclands/js/custom.js') }}"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&libraries=places&callback=initMap" async defer></script>

     <script>
        var place ;
         function initMap() {
             var map = new google.maps.Map(document.getElementById('map'), {
                 center: {
                     lat: 30.157458, 
                     lng: 71.52491540000005
                 },
                 zoom: 13
             });
             var card = document.getElementById('pac-card');
             var input = document.getElementById('pac-input');
             var types = document.getElementById('type-selector');
             var strictBounds = document.getElementById('strict-bounds-selector');
     
             map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
     
             var autocomplete = new google.maps.places.Autocomplete(input);
     
             // Bind the map's bounds (viewport) property to the autocomplete object,
             // so that the autocomplete requests use the current map bounds for the
             // bounds option in the request.
             autocomplete.bindTo('bounds', map);
     
             var infowindow = new google.maps.InfoWindow();
             var infowindowContent = document.getElementById('infowindow-content');
             infowindow.setContent(infowindowContent);
             var marker = new google.maps.Marker({
                 map: map,
                 anchorPoint: new google.maps.Point(0, -29)
             });
     
             autocomplete.addListener('place_changed', function() {
                 infowindow.close();
                 marker.setVisible(false);
                 place = autocomplete.getPlace();
                 document.propetyform.lng.value = place.geometry.location.lng();
                 document.propetyform.lat.value = place.geometry.location.lat();
                 console.log(place.geometry.location.lat(),place.geometry.location.lng());
                 if (!place.geometry) {
                     // User entered the name of a Place that was not suggested and
                     // pressed the Enter key, or the Place Details request failed.
                     window.alert("No details available for input: '" + place.name + "'");
                     return;
                 }
     
                 // If the place has a geometry, then present it on a map.
                 if (place.geometry.viewport) {
                     map.fitBounds(place.geometry.viewport);
                 } else {
                     map.setCenter(place.geometry.location);
                     map.setZoom(17); // Why 17? Because it looks good.
                 }
                 marker.setPosition(place.geometry.location);
                 marker.setVisible(true);
     
                 var address = '';
                 if (place.address_components) {
                     address = [
                         (place.address_components[0] && place.address_components[0].short_name || ''),
                         (place.address_components[1] && place.address_components[1].short_name || ''),
                         (place.address_components[2] && place.address_components[2].short_name || '')
                     ].join(' ');
                 }
     
                 infowindowContent.children['place-icon'].src = place.icon;
                 infowindowContent.children['place-name'].textContent = place.name;
                 infowindowContent.children['place-address'].textContent = address;
                 infowindow.open(map, marker);
             });
     
             // Sets a listener on a radio button to change the filter type on Places
             // Autocomplete.
             function setupClickListener(id, types) {
                 var radioButton = document.getElementById(id);
                 radioButton.addEventListener('click', function() {
                     autocomplete.setTypes(types);
                 });
             }
     
             setupClickListener('changetype-all', []);
             setupClickListener('changetype-address', ['address']);
             setupClickListener('changetype-establishment', ['establishment']);
             setupClickListener('changetype-geocode', ['geocode']);
     
             document.getElementById('use-strict-bounds')
                 .addEventListener('click', function() {
                     console.log('Checkbox clicked! New state=' + this.checked);
                     autocomplete.setOptions({
                         strictBounds: this.checked
                     });
                 });
         }
         function DoSubmit(){
             document.propetyform.lng.value = place.geometry.location.lng();
             document.propetyform.lat.value = place.geometry.location.lat();
             return true;
         }
     </script>
   
     <script>
         $(function(){
         $('#countryname').change(function(){ //This event will fire the change event. 
             console.log(this.value);
             if(this.value=="pakistan"){
                $('#cityname').empty();
                 $("#cityname").append(`<option>select City</option><option class="option" value="Abbottabad">Abbottabad</option><option class="option" value="Adezai">Adezai</option><option class="option" value="Ali Bandar">Ali Bandar</option><option class="option" value="Amir Chah">Amir Chah</option><option class="option" value="Attock">Attock</option><option class="option" value="Ayubia">Ayubia</option><option class="option" value="Bahawalpur">Bahawalpur</option><option class="option" value="Baden">Baden</option><option class="option" value="Bagh">Bagh</option><option class="option" value="Bahawalnagar">Bahawalnagar</option><option class="option" value="Burewala">Burewala</option><option class="option" value="Banda Daud Shah">Banda Daud Shah</option><option class="option" value="Bannu district|Bannu">Bannu</option><option class="option" value="Batagram">Batagram</option><option class="option" value="Bazdar">Bazdar</option><option class="option" value="Bela">Bela</option><option class="option" value="Bellpat">Bellpat</option><option class="option" value="Bhag">Bhag</option><option class="option" value="Bhakkar">Bhakkar</option><option class="option" value="Bhalwal">Bhalwal</option><option class="option" value="Bhimber">Bhimber</option><option class="option" value="Birote">Birote</option><option class="option" value="Buner">Buner</option><option class="option" value="Burj">Burj</option><option class="option" value="Chiniot">Chiniot</option><option class="option" value="Chachro">Chachro</option><option class="option" value="Chagai">Chagai</option><option class="option" value="Chah Sandan">Chah Sandan</option><option class="option" value="Chailianwala">Chailianwala</option><option class="option" value="Chakdara">Chakdara</option><option class="option" value="Chakku">Chakku</option><option class="option" value="Chakwal">Chakwal</option><option class="option" value="Chaman">Chaman</option><option class="option" value="Charsadda">Charsadda</option><option class="option" value="Chhatr">Chhatr</option><option class="option" value="Chichawatni">Chichawatni</option><option class="option" value="Chitral">Chitral</option><option class="option" value="Dadu">Dadu</option><option class="option" value="Dera Ghazi Khan">Dera Ghazi Khan</option><option class="option" value="Dera Ismail Khan">Dera Ismail Khan</option> <option class="option" value="Dalbandin">Dalbandin</option><option class="option" value="Dargai">Dargai</option><option class="option" value="Darya Khan">Darya Khan</option><option class="option" value="Daska">Daska</option><option class="option" value="Dera Bugti">Dera Bugti</option><option class="option" value="Dhana Sar">Dhana Sar</option><option class="option" value="Digri">Digri</option><option class="option" value="Dina City|Dina">Dina</option><option class="option" value="Dinga">Dinga</option><option class="option" value="Diplo, Pakistan|Diplo">Diplo</option><option class="option" value="Diwana">Diwana</option><option class="option" value="Dokri">Dokri</option><option class="option" value="Drosh">Drosh</option><option class="option" value="Duki">Duki</option><option class="option" value="Dushi">Dushi</option><option class="option" value="Duzab">Duzab</option><option class="option" value="Faisalabad">Faisalabad</option><option class="option" value="Fateh Jang">Fateh Jang</option><option class="option" value="Ghotki">Ghotki</option><option class="option" value="Gwadar">Gwadar</option><option class="option" value="Gujranwala">Gujranwala</option><option class="option" value="Gujrat">Gujrat</option><option class="option" value="Gadra">Gadra</option><option class="option" value="Gajar">Gajar</option><option class="option" value="Gandava">Gandava</option><option class="option" value="Garhi Khairo">Garhi Khairo</option><option class="option" value="Garruck">Garruck</option><option class="option" value="Ghakhar Mandi">Ghakhar Mandi</option><option class="option" value="Ghanian">Ghanian</option><option class="option" value="Ghauspur">Ghauspur</option><option class="option" value="Ghazluna">Ghazluna</option><option class="option" value="Girdan">Girdan</option><option class="option" value="Gulistan">Gulistan</option><option class="option" value="Gwash">Gwash</option><option class="option" value="Hyderabad">Hyderabad</option><option class="option" value="Hala">Hala</option><option class="option" value="Haripur">Haripur</option><option class="option" value="Hab Chauki">Hab Chauki</option><option class="option" value="Hafizabad">Hafizabad</option><option class="option" value="Hameedabad">Hameedabad</option><option class="option" value="Hangu">Hangu</option><option class="option" value="Harnai">Harnai</option><option class="option" value="Hasilpur">Hasilpur</option><option class="option" value="Haveli Lakha">Haveli Lakha</option><option class="option" value="Hinglaj">Hinglaj</option><option class="option" value="Hoshab">Hoshab</option><option class="option" value="Islamabad">Islamabad</option><option class="option" value="Islamkot">Islamkot</option><option class="option" value="Ispikan">Ispikan</option><option class="option" value="Jacobabad">Jacobabad</option><option class="option" value="Jamshoro">Jamshoro</option><option class="option" value="Jhang">Jhang</option><option class="option" value="Jhelum">Jhelum</option><option class="option" value="Jamesabad">Jamesabad</option><option class="option" value="Jampur">Jampur</option><option class="option" value="Janghar">Janghar</option><option class="option" value="Jati, Jati(Mughalbhin)">Jati</option><option class="option" value="Jauharabad">Jauharabad</option><option class="option" value="Jhal">Jhal</option><option class="option" value="Jhal Jhao">Jhal Jhao</option><option class="option" value="Jhatpat">Jhatpat</option><option class="option" value="Jhudo">Jhudo</option><option class="option" value="Jiwani">Jiwani</option><option class="option" value="Jungshahi">Jungshahi</option><option class="option" value="Karachi">Karachi</option><option class="option" value="Kotri">Kotri</option><option class="option" value="Kalam">Kalam</option><option class="option" value="Kalandi">Kalandi</option><option class="option" value="Kalat">Kalat</option><option class="option" value="Kamalia">Kamalia</option><option class="option" value="Kamararod">Kamararod</option><option class="option" value="Kamber">Kamber</option><option class="option" value="Kamokey">Kamokey</option><option class="option" value="Kanak">Kanak</option><option class="option" value="Kandi">Kandi</option><option class="option" value="Kandiaro">Kandiaro</option><option class="option" value="Kanpur">Kanpur</option><option class="option" value="Kapip">Kapip</option><option class="option" value="Kappar">Kappar</option><option class="option" value="Karak City">Karak City</option><option class="option" value="Karodi">Karodi</option><option class="option" value="Kashmor">Kashmor</option><option class="option" value="Kasur">Kasur</option><option class="option" value="Katuri">Katuri</option><option class="option" value="Keti Bandar">Keti Bandar</option><option class="option" value="Khairpur">Khairpur</option><option class="option" value="Khanaspur">Khanaspur</option><option class="option" value="Khanewal">Khanewal</option><option class="option" value="Kharan">Kharan</option><option class="option" value="kharian">kharian</option><option class="option" value="Khokhropur">Khokhropur</option><option class="option" value="Khora">Khora</option><option class="option" value="Khushab">Khushab</option><option class="option" value="Khuzdar">Khuzdar</option><option class="option" value="Kikki">Kikki</option><option class="option" value="Klupro">Klupro</option><option class="option" value="Kohan">Kohan</option><option class="option" value="Kohat">Kohat</option><option class="option" value="Kohistan">Kohistan</option><option class="option" value="Kohlu">Kohlu</option><option class="option" value="Korak">Korak</option><option class="option" value="Korangi">Korangi</option><option class="option" value="Kot Sarae">Kot Sarae</option><option class="option" value="Kotli">Kotli</option><option class="option" value="Lahore">Lahore</option><option class="option" value="Larkana">Larkana</option><option class="option" value="Lahri">Lahri</option><option class="option" value="Lakki Marwat">Lakki Marwat</option><option class="option" value="Lasbela">Lasbela</option><option class="option" value="Latamber">Latamber</option><option class="option" value="Layyah">Layyah</option><option class="option" value="Leiah">Leiah</option><option class="option" value="Liari">Liari</option><option class="option" value="Lodhran">Lodhran</option><option class="option" value="Loralai">Loralai</option><option class="option" value="Lower Dir">Lower Dir</option><option class="option" value="Shadan Lund">Shadan Lund</option><option class="option" value="Multan">Multan</option><option class="option" value="Mandi Bahauddin">Mandi Bahauddin</option><option class="option" value="Mansehra">Mansehra</option><option class="option" value="Mian Chanu">Mian Chanu</option><option class="option" value="Mirpur">Mirpur</option><option class="option" value="Moro, Pakistan|Moro">Moro</option><option class="option" value="Mardan">Mardan</option><option class="option" value="Mach">Mach</option><option class="option" value="Madyan">Madyan</option><option class="option" value="Malakand">Malakand</option><option class="option" value="Mand">Mand</option><option class="option" value="Manguchar">Manguchar</option><option class="option" value="Mashki Chah">Mashki Chah</option><option class="option" value="Maslti">Maslti</option><option class="option" value="Mastuj">Mastuj</option><option class="option" value="Mastung">Mastung</option><option class="option" value="Mathi">Mathi</option><option class="option" value="Matiari">Matiari</option><option class="option" value="Mehar">Mehar</option><option class="option" value="Mekhtar">Mekhtar</option><option class="option" value="Merui">Merui</option>
                  <option class="option" value="Mianwali">Mianwali</option><option class="option" value="Mianez">Mianez</option><option class="option" value="Mirpur Batoro">Mirpur Batoro</option><option class="option" value="Mirpur Khas">Mirpur Khas</option><option class="option" value="Mirpur Sakro">Mirpur Sakro</option><option class="option" value="Mithi">Mithi</option><option class="option" value="Mongora">Mongora</option><option class="option" value="Murgha Kibzai">Murgha Kibzai</option><option class="option" value="Muridke">Muridke</option><option class="option" value="Musa Khel Bazar">Musa Khel Bazar</option><option class="option" value="Muzaffar Garh">Muzaffar Garh</option><option class="option" value="Muzaffarabad">Muzaffarabad</option><option class="option" value="Nawabshah">Nawabshah</option><option class="option" value="Nazimabad">Nazimabad</option><option class="option" value="Nowshera">Nowshera</option><option class="option" value="Nagar Parkar">Nagar Parkar</option><option class="option" value="Nagha Kalat">Nagha Kalat</option><option class="option" value="Nal">Nal</option><option class="option" value="Naokot">Naokot</option><option class="option" value="Nasirabad">Nasirabad</option><option class="option" value="Nauroz Kalat">Nauroz Kalat</option><option class="option" value="Naushara">Naushara</option><option class="option" value="Nur Gamma">Nur Gamma</option><option class="option" value="Nushki">Nushki</option><option class="option" value="Nuttal">Nuttal</option><option class="option" value="Okara">Okara</option><option class="option" value="Ormara">Ormara</option><option class="option" value="Peshawar">Peshawar</option><option class="option" value="Panjgur">Panjgur</option><option class="option" value="Pasni City">Pasni City</option><option class="option" value="Paharpur">Paharpur</option><option class="option" value="Palantuk">Palantuk</option><option class="option" value="Pendoo">Pendoo</option><option class="option" value="Piharak">Piharak</option><option class="option" value="Pirmahal">Pirmahal</option><option class="option" value="Pishin">Pishin</option><option class="option" value="Plandri">Plandri</option><option class="option" value="Pokran">Pokran</option><option class="option" value="Pounch">Pounch</option><option class="option" value="Quetta">Quetta</option><option class="option" value="Qambar">Qambar</option><option class="option" value="Qamruddin Karez">Qamruddin Karez</option><option class="option" value="Qazi Ahmad">Qazi Ahmad</option><option class="option" value="Qila Abdullah">Qila Abdullah</option><option class="option" value="Qila Ladgasht">Qila Ladgasht</option><option class="option" value="Qila Safed">Qila Safed</option><option class="option" value="Qila Saifullah">Qila Saifullah</option><option class="option" value="Rawalpindi">Rawalpindi</option><option class="option" value="Rabwah">Rabwah</option><option class="option" value="Rahim Yar Khan">Rahim Yar Khan</option><option class="option" value="Rajan Pur">Rajan Pur</option><option class="option" value="Rakhni">Rakhni</option><option class="option" value="Ranipur">Ranipur</option><option class="option" value="Ratodero">Ratodero</option><option class="option" value="Rawalakot">Rawalakot</option><option class="option" value="Renala Khurd">Renala Khurd</option><option class="option" value="Robat Thana">Robat Thana</option><option class="option" value="Rodkhan">Rodkhan</option><option class="option" value="Rohri">Rohri</option><option class="option" value="Sialkot">Sialkot</option><option class="option" value="Sadiqabad">Sadiqabad</option><option class="option" value="Safdar Abad- (Dhaban Singh)">Safdar Abad</option><option class="option" value="Sahiwal">Sahiwal</option><option class="option" value="Saidu Sharif">Saidu Sharif</option><option class="option" value="Saindak">Saindak</option><option class="option" value="Sakrand">Sakrand</option><option class="option" value="Sanjawi">Sanjawi</option><option class="option" value="Sargodha">Sargodha</option><option class="option" value="Saruna">Saruna</option><option class="option" value="Shabaz Kalat">Shabaz Kalat</option><option class="option" value="Shadadkhot">Shadadkhot</option><option class="option" value="Shahbandar">Shahbandar</option><option class="option" value="Shahpur">Shahpur</option><option class="option" value="Shahpur Chakar">Shahpur Chakar</option><option class="option" value="Shakargarh">Shakargarh</option><option class="option" value="Shangla">Shangla</option><option class="option" value="Sharam Jogizai">Sharam Jogizai</option><option class="option" value="Sheikhupura">Sheikhupura</option><option class="option" value="Shikarpur">Shikarpur</option><option class="option" value="Shingar">Shingar</option><option class="option" value="Shorap">Shorap</option><option class="option" value="Sibi">Sibi</option><option class="option" value="Sohawa">Sohawa</option><option class="option" value="Sonmiani">Sonmiani</option><option class="option" value="Sooianwala">Sooianwala</option><option class="option" value="Spezand">Spezand</option><option class="option" value="Spintangi">Spintangi</option><option class="option" value="Sui">Sui</option><option class="option" value="Sujawal">Sujawal</option><option class="option" value="Sukkur">Sukkur</option><option class="option" value="Suntsar">Suntsar</option><option class="option" value="Surab">Surab</option><option class="option" value="Swabi">Swabi</option><option class="option" value="Swat">Swat</option><option class="option" value="Tando Adam">Tando Adam</option><option class="option" value="Tando Bago">Tando Bago</option><option class="option" value="Tangi">Tangi</option><option class="option" value="Tank City">Tank City</option><option class="option" value="Tar Ahamd Rind">Tar Ahamd Rind</option><option class="option" value="Thalo">Thalo</option><option class="option" value="Thatta">Thatta</option><option class="option" value="Toba Tek Singh">Toba Tek Singh</option><option class="option" value="Tordher">Tordher</option><option class="option" value="Tujal">Tujal</option><option class="option" value="Tump">Tump</option><option class="option" value="Turbat">Turbat</option><option class="option" value="Umarao">Umarao</option><option class="option" value="Umarkot">Umarkot</option><option class="option" value="Upper Dir">Upper Dir</option><option class="option" value="Uthal">Uthal</option><option class="option" value="Vehari">Vehari</option><option class="option" value="Veirwaro">Veirwaro</option><option class="option" value="Vitakri">Vitakri</option><option class="option" value="Wadh">Wadh</option><option class="option" value="Wah Cantt">Wah Cantt</option><option class="option" value="Warah">Warah</option><option class="option" value="Washap">Washap</option><option class="option" value="Wasjuk">Wasjuk</option><option class="option" value="Wazirabad">Wazirabad</option><option class="option" value="Yakmach">Yakmach</option><option class="option" value="Zhob">Zhob</option><option class="option" value="Other">Other</option>`);
                }
         });
         });
     </script>
     @endsection