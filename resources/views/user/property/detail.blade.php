@extends('user.layouts.apps')
@section('css')
 <!-- External Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- CSS files -->
   <link rel="stylesheet" href="{{ asset('bbclands/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('bbclands/css/style.css') }}">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
<!-- .header -->
<section class="property">
    <div class="container">
        <ul class="ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border">
            <li class="ht-breadcrumbs__item">
                <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Home</span></a>
            </li>
            <li class="ht-breadcrumbs__item">
                <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Property</span></a>
            </li>
            <li class="ht-breadcrumbs__item">
                <span class="ht-breadcrumbs__page">Single Property V1</span>
            </li>
        </ul>
        <!-- ht-breadcrumb -->
    </div>
    <!-- .container -->
    <div class="property__header">
        <div class="container">
            <br><br><br>
            <div class="property__header-container">
                <ul class="property__main">
                    <li class="property__title property__main-item">
                        <div class="property__meta">
                            <span class="property__offer">For {{$property->purpose}}</span>
                            <span class="property__type">Luxury House</span>
                        </div>
                        <!-- .property__meta -->
                        <h2 class="property__name">{{$property->property_title}}</h2>
                        <span class="property__address"><i class="ion-ios-location-outline property__address-icon"></i>{{$property->lo_address}},<br> {{$property->city}}, Pakistan</span>
                    </li>
                    <li class="property__details property__main-item">
                        <ul class="property__stats">
                            {{-- <li class="property__stat">
                                <span class="property__figure">5<sup>&plus;</sup></span> Beds
                            </li>
                            <li class="property__stat">
                                <span class="property__figure">3</span> Baths
                            </li> --}}
                            <li class="property__stat">
                                <span class="property__figure">{{$property->area}}</span> {{$property->unit}}
                            </li>
                        </ul>
                        <!-- .listing__stats -->
                    </li>
                    <li class="property__price property__main-item">
                        <h4 class="property__price-primary">
                                @if($property->country=="pakistan")
                                <span>Rs.</span>
                                @endif
                                {{$property->price}}
                        </h4>
                        <span class="property__price-secondary">Rs. {{round($property->price/$property->area)}}/{{$property->unit}}</span>
                    </li>
                </ul>
                <!-- .property__main -->
                <ul class="property__list">
                    <li class="property__item">
                        <a href="#" class="property__link"> <i class="fa fa-heart-o property__icon" aria-hidden="true"></i> <span class="property__item-desc">Favorite</span> </a>
                    </li>
                    <li class="property__item">
                        <a href="#" class="property__link"> <i class="ion-ios-loop-strong property__icon"></i> <span class="property__item-desc">Compare</span> </a>
                    </li>
                    <li class="property__item">
                        <a href="#" class="property__link"> <i class="ion-android-share-alt property__icon"></i> <span class="property__item-desc">Share</span> </a>
                    </li>
                </ul>
                <!-- .property__list -->
            </div>
            <!-- .property__header-container -->
        </div>
        <!-- .container -->
    </div>
    <!-- .property__header -->
    <div class="property__slider">
        <div class="container">
            <div class="property__slider-container property__slider-container--vertical">
                <ul class="property__slider-nav property__slider-nav--vertical">
                        <?php   $values  = (array)json_decode($property->uploaded_files,true); 
                       
                       foreach ($values as $key => $value) {
                       
                        $src =  '/attachment_property/'.$value['0'];
                        ?>
                         <li class="property__slider-item">
                                <img src="{{$src}}" alt="Image 1">
                        </li>
                     <?php  }
                       ?>
                    
                   {{--  <li class="property__slider-item">
                        <img src="images/uploads/property_slider_2.jpeg" alt="Image 2">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_3.jpeg" alt="Image 3">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_4.jpeg" alt="Image 4">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_5.jpeg" alt="Image 5">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_6.jpeg" alt="Image 6">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_7.jpeg" alt="Image 7">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_8.jpeg" alt="Image 8">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_9.jpeg" alt="Image 9">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_10.jpeg" alt="Image 10">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_11.jpeg" alt="Image 11">
                    </li>
                    <li class="property__slider-item">
                        <img src="images/uploads/property_slider_12.jpeg" alt="Image 12">
                    </li> --}}
                </ul>
                <!-- .property__slider-nav -->
                <div class="property__slider-main property__slider-main--vertical">
                    <div class="property__slider-images">
                            <?php   $values  = (array)json_decode($property->uploaded_files,true); 
                       
                            foreach ($values as $key => $value) {
                            
                             $src =  '/attachment_property/'.$value['0'];
                             ?>
                              <div class="property__slider-image">
                                    <img style="height:510px;width:100%" src="{{$src}}" alt="Image 1">
                                </div>
                          <?php  }
                            ?>
                        
                        <!-- .property__slider-image -->
                       {{--  <div class="property__slider-image">
                            <img src="images/uploads/property_slider_2.jpeg" alt="Image 2">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_3.jpeg" alt="Image 3">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_4.jpeg" alt="Image 4">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_5.jpeg" alt="Image 5">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_6.jpeg" alt="Image 6">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_7.jpeg" alt="Image 7">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_8.jpeg" alt="Image 8">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_9.jpeg" alt="Image 9">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_10.jpeg" alt="Image 10">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_11.jpeg" alt="Image 11">
                        </div>
                        <!-- .property__slider-image -->
                        <div class="property__slider-image">
                            <img src="images/uploads/property_slider_12.jpeg" alt="Image 12">
                        </div> --}}
                        <!-- .property__slider-image -->
                    </div>
                    <!-- .property__slider-images -->
                    <ul class="image-navigation">
                        <li class="image-navigation__prev">
                            <span class="ion-ios-arrow-left"></span>
                        </li>
                        <li class="image-navigation__next">
                            <span class="ion-ios-arrow-right"></span>
                        </li>
                    </ul>
                    <span class="property__slider-info"><i class="ion-android-camera"></i><span class="sliderInfo"></span></span>
                </div>
                <!-- .property__slider-main -->
            </div>
            <!-- .property__slider-container -->
        </div>
        <!-- .container -->
    </div>
    <!-- .property__slider -->
    <div class="property__container">
        <div class="container">
            <div class="row">
                <aside class="col-md-3">
                    <div class="widget__container">
                        <section class="widget">
                            <form class="contact-form contact-form--white" method="post"  id="customer_form">
                                <div class="contact-form__header">
                                    <div class="contact-form__header-container">
                                        {{-- <img src="images/uploads/contact_agent.png" alt="Tristina Avelon">
                                        <div class="contact-info">
                                            <span class="contact-company">Realand Real Estate</span>
                                            <h3 class="contact-name"><a href="#">Tristina Avelon</a></h3>
                                            <a href="tel:+8002883991" class="contact-number">Call: (800) 288 3991</a>
                                        </div> --}}
                                        <!-- .contact-info -->
                                    </div>
                                    <!-- .contact-form__header-container -->
                                </div>
                                <!-- .contact-form__header -->
                                
                                <div class="contact-form__body">
                                    
                                    <input type="text" class="contact-form__field" placeholder="Name" name="name" required>
                                    <input type="email" class="contact-form__field" placeholder="Email" name="email" >
                                    <input type="tel" class="contact-form__field" placeholder="Phone number" name="phoneno">
                                    <textarea class="contact-form__field contact-form__comment" cols="30" rows="5" placeholder="Comment" name="description" required>I am interested in
                                            {{$property->lo_address}}, {{$property->city}}, Pakistan
                                    </textarea>
                                </div>
                                <!-- .contact-form__body -->
                                <div class="contact-form__footer">
                                    <input type="submit" class="contact-form__submit"  value="Contact Agent">
                                </div>
                               
                                <!-- .contact-form__footer -->
                            </form>
                            <!-- .contact-form -->
                           
                        </section>
                
                    </div></aside>
                    <aside class="col-md-9">
                        <div class="widget__container">
                            <section class="widget">
                                <div id="map" style="background-color:rebeccapurple;height:360px;"></div>
                            </section>
                        </div>
                    </aside>
                     <!-- .widget -->
                       {{--  <section class="widget widget--white widget--padding-20">
                            <h3 class="widget__title">Similar Homes</h3>
                            <div class="similar-home">
                                <a href="single_property_1.html">
                                    <div class="similar-home__image">
                                        <div class="similar-home__overlay"></div>
                                        <!-- .similar-home__overlay -->
                                        <img src="images/uploads/residia_nishi_azabu.jpg" alt="Residia Nishi Azabu">
                                        <span class="similar-home__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                                    </div>
                                    <!-- .similar-home__image -->
                                    <div class="similar-home__content">
                                        <h3 class="similar-home__title">Residia Nishi Azabu</h3>
                                        <span class="similar-home__price">$2,185,000</span>
                                    </div>
                                    <!-- .similar-home__content -->
                                </a>
                            </div>
                            <!-- .similar-home -->
                            <div class="similar-home">
                                <a href="single_property_1.html">
                                    <div class="similar-home__image">
                                        <div class="similar-home__overlay"></div>
                                        <!-- .similar-home__overlay -->
                                        <img src="images/uploads/dream_house_take_away.jpg" alt="Dream House Take Away">
                                        <span class="similar-home__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                                    </div>
                                    <!-- .similar-home__image -->
                                    <div class="similar-home__content">
                                        <h3 class="similar-home__title">Dream House Take Away</h3>
                                        <span class="similar-home__price">$2,185,000</span>
                                    </div>
                                    <!-- .similar-home__content -->
                                </a>
                            </div>
                            <!-- .similar-home -->
                            <div class="similar-home">
                                <a href="single_property_1.html">
                                    <div class="similar-home__image">
                                        <div class="similar-home__overlay"></div>
                                        <!-- .similar-home__overlay -->
                                        <img src="images/uploads/castalia_shibakoen.jpg" alt="Castalia Shibakoen">
                                        <span class="similar-home__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                                    </div>
                                    <!-- .similar-home__image -->
                                    <div class="similar-home__content">
                                        <h3 class="similar-home__title">Castalia Shibakoen</h3>
                                        <span class="similar-home__price">$2,185,000</span>
                                    </div>
                                    <!-- .similar-home__content -->
                                </a>
                            </div>
                            <!-- .similar-home -->
                        </section>
                        <!-- .widget -->
                        <section class="widget widget--white widget--padding-20">
                            <h3 class="widget__title">Mortgage Calculator</h3>
                            <form class="form-calculator">
                                <div class="form-calculator__group">
                                    <label for="home-price" class="form-calculator__label">Home Price</label>
                                    <span class="form-calculator__icon form-calculator__currency">$</span>
                                    <input id="home-price" type="text" class="form-calculator__field" required value="2,568,000">
                                </div>
                                <!-- .form-calculator__group -->
                                <div class="form-calculator__wrapper">
                                    <div class="form-calculator__group form-calculator__group--larger">
                                        <label for="down-payment" class="form-calculator__label">Down Payment</label>
                                        <span class="form-calculator__icon form-calculator__currency">$</span>
                                        <input id="down-payment" type="text" class="form-calculator__field" required value="317,600">
                                    </div>
                                    <!-- .form-calculator__group -->
                                    <div class="form-calculator__group form-calculator__group--smaller">
                                        <label for="percent" class="form-calculator__label">Percent</label>
                                        <span class="form-calculator__icon form-calculator__percent">%</span>
                                        <input id="percent" type="text" class="form-calculator__field" required value="20">
                                    </div>
                                    <!-- .form-calculator__group -->
                                </div>
                                <!-- .form-calculator__wrapper -->                                         
                                <div class="form-calculator__group">
                                    <label for="annual-rate" class="form-calculator__label">Annual Interest Rate</label>
                                    <span class="form-calculator__icon form-calculator__percent">%</span>
                                    <input id="annual-rate" type="text" class="form-calculator__field" required value="3.5">
                                </div>
                                <!-- .form-calculator__group -->
                                <div class="form-calculator__group">
                                    <label for="loan-term" class="form-calculator__label">Loan Term (Years)</label>
                                    <select id="loan-term" class="ht-field" required>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10" selected>10</option>
                                    </select>
                                </div>
                                <!-- .form-calculator__group -->
                                <div class="form-calculator__group">
                                    <label for="percent" class="form-calculator__label">Payment Method</label>
                                    <select id="percent" class="ht-field" required>
                                        <option value="Weekly">WEEKLY</option>
                                        <option value="Monthly" selected>MONTHLY</option>
                                        <option value="Bi-Weekly">BI-WEEKLY</option>
                                    </select>
                                </div>
                                <!-- .form-calculator__group -->
                                <input type="submit" class="form-calculator__submit" value="Calculate">
                                <div class="form-calculator__result">
                                    <h3 class="mortgage-payment">Mortgage Payments: <span>$22253.28</span></h3>
                                </div>
                            </form>
                            <!-- .form-calculator -->
                        </section> --}}
                        
@endsection
@section('script')
   <!-- JS Files -->
   <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script> --}}
    <script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
    
    <script src="{{ asset('bbclands/js/plugins.js') }}"></script>
    <script src="{{ asset('bbclands/js/custom.js') }}"></script>
    
          <script>
          
          
          
            $("#customer_form").on("submit", function(){
                var data = $("#customer_form").serializeArray()
                var _token = $('input[name="_token"]').val();
                var property_id  = {{$id}}
           $.ajax({
               url: "{{route('ajaxDeal')}}",
               type: 'POST',
               data: {
                   _token : _token,data:data,property_id:property_id
               },
               success: function (data) {
                if(data == 'ok')
                    swal("Good job!", "Our Team Member will contact soon!", "success");
                if(data == 'alread_have_req')
                    swal("Ok!", "Request request submited ! Our Team member will contact you soon.", "success");
                    
                }
            });
                
            return false;
            })
          
          </script>
   
          <script>
            function myMap() {
            var myCenter = new google.maps.LatLng({{$property->lat}},{{$property->lng}});
            var mapCanvas = document.getElementById("map");
            var mapOptions = {center: myCenter, zoom: 18};
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({position:myCenter});
            marker.setMap(map);

            // Zoom to 9 when clicking on marker
            google.maps.event.addListener(marker,'click',function() {
                map.setZoom(9);
                map.setCenter(marker.getPosition());
            });
            }
            </script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4&callback=myMap"></script>
      @endsection