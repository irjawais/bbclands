@extends('user.layouts.apps')
@section('css')
 <!-- External Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- CSS files -->
   <link rel="stylesheet" href="{{ asset('bbclands/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('bbclands/css/style.css') }}">
@endsection
@section('content')

<section class="bookmarked-listing">
    <div class="container">
        <ul class="ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border">
            <li class="ht-breadcrumbs__item">
                <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Home</span></a>
            </li>
            <li class="ht-breadcrumbs__item">
                <a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Pages</span></a>
            </li>
            
        </ul>
        <!-- .ht-breadcrumb -->
        <br><br>
        <div class="bookmarked-listing__container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="bookmarked-listing__headline">Howdy, <strong>{{Auth::user()->name}}!</strong></h2>
                    <div class="settings-block">
                        <span class="settings-block__title">Manage Account</span>
                        <ul class="settings">
                            <li class="setting">
                                <a href="{{route('myProfile')}}" class="setting__link"><i class="ion-ios-person-outline setting__icon"></i>My Profile</a>
                            </li>
                           
                        </ul>
                        <!-- settings -->
                    </div>
                    <!-- .settings-block -->
                    <div class="settings-block">
                        <span class="settings-block__title">Manage Listing</span>
                        <ul class="settings">
                            <li class="setting">
                                <a href="{{route('myProperty')}}"  class="setting__link"><i class="ion-ios-home-outline setting__icon"></i>My Property</a>
                            </li>
                            <li class="setting setting--current">
                                <a href="{{route('submit.property')}}" class="setting__link"><i class="ion-ios-upload-outline setting__icon"></i>Submit New Property</a>
                            </li>
                            
                        </ul>
                        <!-- settings -->
                    </div>
                    <!-- .settings-block -->
                    <div class="settings-block">
                        <ul class="settings">
                            {{-- <li class="setting">
                                <a href="change_password.html" class="setting__link"><i class="ion-ios-unlocked-outline setting__icon"></i>Change Password</a>
                            </li> --}}
                            <li class="setting">
                                <a href="/logout" class="setting__link"><i class="ion-ios-undo-outline setting__icon"></i>Log Out</a>
                            </li>
                        </ul>
                        <!-- settings -->
                    </div>
                    <!-- .settings-block -->
                </div>
                <!-- .col -->
                <div class="col-md-9">
                    <ul class="manage-list manage-list--bookmark">
                        <li class="manage-list__header">
                            <span class="manage-list__title"> My Property list...!</span>
                            {{-- <div class="manage-list__action">
                                <a href="#" class="remove"><i class="fa fa-times" aria-hidden="true"></i> Remove All</a>
                            </div> --}}
                        </li>
                        @foreach ($properties as $property)
                        <?php  
                                if((array)json_decode($property->uploaded_files,true)){
                                    $value  = (array)json_decode($property->uploaded_files,true)[0]; 
                                    $src =  '/attachment_property/'.$value[0]; 
                                }else{
                                    $src =  '/attachment_property/noimage.jpg'; 
                                }
                              
                        ?>
                        <li class="manage-list__item">
                            <div class="manage-list__item-container">
                                <div class="manage-list__item-img">
                                    <a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}">
                                        <img  style="width:130px;height:130px;" src="{{asset($src)}}" alt="{{$property->property_title}}" class="listing__img">
                                    </a>
                                </div>
                                <!-- manage-list__item-img -->
                                <div class="manage-list__item-detail">
                                    <h3 class="listing__title"><a href="{{route('detail',[$property->id, str_slug(str_replace('-',' ',$property->property_title))])}}">{{$property->property_title}}</a></h3>
                                    <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> {{$property->lo_address}}</p>
                                    <p class="listing__price">
                                        @if($property->country=="pakistan")
                                            <span>Rs.</span>
                                        @endif
                                        {{$property->price}}
                                    </p>
                                    @if($property->approval!=1)
                                        <span title="Your Property will approve soon." style="background: #66d1ff;border: 1px solid yellow;border-radius: 6px;padding:  5px;">Under Review</span>
                                    @endif
                                </div>
                            </div>
                            <!-- .manage-list__item-container -->
                            <div class="manage-list__action">
                                <a href="{{route('editProperty',$property->id)}}" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                <a href="{{route('removeProperty',$property->id)}}" class="remove"><i class="fa fa-times" aria-hidden="true"></i> Remove</a>
                            </div>
                        </li>
                        @endforeach

                        {{ $properties->links() }}
                    </ul>
                </div>
                <!-- .col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .bookmarked-listing__container -->
    </div>
    <!-- .container -->
</section>
@endsection
@section('script')
   <!-- JS Files -->
   <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
    <script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
    
    <script src="{{ asset('bbclands/js/plugins.js') }}"></script>
    <script src="{{ asset('bbclands/js/custom.js') }}"></script>
    <script>
     
    </script>
    @endsection