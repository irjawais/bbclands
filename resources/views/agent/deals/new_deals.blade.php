@extends('agent.layouts.apps')
 @section('content')

<div class="main">

    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">
    
                        <div class="panel-heading">
                            <h3 class="panel-title">New Deals</h3>
                        </div>
                        <div class="panel-body">
                        <table id="property_table" class="table table-striped">
    <thead>
      <tr>
        <th>Customer Name</th>
        <th>Customer Phone No</th>
        <th>Property</th>
        <th>Price</th>
        <th>Property Type </th>
        <th>Purpose </th>
        <th> </th>
      </tr>
    </thead>
   
  </table>
        
                        </div>
                        
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
@endsection
@section('script')

<script type="text/javascript">
 $(document).ready(function () {
    
    $('#property_table').DataTable({
   
        processing: true,
        serverSide: true,
        ajax:{ 
            "url":  '{{route('agent.ajaxNewDeals')}}' ,
            data: {
            'csrf_token':$('meta[name=csrf_token]').attr("content")
            }
        },
        
        columns: [
            {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue" class="btn btn-info btn-xs" title="View Deal" href=/agent/viewproperty/${data.id}>${data.name}</a>
                    `; 
                }
            },
            //{ data: 'name', name: 'name' },  
            { data: 'phoneno', name: 'phoneno' },  
            { data: 'property.property_title', name: 'property.property_title' }, 
            { data: 'property.price', name: 'property.price' },
            { data: 'property.property_type', name: 'property.property_type' },  
            { data: 'property.purpose', name: 'property.purpose' },
            {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue" class="btn btn-warning btn-xs"  title="Accept Deal" href=/agent/deal-accept/${data.id}>Accept</a>
                        <a style="color:blue" class="btn btn-danger btn-xs"  title="Delete" href=/agent/deal-delete/${data.id}>Delete</a>
                        <a style="color:blue" class="btn btn-success btn-xs"  title="Delete" href=/agent/deal-complete/${data.id}>Complete Deal</a>
                        
                    `; 
                }
            }
            /* {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue" class="btn btn-info btn-xs" title="View Property" href=/agent/viewproperty/${data.id}>${data.id}</a>
                    `; 
                }
            },
            { data: 'property_title', name: 'property_title' },
            { data: 'price', name: 'price' },
            { data: 'city', name: 'city' },  
            { data: 'created_at', name: 'created_at' },
            {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue" class="btn btn-warning btn-xs"  title="Accept Requested" href=/agent/viewproperty/${data.id}>Accept</a>
                        <a style="color:blue" class="btn btn-danger btn-xs"  title="Delete" href=/agent/viewproperty/${data.id}>Delete</a>
                    `; 
                }
            } */
        ]
    });
});
</script>
@endsection