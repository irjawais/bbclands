<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Agent Panel</title>



    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/linearicons/style.css') }}">
    <link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/chartist/css/chartist-custom.css') }}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('klorofil/assets/css/main.css') }}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{ asset('klorofil/assets/css/demo.css') }}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('klorofil/assets/switcher/switcher.css') }}">
    <link rel="stylesheet" href="{{ asset('countrySelect.min.css') }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
     @yield("css")
</head>

<body>

    <div id="wrapper">
        <!-- NAVBAR -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <!-- <div class="brand">
				<a href="{{ url('/') }}"><img height="40px" width="190px" src='{{asset("/img/logo.png")}}' alt="Klorofil Logo" class="img-responsive logo"></a>
            </div> -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="brand" style="padding:0">
                    <a href="/agent/home"><img style="height:70px!important"  width="190px" src='{{asset("/img/logo.png")}}' alt="Agent Logo" class="img-responsive logo"></a>
                </div>
                <div class="container-fluid">
                    <div class="navbar-btn">
                        <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
                    </div>


                    <div id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right">

                        @if (!Auth::guard("agent")->check())  
                            <li><a href="{{ route('agent.login') }}">Login</a></li>
                            <li><a href="{{ route('agent.register') }}">Register</a></li>
                            
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span>{{ Auth::guard('agent')->user()->name }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                                <ul class="dropdown-menu">
                                    <!-- <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
								<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li> -->
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="lnr lnr-exit"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            <!-- <li>
							<a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
						</li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        </nav>

        <div id="sidebar-nav" class="sidebar">
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 95%;">
                <div class="sidebar-scroll" style="overflow: hidden; width: auto; height: 95%;">
                    <nav>

                        <ul class="nav">
                            <li><a href="{{ route('agent.home') }}" class="{{ (Request::is('agent.home') ? 'active' : '') }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>

                            <li><a href="{{ route('agent.add.property') }}" class="{{ (Request::is('agent.add.property') ? 'active' : '') }}"><i class="lnr lnr-code"></i> <span>Add Property</span></a></li>
                            @if(Auth::guard('agent')->user()->power)
                            <li>
                                <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Manage Property</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                <div id="subPages" class="collapse ">
                                    <ul class="nav">
                                        @if(Auth::guard('agent')->user()->power == "manager" || Auth::guard('agent')->user()->power == "assistant")
                                            <li><a href="{{ route('agent.property.request') }}" class="">Property Request</a></li>
                                            <li><a href="{{ route('agent.property.all') }}" class="">All Property</a></li>
                                        @endif
                                    </ul>

                                </div>
                            </li>
                            @endif
                            <li><a href="{{ route('agent.my.property') }}" class="{{ (Request::is('agent.my.property') ? 'active' : '') }}"><i class="lnr lnr-home"></i> <span>My Property</span></a></li>
                            <li>
                                <a href="#Deals" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Deals</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                <div id="Deals" class="collapse ">
                                    <ul class="nav">
                                        <li><a href="{{ route('agent.new.deals') }}" class="">My New Deals</a></li>
                                        <li><a href="{{ route('agent.my.all.property') }}" class="">My All Deals</a></li>
                                        {{-- <li><a href="{{ route('agent.new.deals') }}" class="">Manage New Deals</a></li> --}}
                                        
                                        
                                    </ul>
                                </div>
                            </li>

                       </ul>
                    </nav>
                </div>
                <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 496.324px;"></div>
                <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
            </div>
            
        </div>




        @if($msg = session("message"))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <i class="fa fa-check-circle"></i> {{$msg}}
        </div>



        @endif @if($error = session("error"))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <i class="fa fa-check-circle"></i> {{$error}}
        </div>
        @endif 
        <div class="clearfix"></div>
        <footer>
            <div class="container-fluid">
                <p class="copyright">© 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
            </div>
        </footer>
    <!-- </div>
    </div> -->

</body>
 @yield('content')
        <script src="{{ asset('klorofil/assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('klorofil/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('klorofil/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('klorofil/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
        <script src="{{ asset('klorofil/assets/vendor/chartist/js/chartist.min.js') }}"></script>
        <script src="{{ asset('klorofil/assets/scripts/klorofil-common.js') }}"></script>

        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="{{ asset('klorofil/assets/switcher/switcher.js') }}"></script>
        


<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        @yield("script");
</html>