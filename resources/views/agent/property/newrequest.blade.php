@extends('agent.layouts.apps')
 @section('content')

<div class="main">

    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">
    
                        <div class="panel-heading">
                            <h3 class="panel-title">New Property</h3>
                        </div>
                        <div class="panel-body">
                        <table id="property_table" class="table table-striped">
    <thead>
      <tr>
        <th>Property Id</th>
        <th>Title</th>
        <th>Price</th>
        <th>City</th>
        <th>Date</th>
        <th></th>
      </tr>
    </thead>
   
  </table>
        
                        </div>
                        
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
@endsection
@section('script')

<script type="text/javascript">
 $(document).ready(function () {
    
    $('#property_table').DataTable({
   
        processing: true,
        serverSide: true,
        ajax:{ 
            "url":  '{{route('agent.ajaxNewPropertyRequest')}}' ,
            data: {
            'csrf_token':$('meta[name=csrf_token]').attr("content")
            }
        },
        
        columns: [
            {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue" class="btn btn-info btn-xs" title="View Property" href=/agent/viewproperty/${data.id}>${data.id}</a>
                    `; 
                }
            },
            { data: 'property_title', name: 'property_title' },
            { data: 'price', name: 'price' },
            { data: 'city', name: 'city' },  
            { data: 'created_at', name: 'created_at' },
            {
                
                "mData": null,
                "bSortable": false,
                //name:"settlement.id",
               "mRender": function (data) { 
                   return `
                        <a style="color:blue" class="btn btn-warning btn-xs"  title="Accept Requested" href=/agent/accept-property/${data.id}>Accept</a>
                        <a style="color:blue" class="btn btn-danger btn-xs"  title="Delete" href=/agent/delete-property/${data.id}>Delete</a>
                    `; 
                }
            }
        ]
    });
});
</script>
@endsection