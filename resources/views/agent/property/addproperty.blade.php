@extends('agent.layouts.apps')
 @section('content')

<div class="main">

    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">
                    @if($msg = session("message"))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <i class="fa fa-check-circle"></i> {{$msg}}
        </div>



        @endif @if($error = session("error"))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <i class="fa fa-check-circle"></i> {{$error}}
        </div>
        @endif
                        <div class="panel-heading">
                            <h3 class="panel-title">New Property</h3>
                        </div>
                        <div class="panel-body">
                            <div style="background-color: #008000;border-radius: 3px;clear: both;color: #ffffff;float: left;
                                                margin-bottom: 7px;
                                                margin-top: 8px;
                                                padding: 7px 0;
                                                text-indent: 9px;
                                                text-transform: uppercase;
                                                width: 99.6%;">
                                <h6>PROPERTY TYPE AND LOCATION</h6>
                            </div>
                    <form action="{{route('agent.property.Post')}}" enctype="multipart/form-data" method="post" name="propetyform" onsubmit="DoSubmit();">
                    {{ csrf_field() }}
                            <div class="col-md-3">
                                <label>Purpose: * </label>
                            </div>
                            <div class="col-md-3">
                                <label class="label label-info"> Sale
                                    <input type="radio" value="sale"  name="purpose">
                                    <span class="checkmark"></span>
                                    </label>

                                <label class="label label-info">Buy
                                    <input type="radio" value="buy" name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                <label class="label label-info">Rent
                                    <input type="radio" value="rent" name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                            </div>
                            <div style="clear: both;"></div>

                            <div class="col-md-3">
                                <label>Property Type: * </label>
                            </div>
                            <div class="col-md-3">
                                <label class="label label-info"> Home
                                    <input type="radio" value="home"  name="property_type">
                                    <span class="checkmark"></span>
                                    </label>

                                <label class="label label-info">Plots
                                    <input type="radio" value="plot" name="property_type">
                                    <span class="checkmark"></span>
                                    </label>
                                <label class="label label-info"> Commercial
                                    <input type="radio" value="commercial" name="property_type">
                                    <span class="checkmark"></span>
                                    </label>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>Select Country: * </label>
                            </div>
                            <div class="col-md-3">

                                <div class="form-group">
                                    <select id="countryname" name="country" class="form-control">
                                    <option selected="disable">Select Country</option>    
                                    <option value="pakistan">Pakistan</option>
										<option id="canada">Canada</option>
									</select>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>City: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select id="cityname" name="city" class="form-control">
										
									</select>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>Place Name: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="pac-input" type="text" class="form-control" name="lo_address" placeholder="Place Address">
                                </div>
                            </div>
                           
                            <div style="clear: both;"></div>
                            <div id="map" style="width:300px;height:300px;"></div>
                            <div id="infowindow-content">
                                <img src="" width="16" height="16" id="place-icon">
                                <span id="place-name" class="title"></span><br>
                                <span id="place-address"></span>
                            </div>
                            <div style="background-color: #008000;border-radius: 3px;clear: both;color: #ffffff;float: left;margin-bottom: 7px;margin-top: 8px;
                                                padding: 7px 0;
                                                text-indent: 9px;
                                                text-transform: uppercase;
                                                width: 99.6%;">
                                <h6>PROPERTY DETAILS</h6>
                            </div>
                           
                            <div class="col-md-3">
                                <label>Property Title: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="pac-input" type="text" name="property_title" class="form-control" placeholder="Title">
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>Description: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <textarea id="pac-input" type="text" class="form-control" name="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>All Inclusive Price (PKR): * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="pac-input" type="number" name="price" class="form-control" placeholder="Price">
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>Land Area: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="pac-input" type="number" name="area" class="form-control" placeholder="Area">
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>Unit: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                        <select class="ht-field" name="unit" name="unit" id="unit" style="width:136px;" autocomplete="off">
                                                <option value="square-feet">Square Feet</option>
                                                <option value="square-yards">Square Yards</option>
                                                <option value="square-meters">Square Meters</option>
                                                <option value="marla">Marla</option>
                                                <option value="kanal">Kanal</option>
                                            </select>
                                    {{-- <input id="pac-input" type="text" name="unit" class="form-control" placeholder="Unit"> --}}
                                </div>
                            </div>
                            <div style="background-color: #008000;
                                                border-radius: 3px;
                                                clear: both;
                                                color: #ffffff;
                                                float: left;
                                                margin-bottom: 7px;
                                                margin-top: 8px;
                                                padding: 7px 0;
                                                text-indent: 9px;
                                                text-transform: uppercase;
                                                width: 99.6%;">
                                <h6>PROPERTY Images</h6>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
                                <label>Images: * </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                <input type="file" accept="image/x-png,image/gif,image/jpeg" name="img1" class="custom-file-input">
                                </div>
                                <div class="form-group">
                                <input type="file" accept="image/x-png,image/gif,image/jpeg" name="img2" class="custom-file-input">
                                </div>
                                <div class="form-group">
                                <input type="file" accept="image/x-png,image/gif,image/jpeg" name="img3" class="custom-file-input">
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-3">
											<button type="submit" class="btn btn-primary btn-block">Register</button>
                            </div>
                            <input id="pac-input" type="hidden" name="lng" class="form-control" >
                            <input id="pac-input" type="hidden" name="lat" class="form-control" >

                </form>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    
    #map {
        height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    
    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }
    
    #infowindow-content .title {
        font-weight: bold;
    }
    
    #infowindow-content {
        display: none;
    }
    
    #map #infowindow-content {
        display: inline;
    }
    
    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }
    
    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }
    
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }
    
    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    
    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
    }
    
    #pac-input:focus {
        border-color: #4d90fe;
    }
    
    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }
</style>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
   var place ;
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 30.157458, 
                lng: 71.52491540000005
            },
            zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            place = autocomplete.getPlace();
            console.log(place.geometry.location.lat(),place.geometry.location.lng());
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
            });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({
                    strictBounds: this.checked
                });
            });
    }
    function DoSubmit(){
        document.propetyform.lng.value = place.geometry.location.lng();
        document.propetyform.lat.value = place.geometry.location.lat();
        return true;
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&libraries=places&callback=initMap" async defer></script>

<script>
    $(function(){
    $('#countryname').change(function(){ //This event will fire the change event. 
        console.log(this.value);
        if(this.value=="pakistan"){
            $("#cityname").append('<option>select City</option><option class="option" value="Abbottabad">Abbottabad</option><option class="option" value="Adezai">Adezai</option><option class="option" value="Ali Bandar">Ali Bandar</option><option class="option" value="Amir Chah">Amir Chah</option><option class="option" value="Attock">Attock</option><option class="option" value="Ayubia">Ayubia</option><option class="option" value="Bahawalpur">Bahawalpur</option><option class="option" value="Baden">Baden</option><option class="option" value="Bagh">Bagh</option><option class="option" value="Bahawalnagar">Bahawalnagar</option><option class="option" value="Burewala">Burewala</option><option class="option" value="Banda Daud Shah">Banda Daud Shah</option><option class="option" value="Bannu district|Bannu">Bannu</option><option class="option" value="Batagram">Batagram</option><option class="option" value="Bazdar">Bazdar</option><option class="option" value="Bela">Bela</option><option class="option" value="Bellpat">Bellpat</option><option class="option" value="Bhag">Bhag</option><option class="option" value="Bhakkar">Bhakkar</option><option class="option" value="Bhalwal">Bhalwal</option><option class="option" value="Bhimber">Bhimber</option><option class="option" value="Birote">Birote</option><option class="option" value="Buner">Buner</option><option class="option" value="Burj">Burj</option><option class="option" value="Chiniot">Chiniot</option><option class="option" value="Chachro">Chachro</option><option class="option" value="Chagai">Chagai</option><option class="option" value="Chah Sandan">Chah Sandan</option><option class="option" value="Chailianwala">Chailianwala</option><option class="option" value="Chakdara">Chakdara</option><option class="option" value="Chakku">Chakku</option><option class="option" value="Chakwal">Chakwal</option><option class="option" value="Chaman">Chaman</option><option class="option" value="Charsadda">Charsadda</option><option class="option" value="Chhatr">Chhatr</option><option class="option" value="Chichawatni">Chichawatni</option><option class="option" value="Chitral">Chitral</option><option class="option" value="Dadu">Dadu</option><option class="option" value="Dera Ghazi Khan">Dera Ghazi Khan</option><option class="option" value="Dera Ismail Khan">Dera Ismail Khan</option> <option class="option" value="Dalbandin">Dalbandin</option><option class="option" value="Dargai">Dargai</option><option class="option" value="Darya Khan">Darya Khan</option><option class="option" value="Daska">Daska</option><option class="option" value="Dera Bugti">Dera Bugti</option><option class="option" value="Dhana Sar">Dhana Sar</option><option class="option" value="Digri">Digri</option><option class="option" value="Dina City|Dina">Dina</option><option class="option" value="Dinga">Dinga</option><option class="option" value="Diplo, Pakistan|Diplo">Diplo</option><option class="option" value="Diwana">Diwana</option><option class="option" value="Dokri">Dokri</option><option class="option" value="Drosh">Drosh</option><option class="option" value="Duki">Duki</option><option class="option" value="Dushi">Dushi</option><option class="option" value="Duzab">Duzab</option><option class="option" value="Faisalabad">Faisalabad</option><option class="option" value="Fateh Jang">Fateh Jang</option><option class="option" value="Ghotki">Ghotki</option><option class="option" value="Gwadar">Gwadar</option><option class="option" value="Gujranwala">Gujranwala</option><option class="option" value="Gujrat">Gujrat</option><option class="option" value="Gadra">Gadra</option><option class="option" value="Gajar">Gajar</option><option class="option" value="Gandava">Gandava</option><option class="option" value="Garhi Khairo">Garhi Khairo</option><option class="option" value="Garruck">Garruck</option><option class="option" value="Ghakhar Mandi">Ghakhar Mandi</option><option class="option" value="Ghanian">Ghanian</option><option class="option" value="Ghauspur">Ghauspur</option><option class="option" value="Ghazluna">Ghazluna</option><option class="option" value="Girdan">Girdan</option><option class="option" value="Gulistan">Gulistan</option><option class="option" value="Gwash">Gwash</option><option class="option" value="Hyderabad">Hyderabad</option><option class="option" value="Hala">Hala</option><option class="option" value="Haripur">Haripur</option><option class="option" value="Hab Chauki">Hab Chauki</option><option class="option" value="Hafizabad">Hafizabad</option><option class="option" value="Hameedabad">Hameedabad</option><option class="option" value="Hangu">Hangu</option><option class="option" value="Harnai">Harnai</option><option class="option" value="Hasilpur">Hasilpur</option><option class="option" value="Haveli Lakha">Haveli Lakha</option><option class="option" value="Hinglaj">Hinglaj</option><option class="option" value="Hoshab">Hoshab</option><option class="option" value="Islamabad">Islamabad</option><option class="option" value="Islamkot">Islamkot</option><option class="option" value="Ispikan">Ispikan</option><option class="option" value="Jacobabad">Jacobabad</option><option class="option" value="Jamshoro">Jamshoro</option><option class="option" value="Jhang">Jhang</option><option class="option" value="Jhelum">Jhelum</option><option class="option" value="Jamesabad">Jamesabad</option><option class="option" value="Jampur">Jampur</option><option class="option" value="Janghar">Janghar</option><option class="option" value="Jati, Jati(Mughalbhin)">Jati</option><option class="option" value="Jauharabad">Jauharabad</option><option class="option" value="Jhal">Jhal</option><option class="option" value="Jhal Jhao">Jhal Jhao</option><option class="option" value="Jhatpat">Jhatpat</option><option class="option" value="Jhudo">Jhudo</option><option class="option" value="Jiwani">Jiwani</option><option class="option" value="Jungshahi">Jungshahi</option><option class="option" value="Karachi">Karachi</option><option class="option" value="Kotri">Kotri</option><option class="option" value="Kalam">Kalam</option><option class="option" value="Kalandi">Kalandi</option><option class="option" value="Kalat">Kalat</option><option class="option" value="Kamalia">Kamalia</option><option class="option" value="Kamararod">Kamararod</option><option class="option" value="Kamber">Kamber</option><option class="option" value="Kamokey">Kamokey</option><option class="option" value="Kanak">Kanak</option><option class="option" value="Kandi">Kandi</option><option class="option" value="Kandiaro">Kandiaro</option><option class="option" value="Kanpur">Kanpur</option><option class="option" value="Kapip">Kapip</option><option class="option" value="Kappar">Kappar</option><option class="option" value="Karak City">Karak City</option><option class="option" value="Karodi">Karodi</option><option class="option" value="Kashmor">Kashmor</option><option class="option" value="Kasur">Kasur</option><option class="option" value="Katuri">Katuri</option><option class="option" value="Keti Bandar">Keti Bandar</option><option class="option" value="Khairpur">Khairpur</option><option class="option" value="Khanaspur">Khanaspur</option><option class="option" value="Khanewal">Khanewal</option><option class="option" value="Kharan">Kharan</option><option class="option" value="kharian">kharian</option><option class="option" value="Khokhropur">Khokhropur</option><option class="option" value="Khora">Khora</option><option class="option" value="Khushab">Khushab</option><option class="option" value="Khuzdar">Khuzdar</option><option class="option" value="Kikki">Kikki</option><option class="option" value="Klupro">Klupro</option><option class="option" value="Kohan">Kohan</option><option class="option" value="Kohat">Kohat</option><option class="option" value="Kohistan">Kohistan</option><option class="option" value="Kohlu">Kohlu</option><option class="option" value="Korak">Korak</option><option class="option" value="Korangi">Korangi</option><option class="option" value="Kot Sarae">Kot Sarae</option><option class="option" value="Kotli">Kotli</option><option class="option" value="Lahore">Lahore</option><option class="option" value="Larkana">Larkana</option><option class="option" value="Lahri">Lahri</option><option class="option" value="Lakki Marwat">Lakki Marwat</option><option class="option" value="Lasbela">Lasbela</option><option class="option" value="Latamber">Latamber</option><option class="option" value="Layyah">Layyah</option><option class="option" value="Leiah">Leiah</option><option class="option" value="Liari">Liari</option><option class="option" value="Lodhran">Lodhran</option><option class="option" value="Loralai">Loralai</option><option class="option" value="Lower Dir">Lower Dir</option><option class="option" value="Shadan Lund">Shadan Lund</option><option class="option" value="Multan">Multan</option><option class="option" value="Mandi Bahauddin">Mandi Bahauddin</option><option class="option" value="Mansehra">Mansehra</option><option class="option" value="Mian Chanu">Mian Chanu</option><option class="option" value="Mirpur">Mirpur</option><option class="option" value="Moro, Pakistan|Moro">Moro</option><option class="option" value="Mardan">Mardan</option><option class="option" value="Mach">Mach</option><option class="option" value="Madyan">Madyan</option><option class="option" value="Malakand">Malakand</option><option class="option" value="Mand">Mand</option><option class="option" value="Manguchar">Manguchar</option><option class="option" value="Mashki Chah">Mashki Chah</option><option class="option" value="Maslti">Maslti</option><option class="option" value="Mastuj">Mastuj</option><option class="option" value="Mastung">Mastung</option><option class="option" value="Mathi">Mathi</option><option class="option" value="Matiari">Matiari</option><option class="option" value="Mehar">Mehar</option><option class="option" value="Mekhtar">Mekhtar</option><option class="option" value="Merui">Merui</option>\
            <option class="option" value="Mianwali">Mianwali</option><option class="option" value="Mianez">Mianez</option><option class="option" value="Mirpur Batoro">Mirpur Batoro</option><option class="option" value="Mirpur Khas">Mirpur Khas</option><option class="option" value="Mirpur Sakro">Mirpur Sakro</option><option class="option" value="Mithi">Mithi</option><option class="option" value="Mongora">Mongora</option><option class="option" value="Murgha Kibzai">Murgha Kibzai</option><option class="option" value="Muridke">Muridke</option><option class="option" value="Musa Khel Bazar">Musa Khel Bazar</option><option class="option" value="Muzaffar Garh">Muzaffar Garh</option><option class="option" value="Muzaffarabad">Muzaffarabad</option><option class="option" value="Nawabshah">Nawabshah</option><option class="option" value="Nazimabad">Nazimabad</option><option class="option" value="Nowshera">Nowshera</option><option class="option" value="Nagar Parkar">Nagar Parkar</option><option class="option" value="Nagha Kalat">Nagha Kalat</option><option class="option" value="Nal">Nal</option><option class="option" value="Naokot">Naokot</option><option class="option" value="Nasirabad">Nasirabad</option><option class="option" value="Nauroz Kalat">Nauroz Kalat</option><option class="option" value="Naushara">Naushara</option><option class="option" value="Nur Gamma">Nur Gamma</option><option class="option" value="Nushki">Nushki</option><option class="option" value="Nuttal">Nuttal</option><option class="option" value="Okara">Okara</option><option class="option" value="Ormara">Ormara</option><option class="option" value="Peshawar">Peshawar</option><option class="option" value="Panjgur">Panjgur</option><option class="option" value="Pasni City">Pasni City</option><option class="option" value="Paharpur">Paharpur</option><option class="option" value="Palantuk">Palantuk</option><option class="option" value="Pendoo">Pendoo</option><option class="option" value="Piharak">Piharak</option><option class="option" value="Pirmahal">Pirmahal</option><option class="option" value="Pishin">Pishin</option><option class="option" value="Plandri">Plandri</option><option class="option" value="Pokran">Pokran</option><option class="option" value="Pounch">Pounch</option><option class="option" value="Quetta">Quetta</option><option class="option" value="Qambar">Qambar</option><option class="option" value="Qamruddin Karez">Qamruddin Karez</option><option class="option" value="Qazi Ahmad">Qazi Ahmad</option><option class="option" value="Qila Abdullah">Qila Abdullah</option><option class="option" value="Qila Ladgasht">Qila Ladgasht</option><option class="option" value="Qila Safed">Qila Safed</option><option class="option" value="Qila Saifullah">Qila Saifullah</option><option class="option" value="Rawalpindi">Rawalpindi</option><option class="option" value="Rabwah">Rabwah</option><option class="option" value="Rahim Yar Khan">Rahim Yar Khan</option><option class="option" value="Rajan Pur">Rajan Pur</option><option class="option" value="Rakhni">Rakhni</option><option class="option" value="Ranipur">Ranipur</option><option class="option" value="Ratodero">Ratodero</option><option class="option" value="Rawalakot">Rawalakot</option><option class="option" value="Renala Khurd">Renala Khurd</option><option class="option" value="Robat Thana">Robat Thana</option><option class="option" value="Rodkhan">Rodkhan</option><option class="option" value="Rohri">Rohri</option><option class="option" value="Sialkot">Sialkot</option><option class="option" value="Sadiqabad">Sadiqabad</option><option class="option" value="Safdar Abad- (Dhaban Singh)">Safdar Abad</option><option class="option" value="Sahiwal">Sahiwal</option><option class="option" value="Saidu Sharif">Saidu Sharif</option><option class="option" value="Saindak">Saindak</option><option class="option" value="Sakrand">Sakrand</option><option class="option" value="Sanjawi">Sanjawi</option><option class="option" value="Sargodha">Sargodha</option><option class="option" value="Saruna">Saruna</option><option class="option" value="Shabaz Kalat">Shabaz Kalat</option><option class="option" value="Shadadkhot">Shadadkhot</option><option class="option" value="Shahbandar">Shahbandar</option><option class="option" value="Shahpur">Shahpur</option><option class="option" value="Shahpur Chakar">Shahpur Chakar</option><option class="option" value="Shakargarh">Shakargarh</option><option class="option" value="Shangla">Shangla</option><option class="option" value="Sharam Jogizai">Sharam Jogizai</option><option class="option" value="Sheikhupura">Sheikhupura</option><option class="option" value="Shikarpur">Shikarpur</option><option class="option" value="Shingar">Shingar</option><option class="option" value="Shorap">Shorap</option><option class="option" value="Sibi">Sibi</option><option class="option" value="Sohawa">Sohawa</option><option class="option" value="Sonmiani">Sonmiani</option><option class="option" value="Sooianwala">Sooianwala</option><option class="option" value="Spezand">Spezand</option><option class="option" value="Spintangi">Spintangi</option><option class="option" value="Sui">Sui</option><option class="option" value="Sujawal">Sujawal</option><option class="option" value="Sukkur">Sukkur</option><option class="option" value="Suntsar">Suntsar</option><option class="option" value="Surab">Surab</option><option class="option" value="Swabi">Swabi</option><option class="option" value="Swat">Swat</option><option class="option" value="Tando Adam">Tando Adam</option><option class="option" value="Tando Bago">Tando Bago</option><option class="option" value="Tangi">Tangi</option><option class="option" value="Tank City">Tank City</option><option class="option" value="Tar Ahamd Rind">Tar Ahamd Rind</option><option class="option" value="Thalo">Thalo</option><option class="option" value="Thatta">Thatta</option><option class="option" value="Toba Tek Singh">Toba Tek Singh</option><option class="option" value="Tordher">Tordher</option><option class="option" value="Tujal">Tujal</option><option class="option" value="Tump">Tump</option><option class="option" value="Turbat">Turbat</option><option class="option" value="Umarao">Umarao</option><option class="option" value="Umarkot">Umarkot</option><option class="option" value="Upper Dir">Upper Dir</option><option class="option" value="Uthal">Uthal</option><option class="option" value="Vehari">Vehari</option><option class="option" value="Veirwaro">Veirwaro</option><option class="option" value="Vitakri">Vitakri</option><option class="option" value="Wadh">Wadh</option><option class="option" value="Wah Cantt">Wah Cantt</option><option class="option" value="Warah">Warah</option><option class="option" value="Washap">Washap</option><option class="option" value="Wasjuk">Wasjuk</option><option class="option" value="Wazirabad">Wazirabad</option><option class="option" value="Yakmach">Yakmach</option><option class="option" value="Zhob">Zhob</option><option class="option" value="Other">Other</option>');
        }
    });
    });
</script>

@endsection