@extends('agent.layouts.apps')
 @section('content')

<div class="main">

    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">
    
                        <div class="panel-heading">
                            <h3 class="panel-title">Property # {{$property->id}} {{$property->lo_address}}</h3>
                        </div>
                        <div class="panel-body">
                        <table class="table table-bordered">
                            
                            <tbody>
                            <tr>
                                <td>Purpose</td>
                                <td>{{$property->purpose}}</td>
                            </tr>
                            <tr>
                                <td>Country</td>
                                <td>{{$property->country}}</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>{{$property->city}}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>{{$property->lo_address}}</td>
                            </tr>
                            </tbody>
                        </table>
                        <div id="map" style="width:100%;height:400px;"></div>
                        </div>
                        
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>


<script>
function myMap() {
  var myCenter = new google.maps.LatLng(<?php echo $property->lat?>,<?php echo $property->lng?>);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 13};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&callback=myMap"></script>

<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
@endsection