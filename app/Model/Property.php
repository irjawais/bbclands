<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'purpose',
        'property_type',
         'country','city',
         'lo_address',
         'property_title',
         'description',
         'price',
         'area',
         'unit',
         'lng',
         'lat',
         'register_type',
         'register_id',
         'approval',
         'deleted',
         'owner_phoneno',
         'owner_email',
         'owner_address',
         'is_featured'
        ];
    protected $table ="property";
}
