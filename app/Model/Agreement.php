<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    //protected $fillable =  array('*');
    protected $fillable =  ['name', 'phone_office', 'f_or_h_name', 'phone_residence', 'cnic_passport', 'mobile', 'nationality', 'whatsapp', 'email', 'twitter', 'facebook', 'linkedin', 'present_address', 'permanent_address', 'payment_method', 'cheque', 'draft', 'date', 'branch', 'drawn_no', 'price', 'price_unit', 'about_me', 'file'];
    protected $table ="agreements";
}
