<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Deals extends Model
{
    protected $fillable = [
        'property_id',
        'name',
         'email',
         'city',
         'phoneno',
         'description',
         'status',
         'lng',
         'lat',
         
        ];
    protected $table ="deals";
    public function property(){
        return $this->belongsTo(Property::class);
    }
}
