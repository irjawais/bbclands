<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Property;

class UserController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featured_properties = Property::where('approval', 1)->where('deleted', 0)->where("is_featured",1)->get();
        //dd($featured_properties);
        return view('user.main.index',compact('featured_properties'));
    }
    public function signin()
    {
        //return view('home');
        return view('user.main.sign_up');
    }
    
}
