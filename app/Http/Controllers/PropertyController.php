<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Property;
use App\Model\Deals;
use App\Model\Agreement;

use App\Model\Additional_Feature;
use File;
use Illuminate\Support\Facades\Input;
use Auth;
use PDF;
class PropertyController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = $this->getProperty();
        return view('user.property.index',compact('properties'));
    }
    public function search(Request $request)
    {
        $data = $request->all();
        $property = Property::where('deleted',0)->where('approval',1)->orderBy('id', 'desc');
        
        if($data['city']!='all')
            $property->where('city', 'like', '%' . $data['city'] . '%');
            
        if($data['type']!='all')
            $property->where('property_type', 'like', '%' . $data['type'] . '%');
           
        if($data['purpose']!='all')
            $property->where('purpose', 'like', '%' . $data['purpose'] . '%');
        $properties = $property->paginate(10)->appends(request()->except('page'));
        return view('user.property.search',compact('properties'));
    }
    public function detail($id,Request $request)
    {
        
        $property = Property::where('deleted',0)
            ->where('approval',1)
            ->orderBy('id', 'desc')
            ->where('id',$id)->first();
        return view('user.property.detail',compact('property','id'));
    }
    public function ajaxDeal(Request $request)
    {   
        $data = $request->all();
        $id  = $data['property_id'];
        $name = $data['data'][0]['value'];
        $email = $data['data'][1]['value'];
        $phoneno = $data['data'][2]['value'];
        $desc = $data['data'][3]['value'];
        $property = Deals::where('phoneno',$phoneno)->where('property_id',$id)->first();
        if($property){
            return 'alread_have_req';
        }
        $deal = new Deals;
        $deal->name = $name ;
        $deal->email=$email;
        $deal->phoneno =$phoneno;
        $deal->description =$desc;
        $deal->status ="new";
        $deal->property_id =$id;
        
        $deal->save();
        if($deal){
            return 'ok';
        }
        
    }
    
    public function getProperty(){
        /* $ip= \Request::ip();
        $location = \Location::get();
        dd($location); */
        
        $property = Property::where('deleted',0)->where('approval',1)->orderBy('id', 'desc')->get();
        return $property;
    }
    public function submitProperty(){
        $additional_features = Additional_Feature::where('deleted',0)->get();
        return view('user.property.submit_property',compact('additional_features'));
    }
    public function submitPropertyPost(Request $request){
        $data = $request->all();
     
        $property = Property::create([
            "purpose"=>Input::get('purpose'),
            "property_type"=>Input::get('property_type'),
            "country"=>Input::get('country'),
            "city"=>Input::get('city'),
            "lo_address"=>Input::get('lo_address'),
            "property_title"=>Input::get('property_title'),
            "description"=>Input::get('description'),
            "price"=>Input::get('price'),
            "area"=>Input::get('area'),
            "unit"=>Input::get('unit'),
            "lng"=>Input::get('lng'),
            "lat"=>Input::get('lat'),
            "owner_phoneno"=>Input::get('owner_phoneno'),
            "owner_email"=>Input::get('owner_email'),
            "owner_address"=>Input::get('owner_address'),
            
            "register_type"=>"user",
            "approval"=>1,
            "deleted"=>0,
            "register_id"=>Auth::user()->id
        ]);
        
        $array = array();
        if($request->hasFile('featured_image'))
        {
            //if($request->file('others_files')->isValid()) {

            $file = $request->file('featured_image');
           
                if($file->isValid()){
                $file->store('attachments_property/' . $property->id . '/messages');
                
                    $name = "file_" .$property->id.'_0.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
            
        }
        if($request->hasFile('others_files'))
        {
            //if($request->file('others_files')->isValid()) {

            $files = $request->file('others_files');
            foreach ($files as $key => $file) {
                $key++;
                if($file->isValid()){
                $file->store('attachments_property/' . $property->id . '/messages');
                
                    $name = "file_" .$property->id.'_'.$key.'.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
            }
        }
       //$array =  serialize($array);
       $property->uploaded_files = json_encode($array);
       $property->save();
       $request->session()->flash('message', 'Property submited Successfully'); 
       return redirect()->route('home');
    }
    public function myproperty(Request $request)
    {
        $data = $request->all();
        $property = Property::where('deleted',0)->where('register_type','user')->where('register_id', Auth::user()->id )->orderBy('id', 'desc');
       
        $properties = $property->paginate(10)->appends(request()->except('page'));
        return view('user.property.myproperty',compact('properties'));
    }
    public function removeProperty($id,Request $request){
        $property = Property::where('id',$id)->first();
        $property->deleted = 1;
        $property->save();
        return redirect()->back();
    }
    public function editProperty($id,Request $request){
        $property = Property::where('id',$id)->first();
        return view('user.property.submit_property',compact('property'));
    }
    public function editPropertyPost(Request $request){
        
       
        $property = Property::where('id',Input::get('property_id'))->first();
        $property->purpose = Input::get('purpose');
            $property->property_type=Input::get('property_type');
            $property->country=Input::get('country');
            $property->city=Input::get('city');
            $property->lo_address=Input::get('lo_address');
            $property->property_title=Input::get('property_title');
            $property->description=Input::get('description');
            $property->price=Input::get('price');
            $property->area=Input::get('area');
            $property->unit=Input::get('unit');
            $property->lng=Input::get('lng');
            $property->lat=Input::get('lat');
            $property->owner_phoneno=Input::get('owner_phoneno');
            $property->owner_email=Input::get('owner_email');
            $property->owner_address=Input::get('owner_address');
            $property->save();
           
            $array = array();
        if($request->hasFile('featured_image'))
        {
            //if($request->file('others_files')->isValid()) {

            $file = $request->file('featured_image');
           
                if($file->isValid()){
                $file->store('attachments_property/' . $property->id . '/messages');
                
                    $name = "file_" .$property->id.'_0.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
             $property->uploaded_files = json_encode($array);
             $property->save();
        }
        if($request->hasFile('others_files'))
        {
            //if($request->file('others_files')->isValid()) {

            $files = $request->file('others_files');
            foreach ($files as $key => $file) {
                $key++;
                if($file->isValid()){
                $file->store('attachments_property/' . $property->id . '/messages');
                
                    $name = "file_" .$property->id.'_'.$key.'.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
            }
            $property->uploaded_files = json_encode($array);
            $property->save();
        }
       //$array =  serialize($array);
       
       $request->session()->flash('message', 'Property Edited Successfully');

        return redirect()->back();
    }
    public function agreement(){
        return view("user.property.agreement");
    }
    public function agreementPost(Request $request){

        $data = $request->all();
        
        $agreement = Agreement::create($data);
        $string = "Dear ".$data['name'].' your property agrement submit successfully';
        //$request->session()->flash('message', $string);

        //return redirect()->back();
        //$this->agreementDownload($agreement->id);
       // $agreement = Agreement::findOrFail($id);
        
        return view('admin.agreement.pdf',compact('agreement'));

        //$pdf = PDF::loadView('admin.agreement.pdf',compact('agreement'));
        //return $pdf->download($agreement->name.'- '.$agreement->id.'.pdf');
        /* $pdf = PDF::loadView('admin.agreement.pdf',compact('agreement'));
	    return $pdf->render('document.pdf'); */
    }
    public function agreementDownload($id){
        
        $agreement = Agreement::findOrFail($id);
        
        return view('admin.agreement.pdf',compact('agreement'));
        /* $pdf = PDF::loadView('admin.agreement.pdf',compact('agreement'));
        return $pdf->download($agreement->name.'- '.$agreement->id.'.pdf'); */

        /* $pdf = PDF::loadView('admin.agreement.pdf',compact('agreement'));
	    return $pdf->stream('document.pdf'); */
    }
}
