<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Closure;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $guard = 'admin';
    public function __construct()
    {
        
        $this->middleware('admin:admin', ['except' => 'logout'])->except('logout');
        /* if(!Auth::guard('admin')->check()){
            return redirect('/admin/login');
        }
        if(Auth::guard('admin')->check()){
            return redirect('/admin/home');
        }
        dd('dd'); */
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.dashboard.home');
    }
    protected function guard() {
        return Auth::guard('admin:admin');
    }
}
