<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Model\Agent;
use App\Model\Property;
use App\Model\Agreement;
use DataTables;
use Redirect;
use Mail;
use PDF;
class PropertyController extends Controller
{
    

    public function featured()
    {
        return view('admin.featured.index');
    }
    public function ajaxFeatured(Request $request)
    {   
        $properties = Property::where('deleted', 0)->where('approval', 1);
        return DataTables::of($properties)->make(true);
    }
    public function mark_as_featured($id,$mark,Request $request)
    {   
        $properties = Property::where('id', $id)->first();
        if($mark==0){
            $properties->is_featured = 0;
        }else if($mark == 1 ){
            $properties->is_featured = 1;
        }
        $properties->save();
        return redirect()->back();
    }
    public function agreement(){
        return view('admin.agreement.index');
    }
    public function ajaxAgreement(){
        $properties = Agreement::orderBy('id', 'DESC');
        return DataTables::of($properties)->make(true);
        //return view('admin.agreement.index');
    }
    public function agreementDownload($id){
        $agreement = Agreement::findOrFail($id);
        return view('admin.agreement.pdf',compact('agreement'));
        /* $pdf = PDF::loadView('admin.agreement.pdf',compact('agreement'));
        return $pdf->download($agreement->name.'- '.$agreement->id.'.pdf'); */

        /* $pdf = PDF::loadView('admin.agreement.pdf',compact('agreement'));
        return $pdf->stream('document.pdf'); */
        
    }
    
}
