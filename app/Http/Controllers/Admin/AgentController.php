<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Model\Agent;
use DataTables;
use Redirect;
use Mail;
class AgentController extends Controller
{
    

    public function index()
    {
        return view('admin.agent.index');
    }
    public function ajax_get_agent(Request $request)
    {   
        $agent = Agent::get();
        
        return Datatables::of($agent)->make(true);
    }
    public function detail($id)
    {   
        $agent = Agent::where('id',$id)->first();
        
        return view('admin.agent.detail',compact('agent'));
    }
    public function agent_approvl_yes($id)
    {   
        $agent = Agent::where('id',$id)->first();
        $agent->approval=1;
        $agent->save();
        Mail::send('agent.email.approval_tamplate', ['agent' => $agent], function ($message)use ($agent)
        {   
                $message->from('bbclands@gmail.com', 'BBClands Team');
                $message->to($agent->email);
        });
        return Redirect::back();
    }
    public function agent_suspend_yes($id)
    {   
        $agent = Agent::where('id',$id)->first();
        $agent->suspend=1;
        $agent->save();
        /* Mail::send('agent.email.approval_tamplate', ['agent' => $agent], function ($message)use ($agent)
        {   
                $message->from('bbclands@gmail.com', 'BBClands Team');
                $message->to($agent->email);
        }); */
        return Redirect::back();
    }
    public function agent_suspend_no($id)
    {   
        $agent = Agent::where('id',$id)->first();
        $agent->suspend=0;
        $agent->save();
        /* Mail::send('agent.email.approval_tamplate', ['agent' => $agent], function ($message)use ($agent)
        {   
                $message->from('bbclands@gmail.com', 'BBClands Team');
                $message->to($agent->email);
        }); */
        return Redirect::back();
    }
    public function power($id)
    {   
        return view('admin.agent.power',compact('id'));
    }

}
