<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Model\Agent;
use DataTables;
use Redirect;
use Mail;
class PowerController extends Controller
{
    

    public function power_manager($id)
    {   
        $agent = Agent::where('id',$id)->first();
        $agent->power="manager";
        $agent->save();
        return Redirect::back();
    }
    public function power_assist($id)
    {   
        $agent = Agent::where('id',$id)->first();
        $agent->power="assistant";
        $agent->save();
        return Redirect::back();
    }
    public function power_remove($id)
    {   
        $agent = Agent::where('id',$id)->first();
        $agent->power="";
        $agent->save();
        return Redirect::back();
    }
    
}
