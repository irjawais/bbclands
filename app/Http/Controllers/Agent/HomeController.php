<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Http\Controllers\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $guard = 'agent';
    public function __construct()
    {
        
        $this->middleware('agent:agent', ['except' => 'logout'])->except('logout');
        /* if(!Auth::guard('agent')->check()){
            return redirect('/agent/login');
        }
        if(Auth::guard('agent')->check()){
            return redirect('/agent/home');
        } */
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* if(!Auth::guard('agent')->check()){
            return redirect('/agent/login');
        } */
        return view('agent.dashboard.index');
    }
    protected function guard() {
        return Auth::guard('agent:agent');
    }
}
