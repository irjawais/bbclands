<?php 
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Model\Property;
use Illuminate\Support\Facades\Input;
use DataTables;
use File;
class PropertyController extends Controller
{
    public function add_property()
    {   return view('agent.property.addproperty');
    }
    public function add_propertyPost(Request $request)
    {
        
      $property = Property::create([
            "purpose"=>Input::get('purpose'),
            "property_type"=>Input::get('property_type'),
            "country"=>Input::get('country'),
            "city"=>Input::get('city'),
            "lo_address"=>Input::get('lo_address'),
            "property_title"=>Input::get('property_title'),
            "description"=>Input::get('description'),
            "price"=>Input::get('price'),
            "area"=>Input::get('area'),
            "unit"=>Input::get('unit'),
            "lng"=>Input::get('lng'),
            "lat"=>Input::get('lat'),
            "register_type"=>"agent",
            "approval"=>1,
            "deleted"=>0,
            "register_id"=>Auth::guard("agent")->user()->id
        ]);
        $array = array();
        if($request->hasFile('img1'))
        {
            $file = $request->file('img1');
                if($file->isValid()){
                    $file->store('attachments_property/' . $property->id . '/messages');
                    $name = "file_" .$property->id.'_0.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
        }
        if($request->hasFile('img2'))
        {
            $file = $request->file('img2');
                if($file->isValid()){
                    $file->store('attachments_property/' . $property->id . '/messages');
                    $name = "file_" .$property->id.'_1.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
        }
        if($request->hasFile('img3'))
        {
            $file = $request->file('img3');
                if($file->isValid()){
                    $file->store('attachments_property/' . $property->id . '/messages');
                    $name = "file_" .$property->id.'_2.'.$file->getClientOriginalExtension();;
                    $path = public_path() . '/attachment_property/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $array[] = array( $name);
                    $file->move($path, $name);
             } 
        }
        
       $property->uploaded_files = json_encode($array);
       $property->save();
       
        $request->session()->flash('message', 'Property created Successful!');
        return redirect(route('agent.add.property'));
    }
    public function myproperty(Request $request)
    {   
           return view('agent.property.myproperty');
    }
    public function ajaxmypropertydata(Request $request)
    {   
        $properties = Property::where('register_id', Auth::guard("agent")->user()->id)->where('register_type', "agent")->get();
        return Datatables::of($properties)->make(true);
    }
    
    public function viewproperty($id,Request $request)
    {   
        $property = Property::where('id',$id)->first();
        return view('agent.property.viewproperty',compact('property'));
    }
    public function acceptProperty($id,Request $request)
    {   
        if(Auth::guard('agent')->user()->power != 'manager'){
        abort(404);
        }
        $property = Property::where('id',$id)->first();
        $property->approval = 1;
        $property->save();
        return redirect()->back();
    }
    public function deleteProperty($id,Request $request)
    {   
        if(Auth::guard('agent')->user()->power != 'manager'){
            abort(404);
         }
        $property = Property::where('id',$id)->first();
        $property->deleted = 1;
        $property->save();
        return redirect()->back();
    }
    public function deleteMyProperty($id,Request $request)
    {   
        $property = Property::where('id',$id)->first();
        if($property->register_id != Auth::guard('agent')->user()->id && $property->register_type != 'agent'){
            abort(404);
        }
        $property->deleted = 1;
        $property->save();
        return redirect()->back();
    }
    
    
    public function newRequest(Request $request)
    {   
        if(!Auth::guard('agent')->user()->power){
           abort(404);
        }
        
        return view('agent.property.newrequest');
    }
    public function allProperty(Request $request)
    {   
        if(!Auth::guard('agent')->user()->power){
           abort(404);
        }
        
        return view('agent.property.all');
    }
    
    public function ajaxallProperty(Request $request)
    {   
        $properties = Property::where('approval', 1)->where('deleted', 0);
        return DataTables::of($properties)->make(true);
    }

    public function ajaxNewPropertyRequest(Request $request)
    {   
        $properties = Property::where('approval', 0)->where('deleted', 0);
        return DataTables::of($properties)->make(true);
    }
    public function ajaxMyProperty(Request $request)
    {   
        $properties = Property::where('register_id', Auth::guard('agent')->user()->id)
            ->where('register_type', 'agent')
            ->where('deleted', 0);
        return DataTables::of($properties)->make(true);
    }
    
    
}
