<?php 
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Model\Property;
use Illuminate\Support\Facades\Input;
use DataTables;
use App\Model\Deals;
class DealsController extends Controller
{
    public function newDeals(Request $request)
    {   
        if(!Auth::guard('agent')->user()->power){
            abort(404);
         }
        return view('agent.deals.new_deals');
    }
    public function ajaxNewDeals(Request $request)
    {   
        $deals = Deals::with('property')->where('status', 'new')->where('delete', 0);
        return DataTables::of($deals)->make(true);
    }
    public function acceptDeal($id,Request $request)
    {   
        $deals = Deals::where('id', $id)->first();
        $deals->status="progress";
        $deals->save();
        return redirect()->back();
    }
    /* public function rejectDeal($id,Request $request)
    {   
        $deals = Deals::where('id', $id)->first();
        $deals->status="progress";
        $deals->save();
        return redirect()->back();
    } */
    public function completeDeal($id,Request $request)
    {   
        $deals = Deals::where('id', $id)->first();
        $deals->status="done";
        $deals->save();
        return redirect()->back();
    }
    public function deleteDeal($id,Request $request)
    {   
        $deals = Deals::where('id', $id)->first();
        $deals->delete=1;
        $deals->save();
        return redirect()->back();
    }
    public function myAllDeals(Request $request)
    {   
        return view('agent.deals.my_deals');
    }
    public function ajaxMyAllDeals(Request $request)
    {   
        $deals = Deals::with('property')->where('delete', 0);
        return DataTables::of($deals)->make(true);
    }
    
}
