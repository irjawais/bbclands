<?php 
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Closure;
use Auth;
use App\Model\Agent;
use Illuminate\Support\Facades\Input;
use DataTables;
use Redirect;
use Mail;
class EmailController extends Controller
{
    protected function email_verifyPost(Request $request){
        
        $data = $request->all();
        $user =Agent::where('id', Auth::guard("agent")->user()->id)->first();
        if($user->email_verify_code==$data['email_code']){
            $user->email_verified=1;
            $user->save();
            return redirect("agent/home");
        }
        else 
        {
            $request->session()->flash('error', 'Wrong Verificaton Code'); 
            return view('agent.email.emailverification');
        }
        
    }
    protected function resend_email_code(Request $request){
        $email_code = mt_rand(100000,999999);
        $agent =Agent::where('id', Auth::guard("agent")->user()->id)->first();
        $agent->email_verify_code = $email_code;
        $agent->save();
        /* Mail::raw('Email verification code is '.$email_code, function ($message)use ($agent) {
            $message->to($agent->email);
         }); */

            Mail::send('agent.email.emailverify_tamplate', ['agent' => $agent,'email_code'=>$email_code], function ($message)use ($agent)
            {   
                $message->from('bbclands@gmail.com', 'BBClands Team');
                $message->to($agent->email);
            });
          $request->session()->flash('error', 'Email Has been sent again. Please check inbox');
          return Redirect::back(); 
    }
    protected function email_tamplate(Request $request){
        
        return view('agent.email.emailverify_tamplate');
    
    }
}
