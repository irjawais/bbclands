<?php

namespace App\Http\Controllers\Agent\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Model\Agent;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'agent/home';
    protected $guard = 'agent';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware('agent:agent', ['except' => 'logout'])->except('logout');
    }
    public function showLoginForm()
    {
        /* if(Auth::guard('agent')->check()){
            return redirect('/agent/home');
        } */
        return view('agent.auth.login');
    }
    
    protected function attemptLogin(Request $request)
    {
       
        return $this->guard('agent:agent')->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
   /*  protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard('admin:admin')->user())
                ?: redirect()->intended($this->redirectPath());
    } */
    protected function guard() {
        return Auth::guard('agent');
    }
    public function logout(Request $request)
    {
        $this->guard('agent')->logout();

        $request->session()->invalidate();

        return redirect('/agent/login');
    }
    protected function unauthenticated($request, AuthenticationException $exception)
    {   
        return redirect('/agent/login');
    }
    protected function authenticated(Request $request, $user)
    {
        $client= Auth::guard('agent')->user();
        if ( $client->email_verified==0 ) {
            return redirect()->to('email_verify');
        }
        return redirect('/agent/home');
    }
    
    protected function email_verify(){
        return view('agent.email.emailverification');
        
    }
    
    
}
