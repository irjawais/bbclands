<?php

namespace App\Http\Controllers\Agent\Auth;

use App\Model\Agent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'email_verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware('agent:agent');
    }
    public function showRegistrationForm()
    {
        return view('agent.auth.register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:agent',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new agent instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Agent
     */
    protected function create(array $data)
    {
        $email_code = mt_rand(100000,999999);
        $agent = Agent::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'email_verify_code'=>$email_code
        ]);
        Mail::send('agent.email.emailverify_tamplate', ['agent' => $agent,'email_code'=>$email_code], function ($message)use ($agent)
            {   
                $message->from('bbclands@gmail.com', 'BBClands Team');
                $message->to($agent->email);
        });
         return $agent;
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard("agent")->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
    protected function registered(Request $request, $user)
    {
        //
    }
    protected function guard() {
        return Auth::guard('agent');
    }
}
