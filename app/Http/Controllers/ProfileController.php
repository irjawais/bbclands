<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Property;
use App\Model\Deals;

use App\Model\Additional_Feature;
use File;
use Illuminate\Support\Facades\Input;
use Auth;

class ProfileController extends Controller
{
    public function myProfile(){
        return view('user.main.myprofile');
    }
    public function submitProfile(Request $request){
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'phoneno' => 'required|numeric|min:11',
            'aboutme' => 'required|string|max:1000',
       ]);
       $data = $request->all();
       Auth::user()->update(array('name' => $data['name'],'phoneno' => $data['phoneno'],'aboutme' => $data['aboutme']));
       return redirect()->back();
    }
    
}
