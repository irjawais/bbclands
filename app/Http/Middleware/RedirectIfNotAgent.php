<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAgent
{
/**
 * Handle an incoming request.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Closure  $next
 * @param  string|null  $guard
 * @return mixed
 */
public function handle($request, Closure $next, $guard)
{
     /* if (!Auth::guard($guard)->check()) {
        return redirect('/');
    } */
    if ($guard != "agent" && Auth::guard($guard)->check()) {
        //
        return redirect('/');
    }
    if ($guard == "agent" && Auth::guard($guard)->check()) {
        if(Auth::guard($guard)->user()->email_verified==0){
            return redirect('email_verify');
        }
    }
    if ($guard == "agent" && Auth::guard($guard)->check()) {
        if(Auth::guard($guard)->user()->approval!=1){
            Auth::guard('agent')->logout();
            return redirect('acount_approval');
        }
    }
    if ($guard == "agent" && Auth::guard($guard)->check()) {
        if(Auth::guard($guard)->user()->suspend!=0){
            Auth::guard('agent')->logout();
            return redirect('acount_suspend');
        }
    }

    return $next($request);
}
}